# METS MODS Mapping

## Description

An implementation of the mapping created by the project "Textdatabase and Dictionary of Classic Mayan".

In the first setup it takes an URI of an Artefact-RDF-File (meaning the rdf-class "Artefact" of the IDIOM-Schema (https://www.classicmayan.org/rdfmask/idiom.ttl)). Based on this it queries several related files of activities, vocabularies, images and so on.

The result is a representation of an artefact with all related and important metadata from the schema.

This mapping must be included in the pom file of the oai-pmh code of the TextGrid-OAI-PMH interface (https://gitlab.gwdg.de/dariah-de/dariah-de-oai-pmh-services). In other way the it won't be possible to harvesting the data from TextGrid.

Additionally it process also a files in METS/MODS representation based on an identifier of an image in the ConedaKor-Database of the project (https://classicmayan.kor.de.dariah.eu/). 

## Releasing a New Version

For releasing a new version of ClassicMayan METS MODS Mapping, please have a look at the [DARIAH-DE Release Management Page](https://wiki.de.dariah.eu/display/DARIAH3/DARIAH-DE+Release+Management#DARIAHDEReleaseManagement-Gitlabflow/Gitflow(develop,main,featurebranchesundtags)) or see the [Gitlab CI file](.gitlab-ci.yml).
