package org.classicmayan.tools;

import static org.junit.Assert.assertFalse;
import org.junit.Test;

/**
 * @author Stefan E. Funk, SUB Göttingen
 */
public class TestUtils {

  /**
   * 
   */
  @Test
  public void testCutExtend() {

    String extend = "5.2 cm (height), 4.4 cm (width), ";
    String expectedExtend = "5.2 cm (height), 4.4 cm (width)";
    String resultExtend = ClassicMayanMetsMods.cutExtend(extend);
    if (!expectedExtend.equals(resultExtend)) {
      assertFalse("'" + resultExtend + "' != '" + expectedExtend + "'", true);
    }

    String extend2 = "5.2 cm (height), 5.2 cm (width), 2.2 cm (thickness)";
    String expectedExtend2 = "5.2 cm (height), 5.2 cm (width), 2.2 cm (thickness)";
    String resultExtend2 = ClassicMayanMetsMods.cutExtend(extend2);
    if (!expectedExtend2.equals(resultExtend2)) {
      assertFalse("'" + resultExtend2 + "' != '" + expectedExtend2 + "'", true);
    }
  }

}
