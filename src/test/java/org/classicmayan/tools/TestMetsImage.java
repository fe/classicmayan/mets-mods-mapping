package org.classicmayan.tools;

import static org.junit.Assert.assertFalse;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.StringWriter;
import java.io.Writer;
import java.net.URISyntaxException;
import java.net.URL;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import javax.xml.bind.JAXB;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;
import info.textgrid.clients.tgcrud.TextGridObject;
import info.textgrid.namespaces.metadata.core._2010.MetadataContainerType;

/**
 * TODO Check if ignored tests are making sense later!
 */
@Ignore
public class TestMetsImage {

  // private static final String PROPERTIES_FILE = "metsmods.properties";
  private static final String PROPERTIES_FILE = "metsmods.HIDDEN.properties";
  // private static final String PROPERTIES_FILE = "metsmods.dev.HIDDEN.properties";

  // private static JSONObject imageMetadataSet;
  private static String idiomTGCrudEndpoint;
  private static String idiomRbacSessionID;
  private static ConedaKorID conedakorID;
  private static String conedakorValue;
  private static TextGridUri textgridJSONURI;
  private static String textgridJSONValue;
  private static String imageListURI;
  private static List<String> imageListValues = new ArrayList<String>();

  /**
   * @throws IOException
   * @throws Exception
   */
  @BeforeClass
  public static void setUpBeforeClass() throws IOException {

    System.out.println("properties path: " + PROPERTIES_FILE);

    Properties p = new Properties();
    File f = getResource(PROPERTIES_FILE);
    p.load(new FileInputStream(f));

    // Get values from properties file.
    idiomTGCrudEndpoint = p.getProperty("TGCRUD_ENDPOINT");
    idiomRbacSessionID = p.getProperty("RBAC_SESSION_ID");
    conedakorID = new ConedaKorID(p.getProperty("CONEDAKOR_ID"));
    conedakorValue = p.getProperty("CONEDAKOR_VALUE");
    textgridJSONURI = new TextGridUri(p.getProperty("TEXTGRID_JSON_URI"));
    textgridJSONValue = p.getProperty("TEXTGRID_JSON_VALUE");
    imageListURI = p.getProperty("IMAGE_LIST_URI");
    String imageList[] = p.getProperty("IMAGE_LIST_VALUES").split(",");
    for (String i : imageList) {
      imageListValues.add(i);
    }

    System.out.println("properties:");
    System.out.println("\tTGCRUD_ENDPOINT: " + idiomTGCrudEndpoint);
    System.out.println("\tSID: " + idiomRbacSessionID.substring(0, 4) + "...");
    System.out.println("\tKONEDAKOR_ID: " + conedakorID.getId());
    System.out.println("\tKONEDAKOR_VALUE: " + conedakorValue);
    System.out.println("\tTEXTGRID_JSON_URI: " + textgridJSONURI.getValue());
    System.out.println("\tTEXTGRID_JSON_VALUE: " + textgridJSONValue);
    System.out.println("\tIMAGE_LIST_URI: " + imageListURI);
    System.out.println("\tIMAGE_LIST_VALUES: " + imageListValues);
  }

  // **
  // TESTS
  // **

  /**
   * <p>
   * Test creation of a ImageMetsMods object from TextGrid URI. Needs a TextGrid RBAC Session ID!
   * </p>
   * 
   * @throws IOException
   */
  @Test
  public void testImageMetsModsCreationFromTextGridURI() throws IOException {

    String jsonMetadata = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>\n"
        + "<MetadataContainerType xmlns=\"http://textgrid.info/namespaces/metadata/core/2010\" xmlns:ns2=\"http://www.w3.org/1999/02/22-rdf-syntax-ns#\">\n"
        + "  <object>\n"
        + "    <generic>\n"
        + "      <provided>\n"
        + "        <title>[27293] Xultun, Stela 7, front</title>\n"
        + "        <format>application/json</format>\n"
        + "        <notes>ConedaKorMediumData 27293</notes>\n"
        + "      </provided>\n"
        + "      <generated>\n"
        + "        <created>2022-02-11T15:38:32.764+01:00</created>\n"
        + "        <lastModified>2022-02-11T15:38:32.764+01:00</lastModified>\n"
        + "        <textgridUri extRef=\"\">textgrid:3xr1c.0</textgridUri>\n"
        + "        <revision>0</revision>\n"
        + "        <extent>2427</extent>\n"
        + "        <fixity>\n"
        + "          <messageDigestAlgorithm>md5</messageDigestAlgorithm>\n"
        + "          <messageDigest>2d737bc1d05c2e27903a5998f5c8484e</messageDigest>\n"
        + "          <messageDigestOriginator>crud-base 11.3.0-TG-RELEASE</messageDigestOriginator>\n"
        + "        </fixity>\n"
        + "        <dataContributor>Maximilian.Brodhun@textgrid.de</dataContributor>\n"
        + "        <project id=\"TGPR-0e926f53-1aba-d415-ecf6-539edcd8a318\">idiom</project>\n"
        + "        <permissions>read</permissions>\n"
        + "      </generated>\n"
        + "    </generic>\n"
        + "    <item />\n"
        + "  </object>\n"
        + "</MetadataContainerType>";
    String jsonData = "{\n"
        + "  \"kind_name\": \"Medium (image/jpeg)\",\n"
        + "  \"kind_id\": 1,\n"
        + "  \"distinct_name\": null,\n"
        + "  \"lock_version\": 1,\n"
        + "  \"no_name_statement\": \"enter_name\",\n"
        + "  \"created_at\": \"2022-02-02T13:42:47.000Z\",\n"
        + "  \"medium\": {\n"
        + "    \"content_type\": \"image/jpeg\",\n"
        + "    \"id\": 13738,\n"
        + "    \"video\": null,\n"
        + "    \"audio\": null,\n"
        + "    \"file_size\": 349532,\n"
        + "    \"url\": {\n"
        + "      \"preview\": \"/media/images/preview/000/013/738/image.jpg?1643809367\",\n"
        + "      \"normal\": \"/media/images/normal/000/013/738/image.jpg?1643809367\",\n"
        + "      \"thumbnail\": \"/media/images/thumbnail/000/013/738/image.jpg?1643809367\",\n"
        + "      \"original\": \"/media/images/original/000/013/738/image.jpg?1643809367\",\n"
        + "      \"icon\": \"/media/images/icon/000/013/738/image.jpg?1643809367\",\n"
        + "      \"screen\": \"/media/images/screen/000/013/738/image.jpg?1643809367\"\n"
        + "    }\n"
        + "  },\n"
        + "  \"display_name\": \"medium 27293\",\n"
        + "  \"uuid\": \"acfa05d2-feea-4ed2-a8b2-64d66a006d37\",\n"
        + "  \"tags\": [],\n"
        + "  \"collection_id\": 3,\n"
        + "  \"updated_at\": \"2022-02-02T14:50:23.000Z\",\n"
        + "  \"subtype\": \"\",\n"
        + "  \"creator_id\": 34,\n"
        + "  \"updater_id\": 34,\n"
        + "  \"name\": null,\n"
        + "  \"comment\": \"Cited from: Morley, Sylvanus G. (1937-1938): The Inscriptions of Petén. Vol. 5. Carnegie Institution of Washington Publication 437. Carnegie Institution of Washington, Washington, D.C. (plate 77, a), https://hdl.handle.net/2027/mdp.39015029403725\",\n"
        + "  \"id\": 27293,\n"
        + "  \"medium_id\": 13738,\n"
        + "  \"dataset\": {\n"
        + "    \"digital_camera_model_name\": \"\",\n"
        + "    \"scanner_model_number\": \"\",\n"
        + "    \"note\": \"\",\n"
        + "    \"folder_no\": \"\",\n"
        + "    \"date_of_digitization\": \"\",\n"
        + "    \"scanning_software_name\": \"\",\n"
        + "    \"scanning_software_version_no\": \"\",\n"
        + "    \"source_y_dimension_unit\": \"\",\n"
        + "    \"contributor\": \"Project Text Database and Dictionary of Classic Mayan\",\n"
        + "    \"image_height\": \"\",\n"
        + "    \"color_space\": \"\",\n"
        + "    \"uri_of_image_additional_publication\": \"\",\n"
        + "    \"image_description\": \"Xultun, Stela 7, front\",\n"
        + "    \"creator\": \"Sylvanus G. Morley\",\n"
        + "    \"image_no\": \"SGM_1937_XUL_St_7_Plate 77_a\",\n"
        + "    \"file_name\": \"\",\n"
        + "    \"digital_camera_manufacturer\": \"\",\n"
        + "    \"master_image_file_type\": \"\",\n"
        + "    \"scanner_manufacturer\": \"\",\n"
        + "    \"source_y_dimension_value\": \"\",\n"
        + "    \"digitized_by\": \"\",\n"
        + "    \"source_type\": \"\",\n"
        + "    \"image_width\": \"\",\n"
        + "    \"license_note\": \"\",\n"
        + "    \"source_x_dimension_unit\": \"\",\n"
        + "    \"source_x_dimension_value\": \"\",\n"
        + "    \"license\": \"CC PDM 1.0\",\n"
        + "    \"scanner_model_name\": \"\",\n"
        + "    \"publisher\": \"Maya Image Archive\",\n"
        + "    \"maximum_optical_resolution\": \"\",\n"
        + "    \"rights_holder\": \"Public Domain\",\n"
        + "    \"date_time_created\": \"undated\"\n"
        + "  }\n"
        + "}";
    MetadataContainerType metadata = JAXB.unmarshal(jsonMetadata, MetadataContainerType.class);
    TextGridObject tgo =
        new TextGridObject(metadata, new ByteArrayInputStream(jsonData.getBytes()));

    // Take UTC date formatting here!
    String creationDate = "2022-02-11T14:38:32Z";
    String changeDate = "2022-02-11T14:38:32Z";
    ImageMetsMods metsmods = new ImageMetsMods(tgo, creationDate, changeDate);

    String json = "";
    try (Writer w = new StringWriter();) {
      metsmods.getImageMetadata().write(w);
      json = w.toString();

      if (!json.equals(textgridJSONValue)) {
        System.out.println(json + " !=");
        System.out.println(textgridJSONValue);
        assertFalse(true);
      }
    }
  }

  /**
   * <p>
   * Test creation of a ImageMetsMods object from JSON string.
   * </p>
   * 
   * @throws IOException
   */
  @Test
  public void testImageMetsModsCreationFromJSON() throws IOException {

    JSONObject o = new JSONObject(textgridJSONValue);

    ImageMetsMods metsmods = new ImageMetsMods(o);

    String json = "";
    try (Writer w = new StringWriter();) {
      metsmods.getImageMetadata().write(w);
      json = w.toString();

      if (!json.equals(textgridJSONValue)) {
        System.out.println(json + " !=");
        System.out.println(textgridJSONValue);
        assertFalse(true);
      }
    }
  }

  /**
   * <p>
   * Test creation of a ImageMetsMods object from a ConedaKor ID.
   * </p>
   * 
   * @throws IOException
   * @throws JSONException
   */
  @Test
  @Ignore
  public void testImageMetsModsCreationFromConedaKorID() throws JSONException, IOException {

    ImageMetsMods metsmods = new ImageMetsMods(conedakorID);

    String json = "";
    try (Writer w = new StringWriter();) {
      metsmods.getImageMetadata().write(w);
      json = w.toString();

      if (!json.equals(conedakorValue)) {
        System.out.println(json + " !=");
        System.out.println(conedakorValue);
        assertFalse(true);
      }
    }
  }

  /**
   * <p>
   * Test getting an image list from Fuseki.
   * </p>
   */
  @Test
  public void getImagesOfArtefact() {

    Queries queries = new Queries();

    List<String> imageList = new ArrayList<String>();
    for (String imageID : queries.getImagesOfArtefact(imageListURI)) {
      imageList.add(imageID);
    }

    if (!imageList.equals(imageListValues)) {
      System.out.println(imageList + "!=");
      System.out.println(imageListValues);
      assertFalse(true);
    }
  }

  /**
   * @throws JSONException
   * @throws IOException
   */
  @Test
  @Ignore
  public void getMediaListFromConedaKor() throws JSONException, IOException {
    System.out.println(ConedaKorQueries.getMediaList("1", "100", "1", "2020-02-09", "2021-02-09"));
  }

  /**
   * @throws JSONException
   * @throws IOException
   */
  @Test
  @Ignore
  public void testGetTotalSizeOfMediaListFromConedaKor() throws JSONException, IOException {

    MediaHarvester mediaHarvester = new MediaHarvester("1", "100", "1", "", "");

    System.out.println(mediaHarvester.getTotalMedia());
  }

  /**
   * @throws JSONException
   * @throws IOException
   */
  @Test
  @Ignore
  public void testGetMediaListFromConedaKor() throws JSONException, IOException {

    MediaHarvester mediaHarvester = new MediaHarvester("1", "100", "1", "", "");

    System.out.println(mediaHarvester.getMediaListFromConedaKor());
  }

  /**
   * @throws JSONException
   * @throws IOException
   * @throws ParseException
   */
  @Test
  @Ignore
  public void createMetsModsFromMediaList() throws JSONException, IOException, ParseException {

    MediaHarvester mediaHarvester = new MediaHarvester("1", "100", "1", "2020-02-09", "2021-02-09");
    JSONArray mediaList = mediaHarvester.getMediaListFromConedaKor().getJSONArray("records");

    for (int i = 0; i < mediaList.length(); i++) {
      ImageMetsMods imm = new ImageMetsMods(mediaList.getJSONObject(i));

      System.out.println(imm.getXML());
      System.out.println("---------------\n\n\n");
    }
  }

  /**
   * 
   */
  @Test
  @Ignore
  public void testModsResevedAccessCondition() {

    ModsAccessCondition reservedCopyrights = new ModsAccessCondition();
    ModsCopyRight copyRightsForReserverdAccessCondition = new ModsCopyRight(
        new CopyrightStats("copyrighted"),
        new PublicationStatus("unpublished"));

    copyRightsForReserverdAccessCondition.addModsGeneralNote(new ModsGeneralNote("VALUE"));
    reservedCopyrights.addModsCopyRight(copyRightsForReserverdAccessCondition);

    System.out.println(reservedCopyrights.getXML());
  }

  /**
   * @throws JSONException
   * @throws IOException
   */
  @Test
  @Ignore
  public void testBuildOriginInfo() throws JSONException, IOException {

    ImageMetsMods imm = new ImageMetsMods(new ConedaKorID("17481"));

    System.out.println(imm.buildOriginInfo().getXML());
  }

  /**
   * @throws JSONException
   * @throws IOException
   * @throws ParseException
   */
  @Test
  @Ignore
  public void testPrintMetsXML() throws JSONException, IOException, ParseException {

    ImageMetsMods imm = new ImageMetsMods(new ConedaKorID("11381"));

    System.out.println(imm.getXML());
  }

  /**
   * @throws JSONException
   * @throws IOException
   */
  @Test
  @Ignore
  public void testBuildGenre() throws JSONException, IOException {

    ImageMetsMods imm = new ImageMetsMods(new ConedaKorID("24865"));

    System.out.println(imm.buildGenre().getXMLFine());
  }

  /**
   * @throws JSONException
   * @throws IOException
   */
  @Test
  @Ignore
  public void testBuildStructLink() throws JSONException, IOException {

    ImageMetsMods imm = new ImageMetsMods(new ConedaKorID("24865"));

    System.out.println(imm.buildStructLink().getXML());
  }

  /**
   * @throws JSONException
   * @throws IOException
   */
  @Test
  @Ignore
  public void testStructMap() throws JSONException, IOException {

    ImageMetsMods imm = new ImageMetsMods(new ConedaKorID("24865"));

    System.out.println(imm.buildStructMap("LOGICAL").getXML());
  }

  /**
   * @throws JSONException
   * @throws IOException
   */
  @Test
  @Ignore
  public void testBuildFileGrp() throws JSONException, IOException {

    ImageMetsMods imm = new ImageMetsMods(new ConedaKorID("24865"));

    System.out.println(imm.buildFileSec().getXML());
  }

  /**
   * @throws ParseException
   * @throws JSONException
   * @throws IOException
   */
  @Test
  @Ignore
  public void testBuildAmdSec() throws ParseException, JSONException, IOException {

    ImageMetsMods imm = new ImageMetsMods(new ConedaKorID("24865"));

    System.out.println(imm.buildAmdSec().getXML());
  }

  /**
   * 
   */
  @Test
  @Ignore
  public void testRights() {

    System.out.println(new MetsRights(
        new MetsOwner("Text Database and Dictionary of Classic Mayan"),
        new MetsOwnerSiteURL("http://www.mayadictionary.de"),
        new MetsRightsSponsor(
            "Nordrhein-Westf&#228;lische Akademie der Wissenschaften und der K&#252;nste, Union der Wissenschaftsakademien"))
                .getMetsRightsXML());
  }

  /**
   * @throws ParseException
   * @throws IOException
   * @throws JSONException
   */
  @Test
  @Ignore
  public void testModsNameMultiple() throws ParseException, JSONException, IOException {

    // Get image metadata set.
    JSONObject imageMetadataSet = getImageMetadata(conedakorID);

    Mods mods = new Mods();

    ModsName name = new ModsName(
        new ModsNamePart(imageMetadataSet.getString("<copryrightHolder>")),
        new ModsRole(new ModsRoleTerm("code", "marcelator", "cph")));
    mods.addModsName(name);

    ModsName name2 = new ModsName(
        new ModsNamePart(imageMetadataSet.getString("<copryrightHolder>")),
        new ModsRole(new ModsRoleTerm("code", "marcelator", "cre")));
    mods.addModsName(name2);

    System.out.println(mods.getXML());
  }

  /**
   * @throws IOException
   * @throws JSONException
   * 
   */
  @Test
  @Ignore
  public void testModsName() throws JSONException, IOException {

    // Get image metadata set.
    JSONObject imageMetadataSet = getImageMetadata(conedakorID);

    ModsName name = new ModsName(
        new ModsNamePart(imageMetadataSet.getString("<copryrightHolder>")),
        new ModsRole(
            new ModsRoleTerm("code", "marcelator", "cph")));

    System.out.println(name.getModsNameXML());
  }

  /**
   * 
   */
  @Test
  @Ignore
  public void testModsAccessCondition() {

    ModsAccessCondition accessCondition = new ModsAccessCondition(
        new Type("use and reproduction"),
        new DisplayLabel("kor_license"),
        "CC BY 4.0");

    System.out.println(accessCondition.getXML());
  }

  /**
   * @throws JSONException
   * @throws IOException
   */
  @Test
  @Ignore
  public void testTitleInfo() throws JSONException, IOException {

    // Get image metadata set.
    JSONObject imageMetadataSet = getImageMetadata(conedakorID);

    ModsTitleInfo titleInfo = new ModsTitleInfo();
    titleInfo.addTitle(new Title(imageMetadataSet.get("<title>").toString(), "en"));

    System.out.println(titleInfo.getXML());
  }

  /**
   * @throws JSONException
   * @throws IOException
   * @throws ParseException
   */
  @Test
  @Ignore
  public void testRecordInfo() throws JSONException, IOException, ParseException {

    // Get image metadata set.
    JSONObject imageMetadataSet = getImageMetadata(conedakorID);

    ModsRecordInfo recordInfo = new ModsRecordInfo(
        new RecordIdentifier(imageMetadataSet.get("<recordIdentifier(returnedEntity)>").toString()),
        new RecordCreationDate("iso8601", "2016-02-18T11:15:19.899+01:00"),
        new RecordChangeDate("iso8601", "2016-02-18T11:15:19.899+01:00"));

    System.out.println(recordInfo.getXML());
  }

  /**
   * @throws ParseException
   * @throws IOException
   * @throws JSONException
   */
  @Test
  @Ignore
  public void testMods() throws ParseException, JSONException, IOException {

    // Get image metadata set.
    JSONObject imageMetadataSet = getImageMetadata(conedakorID);

    Mods mods = new Mods();
    ModsRecordInfo recordInfo = new ModsRecordInfo(
        new RecordIdentifier(imageMetadataSet.get("<recordIdentifier(returnedEntity)>").toString()),
        new RecordCreationDate("iso8601", "2016-02-18T11:15:19.899+01:00"),
        new RecordChangeDate("iso8601", "2016-02-18T11:15:19.899+01:00"));
    mods.addRecordInfoToMods(recordInfo);
    ModsTitleInfo titleInfo = new ModsTitleInfo();
    titleInfo.addTitle(new Title(imageMetadataSet.get("<title>").toString(), "en"));
    mods.addModsTitleInfo(titleInfo);

    System.out.println(mods.getXML());
  }

  /**
   * @throws ParseException
   */
  @Test
  @Ignore
  public void testXmlData() throws ParseException {

    MetsXmlData xmlData = new MetsXmlData();
    Mods mods = new Mods();
    xmlData.addMods(mods);

    System.out.println(xmlData.getXML());
  }

  /**
   * @throws ParseException
   */
  @Test
  @Ignore
  public void testMdWrap() throws ParseException {

    MetsMdWrap mdWrap = new MetsMdWrap(new MimeType("text/xml"), new MdType("MODS"));
    MetsXmlData xmlData = new MetsXmlData();
    mdWrap.addXmlData(xmlData);
    Mods mods = new Mods();
    xmlData.addMods(mods);

    System.out.println(mdWrap.getXML());
  }

  /**
   * @throws JSONException
   * @throws IOException
   * @throws ParseException
   */
  @Test
  @Ignore
  public void testDmdSec() throws JSONException, IOException, ParseException {

    JSONObject testIimageMetadataSet =
        new ImageMetsMods(new ConedaKorID("24865")).getImageMetadata();
    MetsDmdSec metsDmdSec = new MetsDmdSec(
        "conedaKor_" + testIimageMetadataSet.get("<recordIdentifier(returnedEntity)>").toString());
    MetsMdWrap mdWrap = new MetsMdWrap(new MimeType("text/xml"), new MdType("MODS"));
    metsDmdSec.addMdWrap(mdWrap);

    MetsXmlData xmlData = new MetsXmlData();
    mdWrap.addXmlData(xmlData);
    Mods mods = new Mods();
    xmlData.addMods(mods);

    System.out.println(metsDmdSec.getXML());
  }

  // **
  // PRIVATE METHODS
  // **

  /**
   * <p>
   * Loads a resource.
   * </p>
   * 
   * TODO Put together with the method in TGCrudServiceUtilities! Maybe create a first build maven
   * module for util things.
   * 
   * @param {@link String} The resource to search for.
   * @return {@link File} The resource.
   * @throws IOException
   */
  private static File getResource(String resPart) throws IOException {

    File res;

    // If we have an absolute resPart, just return the file.
    if (resPart.startsWith(File.separator)) {
      return new File(resPart);
    }

    URL url = ClassLoader.getSystemClassLoader().getResource(resPart);
    if (url == null) {
      throw new IOException("Resource '" + resPart + "' not found");
    }
    try {
      res = new File(url.toURI());
    } catch (URISyntaxException ue) {
      res = new File(url.getPath());
    }

    return res;
  }

  /**
   * @param conedaKorID
   * @return
   * @throws JSONException
   * @throws IOException
   */
  private static JSONObject getImageMetadata(ConedaKorID conedaKorID)
      throws JSONException, IOException {

    JSONObject result = null;

    ImageMetsMods metsMods = new ImageMetsMods(conedaKorID);
    result = metsMods.getImageMetadata();

    return result;
  }

}
