package org.classicmayan.tools;

import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;
import org.json.JSONException;
import org.junit.Ignore;
import org.junit.Test;

/**
 * TODO This seem also to be online tests, ignoring them if not used locally!!
 */
@Ignore
public class TestMets {

  /**
   * @throws ParseException
   * @throws JSONException
   * @throws IOException
   */
  @Test
  public void testClassicMayanMetsMods() throws ParseException, JSONException, IOException {

    String textgridBaseURI = "textgrid:3wm3g";
    // String textgridBaseURI = "textgrid:254w6";
    // String textgridBaseURI = "textgrid:3q17f";
    String creationDate = "1970-01-01T00:00:00.000+01:00";
    String modificationDate = "1970-01-01T00:00:00.000+01:00";

    ClassicMayanMetsMods metsmods =
        new ClassicMayanMetsMods(textgridBaseURI, creationDate, modificationDate);

    System.out.println(metsmods.getMets());
  }


  /*
   * static Mods mods = new Mods(); static ModsRecordInfo modsRecordInfo = new ModsRecordInfo();
   * static RecordIdentifier ri = new RecordIdentifier("textgrid:12121w2"); static
   * RecordCreationDate recordCreationDate = new RecordCreationDate();; static RecordChangeDate
   * recordChangeDate = new RecordChangeDate();; static ModsTypeOfResource modsTypeOfResource = new
   * ModsTypeOfResource();; static ModsPhysicalLocation mpl = new ModsPhysicalLocation();; static
   * ModsLocation ml = new ModsLocation(); static ModsTitleInfo modsTitleInfo = new ModsTitleInfo();
   * static ModsOriginInfo modsOriginInfo = new ModsOriginInfo(); static ModsDateCreated dateCreated
   * = new ModsDateCreated(); static ModsGenre modsGenre = new ModsGenre(); static
   * ModsPhysicalDescription modsPhysicalDescription = new ModsPhysicalDescription(); static Form
   * form = new Form(); static Extent extent = new Extent(); static ModsRelatedItem modsRelatedItem
   * = new ModsRelatedItem(); static ModsNote modsNote = new ModsNote(); static ModsAccessCondition
   * modsAccessCondition = new ModsAccessCondition();
   * 
   * static MetsHdr metsHdr = new MetsHdr(); static Mets mets = new Mets(); static MetsDmdSec
   * metsDmdSec = new MetsDmdSec(); static MetsMdWrap metsMdWrap = new MetsMdWrap(); static
   * MetsXmlData metsXmlData = new MetsXmlData(); static MetsAmdSec metsAmdSec = new MetsAmdSec();
   * static MetsRightsMD metsRightsMD = new MetsRightsMD(""); static MetsMdWrap
   * metsMdWrapWithOtherMdType = new MetsMdWrap();
   * 
   * static MetsOwner metsOwner = new MetsOwner(); static MetsOwnerSiteURL metsOwnerSiteURL = new
   * MetsOwnerSiteURL(); static MetsRightsSponsor metsRightsSponsor = new MetsRightsSponsor();
   * static MetsXmlData metsXmlDataForRights = new MetsXmlData(); static MetsRights metsRights = new
   * MetsRights();
   * 
   * static MetsDigiProvMD metsDigiProvMD = new MetsDigiProvMD(""); static MetsMdWrap
   * metsMdWrapForDigiProv = new MetsMdWrap(); static MetsXmlData metsXmlDataForDigiProv = new
   * MetsXmlData(); static MetsLinks metsLinks = new MetsLinks(); static MetsReference metsReference
   * = new MetsReference();
   * 
   * static MetsFileSec metsFileSec = new MetsFileSec(); static MetsFileGrp metsFileGrp = new
   * MetsFileGrp(); static MetsFile metsFile = new MetsFile("", "", ""); static MetsFlocat
   * metsFlocat = new MetsFlocat("", "");
   * 
   * //static MetsStructMap metsStructMap = new MetsStructMap(); static MetsDiv metsDiv = new
   * MetsDiv(); static MetsDiv metsDivSecondLayer = new MetsDiv(); static MetsFptr metsFptr = new
   * MetsFptr();
   * 
   * static MetsStructLink metsStructLink = new MetsStructLink(); static MetsSmLink metsSmLink = new
   * MetsSmLink();
   */

  // @BeforeClass
  // public static void setUpBeforeClass() throws Exception {

  /*
   * recordCreationDate.setCreationDate("2016-02-18T11:15:19.899+01:00");
   * recordChangeDate.setRecordChangeDateValue("2018-02-02T12:12:34.113+01:00");
   * 
   * mpl.setPhysicalLocationDispayType("Discovery Place"); mpl.setPhysicalLocationType("discovery");
   * mpl.setPhysicalLocationValue("Tikal");
   * 
   * ml.addModsPhysicalLocation(mpl);
   * 
   * modsRecordInfo.addRecordChangeDate(recordChangeDate);
   * modsRecordInfo.addRecordCreationDate(recordCreationDate); modsRecordInfo.addRecordIdenfier(ri);
   * 
   * modsTypeOfResource.setTypeOfResourceValue("three dimensional object");
   * 
   * modsTitleInfo.setModsTitleInfoValue("Tikal, Column Altar 1");
   * modsTitleInfo.setModsTitleInfoLanguageValue("en");
   * 
   * dateCreated.setDateCreatedCalendarType("Longcount");
   * dateCreated.setDateCreatedValue("9.15.17.1.0.4"); modsOriginInfo.addDateCreated(dateCreated);
   * 
   * modsGenre.setGenreLanguage("en");
   * modsGenre.setGenreValueURI("http://idiom-projekt.de/voc/artefacttype/concept000230");
   * modsGenre.setGenreValue("columnar altar");
   * 
   * extent.setExtent("225cm (height), 90 cm (width), 50cm (thickness)");
   * form.setFormValue("http://idiom-projekt.de/voc/material/concept000129");
   * modsPhysicalDescription.addExtent(extent); modsPhysicalDescription.addForm(form);
   * 
   * modsRelatedItem.setRefToComposedObject("http://textgirdrep.de/123as");
   * modsRelatedItem.setType("constituent");
   * 
   * modsNote.setType("transcription"); modsNote.
   * setValue("waxak cauac lajcha' yax k' in chumwaan ta ajawel lik' muy muwaan yeht k'aba'il u mam"
   * );
   * 
   * modsAccessCondition.setRefToLicense("https://creativecommons.org/licenses/by/4.0/");
   * modsAccessCondition.setType("use and reproduction");
   * 
   * 
   * mods.addRecordInfoToMods(modsRecordInfo); mods.addModsTypeOfResource(modsTypeOfResource);
   * mods.addModsLocation(ml); mods.addModsTitleInfo(modsTitleInfo);
   * mods.addModsOriginInfo(modsOriginInfo); mods.addModsGenre(modsGenre);
   * mods.addModsPhysicalDescription(modsPhysicalDescription);
   * mods.addModsRelatedItem(modsRelatedItem); mods.addModsNote(modsNote);
   * mods.addModsAccessCondition(modsAccessCondition);
   * 
   * 
   * metsHdr.setMetsHdrCreationDate("2018-03-22T09:49:07");
   * 
   * metsDmdSec.setId("dmd_12345");
   * 
   * metsMdWrap.setMdtype("MODS"); metsMdWrap.setMimeType("text/xml");
   * 
   * metsDmdSec.addMdWrap(metsMdWrap);
   * 
   * metsMdWrap.addXmlData(metsXmlData);
   * 
   * metsXmlData.addMods(mods);
   * 
   * metsAmdSec.setID("amd_12345"); metsRightsMD.setID("rights_12345");
   * metsAmdSec.addMetsRightsMD(metsRightsMD); metsMdWrapWithOtherMdType.setMimeType("text/xml");
   * metsMdWrapWithOtherMdType.setMdtype("OTHER");
   * metsMdWrapWithOtherMdType.setOtherMdType("DVRIGHTS");
   * metsRightsMD.addMetsMdWrap(metsMdWrapWithOtherMdType);
   * 
   * metsOwner.setOwnerValue("Text Database and Dictionary of Classic Mayan");
   * metsOwnerSiteURL.setMetsOwnerSiteURLValue("http://www.mayadictionary.de"); metsRightsSponsor.
   * setSponsorValue("Nordrhein-Westfälische Akademie der Wissenschaften und der Künste, Union der Wissenschaftsakademien"
   * );
   * 
   * metsMdWrapWithOtherMdType.addXmlData(metsXmlDataForRights);
   * 
   * metsRights.addMetsOwner(metsOwner); metsRights.addMetsRightsSponsor(metsRightsSponsor);
   * metsRights.addMetsOwnerSiteURL(metsOwnerSiteURL);
   * 
   * metsXmlDataForRights.addMetsRights(metsRights);
   * 
   * metsDigiProvMD.setID("digiprov_12345"); metsMdWrapForDigiProv.setMimeType("text/xml");
   * metsMdWrapForDigiProv.setMdtype("OTHER"); metsMdWrapForDigiProv.setOtherMdType("DVLINKS");
   * metsDigiProvMD.addMetsMdWrap(metsMdWrapForDigiProv);
   * metsMdWrapForDigiProv.addXmlData(metsXmlDataForDigiProv);
   * metsXmlDataForDigiProv.addMetsLinks(metsLinks);
   * metsReference.setLinktext("Portal Classic Mayan");
   * metsReference.setReferenceValue("https://classicmayan.org/12mda");
   * metsLinks.addMetsReference(metsReference); metsAmdSec.addMetsDigiProvMD(metsDigiProvMD);
   * 
   * metsFileGrp.setUse("THUMBS"); metsFileSec.addMetsFileGrp(metsFileGrp);
   * metsFile.setId("file_12345_t1"); metsFile.setMimeType("image/jpeg");
   * metsFile.setCreated("2012-08-17T09:14:11Z"); metsFileGrp.addMetsFile(metsFile);
   * metsFlocat.setLocType("URL");
   * metsFlocat.setXlinkHref("https://classicmayan.kor.de.dariah.eu/t1aab");
   * metsFile.addMetsFlocat(metsFlocat);
   * 
   * metsStructMap.setType("PHYSICAL"); metsDiv.setId("all_9876"); metsDiv.setType("physSequence");
   * metsDivSecondLayer.setId("phys_12345_f1"); metsDivSecondLayer.setType("page");
   * metsDivSecondLayer.setOrder("1"); metsFptr.setFileID("file_12345_t1");
   * 
   * metsDivSecondLayer.addMetsFptr(metsFptr); metsDiv.addMetsDiv(metsDivSecondLayer);
   * metsStructMap.addMetsDiv(metsDiv);
   * 
   * metsSmLink.setXlinkFrom("log_12345_m9"); metsSmLink.setXlinkTo("file_12345_t1");
   * metsStructLink.addMetsSmLink(metsSmLink);
   * 
   * mets.addMetsHeader(metsHdr); mets.addMetsDmdSec(metsDmdSec); mets.addMetsFileSec(metsFileSec);
   * mets.addMetsAmdSec(metsAmdSec); mets.addMetsStructMap(metsStructMap);
   * mets.addMetsStructLink(metsStructLink);
   */
  // }

  /**
   * @throws ParseException
   */
  @Test
  @Ignore
  public void testIsDateAlreadyInCorrectFormat() throws ParseException {

    RecordCreationDate date = new RecordCreationDate("iso8601", "2021-09-23T13:10:48.402+02:00");
    System.out.println(date.getXML());

  }

  /**
   * @throws ParseException
   */
  @Test
  @Ignore
  public void testListOfModEntities() throws ParseException {
    MetsXmlData xmlData = new MetsXmlData();

    for (int i = 0; i < 4; i++) {
      xmlData.addMods(new Mods()).addRecordInfoToMods(new ModsRecordInfo(
          new RecordIdentifier("TEST"),
          new RecordCreationDate("iso8601", "2021-09-23T13:10:48.402+02:00"),
          new RecordChangeDate("iso8601", "2021-09-23T13:10:48.402+02:00")));
    }

    System.out.println(xmlData.getXML());
  }

  /**
   * @throws ParseException
   */
  @Test
  @Ignore
  public void testListTitles() throws ParseException {

    MetsXmlData xmlData = new MetsXmlData();
    Mods mods = new Mods();
    for (int i = 0; i < 4; i++) {
      mods.addModsTitleInfo(new ModsTitleInfo()).addTitle(new Title("Hallo", "en"));
    }

    System.out.println("TITLES:");
    System.out.println(xmlData.addMods(mods).getXML());
  }

  /**
   * 
   */
  @Test
  @Ignore
  public void testListOfAccessConditions() {

    ModsRelatedItem relItem = new ModsRelatedItem();

    for (int i = 0; i < 4; i++) {
      relItem.addModsAccessCondition(new ModsAccessCondition("use and reproduction", "kor_license"))
          .addModsCopyRight(new ModsCopyRight("copyrighted"))
          .addModsGeneralNote(
              new ModsGeneralNote("The knowledge of humanity belongs to the world"));
    }

    System.out.println(relItem.getXML());

    ModsRelatedItem relItemSingle = new ModsRelatedItem();
    relItemSingle.addModsAccessCondition(new ModsAccessCondition("", "", ""));
    System.out.println(relItemSingle.getXML());
  }

  /**
   * 
   */
  @Test
  public void testModsResevedAccessCondition() {

    ModsAccessCondition reservedCopyrights = new ModsAccessCondition();
    ModsCopyRight copyRightsForReserverdAccessCondition = new ModsCopyRight(
        new CopyrightStats("copyrighted"),
        new PublicationStatus("unpublished"));
    reservedCopyrights.addModsCopyRight(copyRightsForReserverdAccessCondition);

    System.out.println(reservedCopyrights.getXML());
  }

  /**
   * @throws JSONException
   * @throws ParseException
   * @throws IOException
   */
  @Test
  public void testCompleteMetsMods() throws JSONException, ParseException, IOException {
    ClassicMayanMetsMods cmmm = new ClassicMayanMetsMods("textgrid:254w6",
        "2016-02-18T11:15:19.899+01:00", "2018-02-02T12:12:34.113+01:00");
    System.out.println(cmmm.getMets());
  }

  /**
   * @throws JSONException
   * @throws IOException
   * @throws ParseException
   */
  @Test
  public void testAddAmdSecForImages() throws JSONException, IOException, ParseException {
    ClassicMayanMetsMods cmmm = new ClassicMayanMetsMods("textgrid:3vvn7",
        "2016-02-18T11:15:19.899+01:00", "2018-02-02T12:12:34.113+01:00");
    System.out.println(cmmm.getMets());
  }

  /**
   * @throws JSONException
   * @throws IOException
   * @throws ParseException
   */
  @Test
  public void testImageAsRelatedItem() throws JSONException, IOException, ParseException {
    ClassicMayanMetsMods cmmm = new ClassicMayanMetsMods("textgrid:3qh1z",
        "2016-02-18T11:15:19.899+01:00", "2018-02-02T12:12:34.113+01:00");
    System.out.println(cmmm.getMets());
  }

  /**
   * @throws JSONException
   * @throws IOException
   */
  @Test
  @Ignore
  public void testGetDataFromConedaKor() throws JSONException, IOException {
    for (String mediaID : TripleStoreQuery.getLinkedMediaInKorFromArtefact("textgrid:3bxs0")) {
      System.out.println(ConedaKorQueries.getMediumMetadaSetForMetsMods(mediaID).toString(2));
    }
  }

  /*
   * @Test
   * 
   * @Ignore public void testModsRecordIdentifier() {
   * 
   * System.out.println("------------------------------------------");
   * System.out.println("Test the XML Output of Mods Record Identifier");
   * System.out.println(ri.getRecordIdentifierXML());
   * System.out.println("------------------------------------------\n"); }
   */

  /**
   * 
   */
  @Test
  public void testPlaceHierarchy() {
    System.out.println(Queries.hierarichalPlaceQuery("textgrid:25895"));
  }

  /**
   * @throws ParseException
   */
  @Test
  @Ignore
  public void testModsRecordCreationDate() throws ParseException {
    System.out.println("------------------------------------------");
    System.out.println("Test the XML Output of Mods Record Creation Date");
    System.out.println(new RecordCreationDate("iso8601", "2021-09-23T13:10:48.402+02:00").getXML());
    System.out.println("------------------------------------------\n");
  }

  /**
   * @throws ParseException
   */
  @Test
  @Ignore
  public void testModsRecordChangeDate() throws ParseException {
    System.out.println("------------------------------------------");
    System.out.println("Test the XML Output of Mods Record Change Date");
    System.out.println(new RecordCreationDate("iso8601", "2021-09-23T13:10:48.402+02:00").getXML());
    System.out.println("------------------------------------------\n");
  }

  /**
   * @throws ParseException
   */
  @Test
  @Ignore
  public void testModsRecordInfo() throws ParseException {
    System.out.println("------------------------------------------");
    System.out.println("Test the XML Output of Mods Record Info");
    System.out.println(new ModsRecordInfo(
        new RecordIdentifier("TEST"),
        new RecordCreationDate("iso8601", "2021-09-23T13:10:48.402+02:00"),
        new RecordChangeDate("iso8601", "2021-09-23T13:10:48.402+02:00")).getXML());
    System.out.println("------------------------------------------\n");
  }

  /**
   * 
   */
  @Test
  @Ignore
  public void testModsPhysicalLocation() {
    ModsPhysicalLocation physicalLocation =
        new ModsPhysicalLocation("archaeologicalAre", "arch", "Tikal");
    System.out.println("------------------------------------------");
    System.out.println("Test the XML Output of Physical Location");
    System.out.println(physicalLocation.getPhysicalLocationXML());
    System.out.println("------------------------------------------\n");
  }

  /**
   * 
   */
  @Test
  public void testQueryGetLocation() {
    System.out.println(Queries.getLocationData("textgrid:3vpqc").getDiscoveryPlace());
  }

  /**
   * Test bug #65. Yes, we can catch NSEEs! Fine!
   * 
   * TODO ...but: is this the correct way to handle NSEEs?? Maybe they are Runtime Exceptions??
   */
  @Test
  public void testQueryGetTitle() {
    try {
      System.out.println(Queries.getArtefactTitle("textgrid:40qcn"));
    } catch (NoSuchElementException e) {
      System.out.println("error! " + e.getClass().getName() + ": " + e.getMessage());
    }
  }

  /**
   * 
   */
  @Test
  public void testAllLocations() {

    List<String> artefactList = new ArrayList<String>();
    Queries queries = new Queries();
    artefactList = queries.getArtefactList();
    int i = 1;
    List<String> nullPlaces = new ArrayList<String>();
    for (String artefact : artefactList) {
      System.out.println(i + " of " + artefactList.size());
      if (Queries.getLocationData(artefact).getDiscoveryPlace() == null) {
        nullPlaces.add(artefact + ";" + Queries.getArtefactTitle(artefact));
      }
      i++;
    }

    for (String artfact : nullPlaces) {
      System.out.println(artfact);
    }
  }

  // @Test
  // @Ignore
  /*
   * public void testModsLocation() {
   * 
   * ml.addModsPhysicalLocation(mpl);
   * System.out.println("------------------------------------------");
   * System.out.println("Test the XML Output of Location");
   * System.out.println(ml.getModsLocationXML());
   * System.out.println("------------------------------------------\n"); }
   * 
   * @Test
   * 
   * @Ignore public void testModsTypeOfResource() {
   * 
   * System.out.println("------------------------------------------");
   * System.out.println("Test the XML Output of Mods Type of Resource");
   * System.out.println(modsTypeOfResource.getTypeOfReosourceXML());
   * System.out.println("------------------------------------------\n"); }
   * 
   * @Test
   * 
   * @Ignore public void testModsTitleInfo() {
   * System.out.println("------------------------------------------");
   * System.out.println("Test the XML Output of Mods Title Info");
   * System.out.println(modsTitleInfo.getModsTitleInfoXML());
   * System.out.println("------------------------------------------\n"); }
   * 
   * @Test
   * 
   * @Ignore public void testModsOriginInfo() {
   * System.out.println("------------------------------------------");
   * System.out.println("Test the XML Output of Mods Origin Info");
   * System.out.println(modsOriginInfo.getModsOriginInfoXML());
   * System.out.println("------------------------------------------\n"); }
   * 
   * @Test
   * 
   * @Ignore public void testModsGenre() {
   * System.out.println("------------------------------------------");
   * System.out.println("Test the XML Output of Mods Genre");
   * System.out.println(modsGenre.getModsGenreXML());
   * System.out.println("------------------------------------------\n"); }
   * 
   * @Test
   * 
   * @Ignore public void testModsPhysicalDescription() {
   * System.out.println("------------------------------------------");
   * System.out.println("Test the XML Output of Mods Physical Description");
   * System.out.println(modsPhysicalDescription.getModsPhysicalDescriptionXML());
   * System.out.println("------------------------------------------\n"); }
   * 
   * @Test
   * 
   * @Ignore public void testModsRealtedItem() {
   * System.out.println("------------------------------------------");
   * System.out.println("Test the XML Output of Mods Related Item");
   * System.out.println(modsRelatedItem.getModsRelatedItemXML());
   * System.out.println("------------------------------------------\n"); }
   * 
   * @Test
   * 
   * @Ignore public void testModsNote(){
   * System.out.println("------------------------------------------");
   * System.out.println("Test the XML Output of Mods Note");
   * System.out.println(modsNote.getModsNoteXML());
   * System.out.println("------------------------------------------\n"); }
   * 
   * @Test
   * 
   * @Ignore public void testModsAccessCondition(){
   * System.out.println("------------------------------------------");
   * System.out.println("Test the XML Output of Mods Access Condition");
   * System.out.println(modsAccessCondition.getXML());
   * System.out.println("------------------------------------------\n"); }
   * 
   * @Test
   * 
   * @Ignore public void testMods() throws ParseException {
   * 
   * 
   * System.out.println("------------------------------------------");
   * System.out.println("Test the XML Output of Mods"); System.out.println(mods.getModsXML());
   * System.out.println("------------------------------------------\n"); }
   * 
   * 
   * @Test
   * 
   * @Ignore public void testMetsHeader(){
   * System.out.println("------------------------------------------");
   * System.out.println("Test the XML Output of Mets Hdr");
   * System.out.println(metsHdr.getMetsHdrXML());
   * System.out.println("------------------------------------------\n"); }
   * 
   * @Test
   * 
   * @Ignore public void testMetsDmdSec() throws ParseException{
   * System.out.println("------------------------------------------");
   * System.out.println("Test the XML Output of Mets DmdSec");
   * System.out.println(metsDmdSec.getMetsDmdDecXML());
   * System.out.println("------------------------------------------\n"); }
   * 
   * @Test
   * 
   * @Ignore public void testMetsMdWrap() throws ParseException{
   * System.out.println("------------------------------------------");
   * System.out.println("Test the XML Output of Mets MdWrap");
   * System.out.println(metsMdWrap.getMetsMdWrapXML());
   * System.out.println("------------------------------------------\n"); }
   * 
   * @Test
   * 
   * @Ignore public void testMetsXmlData() throws ParseException{
   * System.out.println("------------------------------------------");
   * System.out.println("Test the XML Output of Mets Xml Data");
   * System.out.println(metsXmlData.getXmlDataXML());
   * System.out.println("------------------------------------------\n"); }
   * 
   * @Test
   * 
   * @Ignore public void testMetsAMdSec() throws ParseException{
   * System.out.println("------------------------------------------");
   * System.out.println("Test the XML Output of Mets AmdSec");
   * System.out.println(metsAmdSec.getMetsAmdSecXML());
   * System.out.println("------------------------------------------\n"); }
   * 
   * @Test
   * 
   * @Ignore public void testMetsRightsMD() throws ParseException{
   * System.out.println("------------------------------------------");
   * System.out.println("Test the XML Output of Mets RightsMD");
   * System.out.println(metsRightsMD.getMetsRightMDXML());
   * System.out.println("------------------------------------------\n"); }
   * 
   * @Test
   * 
   * @Ignore public void testMetsMdWrapForRights() throws ParseException{
   * System.out.println("------------------------------------------");
   * System.out.println("Test the XML Output of Mets MD Wrap for Rights");
   * System.out.println(metsMdWrapWithOtherMdType.getMetsMdWrapXML());
   * System.out.println("------------------------------------------\n"); }
   * 
   * 
   * @Test
   * 
   * @Ignore public void testMets() throws ParseException{
   * 
   * System.out.println("------------------------------------------");
   * System.out.println("Test the XML Output of Mets"); System.out.println(mets.getMetsXML());
   * System.out.println("------------------------------------------\n"); }
   */

  /**
   * Test to build a mets object by giving an URI and using the queries to the sparql endpoint
   */

  /**
   * @throws ParseException
   */
  @Test
  @Ignore
  public void testModsFromTriplestore() throws ParseException {
    /*
     * cmmm.setTitleInfoData(); cmmm.setTypeOfResource(); cmmm.setRecordInfo();
     * cmmm.setLocationData(); cmmm.setArtefactCreationDate(); cmmm.setArtefactGenre();
     * cmmm.setPhysicalDescription(); cmmm.setRelatedItems();
     * cmmm.setModsTranscriptionAndTranslation(); cmmm.setAccessCondition();
     * 
     * System.out.println(cmmm.getMods());
     */
  }

  /**
   * @throws ParseException
   * @throws JSONException
   * @throws IOException
   */
  @Test
  @Ignore
  public void testAllObjects() throws ParseException, JSONException, IOException {

    List<String> artefactList = new ArrayList<String>();
    Queries queries = new Queries();
    artefactList = queries.getArtefactList();
    for (int j = 0; j < artefactList.size(); j++) {
      System.out.println(j + " / " + artefactList.size() + " : " + artefactList.get(j));
      ClassicMayanMetsMods bla = new ClassicMayanMetsMods(artefactList.get(j),
          "2016-02-18T11:15:19.899+01:00", "2018-02-02T12:12:34.113+01:00");

      System.out.println(bla.getMets());
    }
  }

  /**
   * @throws ParseException
   * @throws JSONException
   * @throws IOException
   */
  @Test
  @Ignore
  public void testMetsFromTriplestore() throws ParseException, JSONException, IOException {
    /*
     * cmmm.setTitleInfoData(); cmmm.setTypeOfResource(); cmmm.setRecordInfo();
     * cmmm.setLocationData(); cmmm.setArtefactCreationDate(); cmmm.setArtefactGenre();
     * cmmm.setPhysicalDescription(); cmmm.setRelatedItems();
     * cmmm.setModsTranscriptionAndTranslation(); cmmm.setAccessCondition();
     * System.out.println(cmmm.getMets());
     */
  }

  /**
   * @throws ParseException
   * @throws JSONException
   * @throws IOException
   */
  @Test
  @Ignore
  public void testClassicMayanMetsMOds() throws ParseException, JSONException, IOException {
    ClassicMayanMetsMods testMetsFile = new ClassicMayanMetsMods("textgrid:2stwg",
        "2016-02-18T11:15:19.899+01:00", "2018-02-02T12:12:34.113+01:00");
    System.out.println(testMetsFile.getMets());
  }

  /**
   * @throws JSONException
   * @throws ParseException
   * @throws IOException
   */
  @Test
  @Ignore
  public void testTitleInfoFromTriplestore() throws JSONException, ParseException, IOException {
    // System.out.println(Queries.getArtefactTitle("textgrid:2sg18"));
    // cmmm.setTitleInfoData();
    System.out.println(new ClassicMayanMetsMods("textgrid:2stwg", "2016-02-18T11:15:19.899+01:00",
        "2018-02-02T12:12:34.113+01:00").getMets());
  }

  /**
   * 
   */
  @Test
  @Ignore
  public void testTitleInfo() {
    // System.out.println(cmmm.getTitleInfoData());
  }

  /**
   * 
   */
  @Test
  @Ignore
  public void testPhysicalLocationFromTriplestore() {
    // cmmm.setLocationData();
  }

  /**
   * 
   */
  @Test
  @Ignore
  public void testPhysicalLocation() {
    // System.out.println(cmmm.getLocationData());
  }

  /**
   * 
   */
  @Test
  @Ignore
  public void testOriginInfo() {
    // cmmm.setArtefactCreationDate();
    // System.out.println(cmmm.getArtefactCreationDate());
  }

  /**
   * 
   */
  @Test
  @Ignore
  public void testGenreInfo() {
    // cmmm.setArtefactGenre();
    // System.out.println(cmmm.getArtefactGenre());
  }

  /**
   * 
   */
  @Test
  @Ignore
  public void testPhysicalDescription() {
    // cmmm.setPhysicalDescription();
    // System.out.println(cmmm.getPhysicalDescription());
  }

  /**
   * 
   */
  @Test
  @Ignore
  public void testNote() {
    // cmmm.setModsTranscriptionAndTranslation();
  }

  /**
   * 
   */
  @Test
  @Ignore
  public void testAccessCondition() {
    // cmmm.setAccessCondition();
  }

}
