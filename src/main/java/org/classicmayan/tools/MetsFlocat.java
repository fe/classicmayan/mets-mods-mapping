package org.classicmayan.tools;

/**
 * 
 * @author Maximilian Brodhun State- and University Library Goettingen
 * @version 1.0.0
 * @since 23.11.2018
 */

public class MetsFlocat {
	private String locType;
	private String xlinkHref;
	
	public MetsFlocat(String locType, String xlinkHref) {
		this.locType = locType;
		this.xlinkHref = xlinkHref;
	}
	
	public String getLocType() {
		return locType;
	}
	public void setLocType(String locType) {
		this.locType = locType;
	}
	public String getXlinkHref() {
		return xlinkHref;
	}
	public void setXlinkHref(String xlinkHref) {
		this.xlinkHref = xlinkHref;
	}
	
	public String getMetsFlocatXML() {
		return "<FLocat LOCTYPE=\"" + getLocType() + "\" xlink:href=\"" + getXlinkHref() + "\"/>";
	}
	
}
