package org.classicmayan.tools;

import java.util.ArrayList;
import java.util.List;

/**
 * 
 * @author Maximilian Brodhun State- and University Library Goettingen
 * @version 1.0.0
 * @since 23.11.2018
 */

public class MetsLinks {

	MetsReference metsReference;
	List<MetsReference> metsReferences = new ArrayList<MetsReference>();
	
	public MetsReference addMetsReference(MetsReference metsReference) {
		metsReferences.add(metsReference);
		this.metsReference = metsReference;
		return metsReference;
	}
	
	public String getMetsLinksXML() {
		return "<links>" + metsReference.buildAllReferences() + "</links>";
	}

	public String getXML(){
		String xml = "";
		for(MetsReference metsReference : metsReferences){
			xml += metsReference.getXML();
		}
		return xml;
	}
}
