package org.classicmayan.tools;

public class ConedaKorID {

    private String id;

    public ConedaKorID(String id){
        this.id = id;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

}
