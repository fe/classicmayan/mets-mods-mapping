/*******************************************************************************
 * This software is copyright (c) 2021 by
 * 
 * State and University Library Goettingen
 *
 * This is free software. You can redistribute it and/or modify it under the terms described in the
 * GNU Lesser General Public License v3 of which you should have received a copy. Otherwise you can
 * download it from
 *
 * http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * @copyright State and University Library Goettingen
 * @license GNU Lesser General Public License v3 (http://www.gnu.org/licenses/lgpl-3.0.txt)
 * @author Maximilian Behnert-Brodhun (behnert-brodhun@sub.uni-goettingen.de)
 ******************************************************************************/



package org.classicmayan.tools;

import java.util.ArrayList;
import java.util.List;

public class ModsCopyRight {
	
	private String status;
	private CopyrightStats copyrightStatus;
	private PublicationStatus publicationStatus;
	
	public ModsGeneralNote modsGeneralNote;
	private List<ModsGeneralNote> modsGeneralNotesList = new ArrayList<ModsGeneralNote>();
	private String xmlCode;
	private final String xmlns = "xmlns=\"http://www.cdlib.org/inside/diglib/copyrightMD\"";
	private final String xsiSchemaLocation = "xsi:schemaLocation=\"http://www.cdlib.org/inside/diglib/copyrightMD http://www.cdlib.org/groups/rmg/docs/copyrightMD.xsd\"";
	
	public ModsCopyRight() {}
	
	public String getXsiSchemaLocation() {
		return xsiSchemaLocation;
	}

	public String getXmlns() {
		return xmlns;
	}

	public ModsCopyRight(String status) {
		this.status = status;	
	}
	
	public ModsCopyRight(CopyrightStats copyrightStatus, PublicationStatus publicationStatus){
		setCopyrightStatus(copyrightStatus);
		setPublicationStatus(publicationStatus);
		setXmlCode("<copyright" +
			" copyright.status=\"" + copyrightStatus.getStatus() + "\"" +
			" publication.status=\"" + publicationStatus.getStatus()+ "\"" +
			" " + getXmlns() + 
			" " + getXsiSchemaLocation() + ">");
	}
	public ModsGeneralNote addModsGeneralNote(ModsGeneralNote modsGeneralNote) {
		modsGeneralNotesList.add(modsGeneralNote);
		this.modsGeneralNote = modsGeneralNote;		
		return modsGeneralNote;
	}

	public PublicationStatus getPublicationStatus() {
		return publicationStatus;
	}

	public void setPublicationStatus(PublicationStatus publicationStatus) {
		this.publicationStatus = publicationStatus;
	}

	public CopyrightStats getCopyrightStatus() {
		return copyrightStatus;
	}

	public void setCopyrightStatus(CopyrightStats copyrightStatus) {
		this.copyrightStatus = copyrightStatus;
	}


	public String getModsCopyRightXML() {
		
		return "<copyright copyright.status=\"" + this.getStatus() + "\">"
				+ this.modsGeneralNote.getXML()
				+ "</copyright>";
	}
	
	public String getXMLFine(){
		String xml="";
		for(ModsGeneralNote generalNote : modsGeneralNotesList){
			xml += generalNote.getXML();
		}
		return getXmlCode() + xml + "</copyright>";
	}

	public String getXML() {
		
		String generalNoteXml = "";
		
		for(ModsGeneralNote modsGeneralNote : modsGeneralNotesList) {
			generalNoteXml += modsGeneralNote.getXML();
		}
		
		return "<copyright copyright.status=\"" + this.getStatus() + "\">"
				+ generalNoteXml
				+ "</copyright>";
	}
	
	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getXmlCode() {
		return xmlCode;
	}

	public void setXmlCode(String xmlCode) {
		this.xmlCode = xmlCode;
	}
	
}
