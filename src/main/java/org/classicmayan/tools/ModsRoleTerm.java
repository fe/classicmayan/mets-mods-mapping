/*******************************************************************************
 * This software is copyright (c) 2021 by
 * 
 * State and University Library Goettingen
 *
 * This is free software. You can redistribute it and/or modify it under the terms described in the
 * GNU Lesser General Public License v3 of which you should have received a copy. Otherwise you can
 * download it from
 *
 * http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * @copyright State and University Library Goettingen
 * @license GNU Lesser General Public License v3 (http://www.gnu.org/licenses/lgpl-3.0.txt)
 * @author Maximilian Behnert-Brodhun (behnert-brodhun@sub.uni-goettingen.de)
 ******************************************************************************/



package org.classicmayan.tools;

public class ModsRoleTerm {
	private String roleTermValue;
	private String type;
	private String authority;
	
	public ModsRoleTerm(){

	}

	public ModsRoleTerm (String type, String authority, String value){
		this.setType(type);
		this.setAuthority(authority);
		this.setRoleTermValue(value);
	}

	public String getModsRoleTermXML() {
		return "<roleTerm type=\"" + this.getType() + "\" authority=\"" + this.getAuthority() + "\">" + this.getRoleTermValue() + "</roleTerm>";
	}
	
	public String getRoleTermValue() {
		return roleTermValue;
	}

	public void setRoleTermValue(String roleTermValue) {
		this.roleTermValue = roleTermValue;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getAuthority() {
		return authority;
	}

	public void setAuthority(String authority) {
		this.authority = authority;
	}	
}