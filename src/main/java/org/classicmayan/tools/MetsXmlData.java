package org.classicmayan.tools;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

/**
 * 
 * @author Maximilian Brodhun State- and University Library Goettingen
 * @version 1.0.0
 * @since 21.11.2018
 */

public class MetsXmlData {

	public Mods mods;
	MetsLinks metsLinks;
	MetsRights metsRights;
	List<Mods> modsEntities = new ArrayList<Mods>();
	List<MetsRights> rights = new ArrayList<MetsRights>();
	List<MetsLinks> links = new ArrayList<MetsLinks>();

	public Mods addMods(Mods mods) {
		modsEntities.add(mods);
		this.mods = mods;
		return mods;
	}
	
	public MetsLinks addMetsLinks(MetsLinks metsLinks) {
		this.links.add(metsLinks);
		this.metsLinks = metsLinks;
		return metsLinks;
	}
	
	public MetsRights addMetsRights(MetsRights metsRights) {
		this.rights.add(metsRights);
		this.metsRights = metsRights;
		return metsRights;
	}
	
	public String getXmlDataXML() throws ParseException {
		if(mods != null) {			
			return "<xmlData>" + mods.getModsXML() + "</xmlData>";
		}else if(metsRights != null){
			return "<xmlData>" + metsRights.getMetsRightsXML() + "</xmlData>";
		}else {
			return "<xmlData>" + metsLinks.getXML() + "</xmlData>";
		}
	}
	
	public String getXML() throws ParseException {
		String xml="";
		for(Mods mods : modsEntities) {
			xml += mods.getXML();
		}
		for(MetsRights right : rights){
			xml += right.getMetsRightsXML();
		}
		for(MetsLinks links : links){
			xml += links.getXML();
		}
		return "<xmlData>" + xml + "</xmlData>";
	}
	
}
