package org.classicmayan.tools;

import java.util.ArrayList;
import java.util.List;

/**
 * 
 * @author Maximilian Brodhun State- and University Library Goettingen
 * @version 1.0.0
 * @since 22.11.2018
 */

public class MetsReference {
	private String linkText;
	private String referenceValue;
	
	private List<MetsReference> references = new ArrayList<MetsReference>();
	
	public MetsReference(List<MetsReference> references) {
		this.references = references;
	}
	
	public MetsReference() {
		
	}
	
	public MetsReference(String linkText, String referenceValue) {
		this.linkText = linkText;
		this.referenceValue = referenceValue;
	}
	
	public void addMetsReference(MetsReference metsReference) {
		references.add(metsReference);
	}
	
	public String getLinkText() {
		return linkText;
	}
	public void setLinktext(String linktext) {
		this.linkText = linktext;
	}
	public String getReferenceValue() {
		return referenceValue;
	}
	public void setReferenceValue(String referenceValue) {
		this.referenceValue = referenceValue;
	}
	
	public String getMetsReferenceXML() {
		return "<reference linktext=\"" + getLinkText() + "\">" + getReferenceValue() + "</reference>";
	}
	
	public String getXML() {
		return "<reference linktext=\"" + this.getLinkText() + "\">" + this.getReferenceValue() + "</reference>";
	}


	public String buildAllReferences() {

		String references="";
		
		for(MetsReference reference : this.references){
			references = references.concat(reference.getMetsReferenceXML());
		}
		return references;
	}
}
