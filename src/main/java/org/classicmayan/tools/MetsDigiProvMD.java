package org.classicmayan.tools;

import java.text.ParseException;

/**
 * 
 * @author Maximilian Brodhun State- and University Library Goettingen
 * @version 1.0.0
 * @since 21.11.2018
 */

public class MetsDigiProvMD {
	private String ID;
	MetsMdWrap metsMdWrap;
	
	public String getID() {
		return ID;
	}

	public void setID(String iD) {
		ID = iD;
	}
	
	public MetsDigiProvMD(String ID){
		this.ID = ID;
	}
	
	MetsMdWrap addMetsMdWrap(MetsMdWrap metsMdWrap) {
		this.metsMdWrap = metsMdWrap;
		return metsMdWrap;
	}
	
	/*public String getMetsDigiProvMDXML() throws ParseException {
		return "<digiprovMD ID=\"" + getID() + "\">" + 
					metsMdWrap.getMetsMdWrapXML() + 
			   "</digiprovMD>";
	}*/

	public String getXML() throws ParseException {
		return "<digiprovMD ID=\"" + getID() + "\">" + 
					metsMdWrap.getXML() + 
			   "</digiprovMD>";
	}

}
