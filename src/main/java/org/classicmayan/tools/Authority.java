package org.classicmayan.tools;

public class Authority {
    private String value;
    private final String attributeName = "authority=";

    public Authority(String value){
        setValue(value);
    }

    public String getValue() {
        return value;
    }
    public void setValue(String value) {
        this.value = value;
    }
    public String getAttributeName() {
        return attributeName;
    }
    public String getAuthority(){
        return  getAttributeName() + "\"" + getValue() + "\"";
    }
}
