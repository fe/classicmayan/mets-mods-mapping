package org.classicmayan.tools;

import java.util.ArrayList;
import java.util.List;

/**
 * 
 * @author Maximilian Brodhun State- and University Library Goettingen
 * @version 1.0.0
 * @since 23.11.2018
 */

public class MetsSmLink {
	
	private String xlinkFrom;
	private String xlinkTo;
	private List<MetsSmLink> metsSMLinks = new ArrayList<MetsSmLink>();
	
	public MetsSmLink(List<MetsSmLink> metsSmLinks) {
		this.metsSMLinks = metsSmLinks;
	}
	
	public MetsSmLink(String xlinkFrom, String xlinkTo) {
		this.xlinkFrom = xlinkFrom;
		this.xlinkTo = xlinkTo;
	}
	
	public MetsSmLink() {
		
	}
	
	public String getXlinkFrom() {
		return xlinkFrom;
	}
	public void setXlinkFrom(String xlinkFrom) {
		this.xlinkFrom = xlinkFrom;
	}
	public String getXlinkTo() {
		return xlinkTo;
	}
	public void setXlinkTo(String xlinkTo) {
		this.xlinkTo = xlinkTo;
	}
	
	public String getMetsSmLinkXML() {
		return "<smLink xlink:from=\"" + getXlinkFrom() + "\" xlink:to=\"" + getXlinkTo() + "\"/>";
	}

	public String buildAllSmLInks() {

		String smLinks="";
		
		for(MetsSmLink smLink : this.metsSMLinks){
			smLinks = smLinks.concat(smLink.getMetsSmLinkXML());
		}
		return smLinks;
	}
	
	public String getXML() {
		
		return "<smLink xlink:from=\"" + getXlinkFrom() + "\" xlink:to=\"" + getXlinkTo() + "\"/>";
	}
	
}
