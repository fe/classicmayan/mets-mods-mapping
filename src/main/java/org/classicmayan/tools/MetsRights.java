package org.classicmayan.tools;

/**
 * 
 * @author Maximilian Brodhun State- and University Library Goettingen
 * @version 1.0.0
 * @since 22.11.2018
 */

public class MetsRights {
	
	MetsOwner metsOwner;
	MetsOwnerSiteURL metsOwnerSiteURL;
	MetsRightsSponsor metsRightsSponsor;
	
	public MetsRights(){

	}

	public MetsRights(MetsOwner owner, MetsOwnerSiteURL ownerSiteURL, MetsRightsSponsor rightsSponsor){

		this.setMetsOwner(owner);
		this.setMetsOwnerSiteURL(ownerSiteURL);
		this.setMetsRightsSponsor(rightsSponsor);
	}

	public MetsOwner addMetsOwner(MetsOwner metsOwner) {
		this.metsOwner = metsOwner;
		return metsOwner;
	}
	
	public MetsOwnerSiteURL addMetsOwnerSiteURL(MetsOwnerSiteURL metsOwnerSiteURL) {
		this.metsOwnerSiteURL = metsOwnerSiteURL;
		return metsOwnerSiteURL;
	}
	
	public MetsRightsSponsor addMetsRightsSponsor(MetsRightsSponsor metsRightsSponsor) {
		this.metsRightsSponsor = metsRightsSponsor;
		return metsRightsSponsor;
	}
	
	public void setMetsOwner(MetsOwner owner){
		this.metsOwner = owner;
	}

	public void setMetsOwnerSiteURL(MetsOwnerSiteURL ownerSiteURL){
		this.metsOwnerSiteURL = ownerSiteURL;
	}	

	public void setMetsRightsSponsor(MetsRightsSponsor rightsSponsor){
		this.metsRightsSponsor = rightsSponsor;
	}

	public String getMetsRightsXML() {
		return "<rights>" + 
					metsOwner.getMetsOwnerXML() + 
					metsOwnerSiteURL.getMetsOwnerSiteURLXML() + 
					metsRightsSponsor.getMetsRightsSponsorXML() + 
				"</rights>";
	}
}
