package org.classicmayan.tools;

import java.text.ParseException;

/**
 * @author Maximilian Brodhun, SUB Göttingen
 * @author Stefan E. Funk, SUB Göttingen
 */

public class ModsRecordInfo {

  // **
  // FINALS
  // **

  public final static String RECORD_INFO_START_TAG = "<recordInfo>";
  public final static String RECORD_INFO_CLOSE_TAG = "</recordInfo>";

  // **
  // CLASS
  // **

  public String textgridURI;
  public RecordIdentifier recordIdentifier;
  public RecordCreationDate recordCreationDate;
  public RecordChangeDate recordChangeDate;

  /**
   * @param recordIdentifier
   * @param recordCreationDate
   * @param recordChangeDate
   */
  public ModsRecordInfo(RecordIdentifier recordIdentifier, RecordCreationDate recordCreationDate,
      RecordChangeDate recordChangeDate) {
    this.recordIdentifier = recordIdentifier;
    this.recordCreationDate = recordCreationDate;
    this.recordChangeDate = recordChangeDate;
  }

  /**
   * @param ri
   */
  public void addRecordIdenfier(RecordIdentifier ri) {
    this.recordIdentifier = ri;
  }

  /**
   * @param rcd
   */
  public void addRecordCreationDate(RecordCreationDate rcd) {
    this.recordCreationDate = rcd;
  }

  /**
   * @param rcd
   */
  public void addRecordChangeDate(RecordChangeDate rcd) {
    this.recordChangeDate = rcd;
  }

  /**
   * @return
   * @throws ParseException
   */
  public String getRecordInfoXML() throws ParseException {
    return RECORD_INFO_START_TAG +
        this.recordIdentifier.getRecordIdentifierXML() +
        this.recordCreationDate.getRecordCreationDateXML() +
        this.recordChangeDate.getRecordChangeDateXML() +
        RECORD_INFO_CLOSE_TAG;
  }

  /**
   * @return
   * @throws ParseException
   */
  public String getXML() throws ParseException {
    return getRecordInfoXML();
  }

}
