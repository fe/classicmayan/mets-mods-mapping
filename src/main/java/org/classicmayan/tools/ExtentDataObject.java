package org.classicmayan.tools;

/**
 *
 */
public class ExtentDataObject {

  // Basic values from RDF.
  private String height;
  private String width;
  private String thickness;
  private String unit;
  // More sophisticated values from RDF.
  private String heightLowCarving;
  private String heightSculptuedArea;
  private String heightButt;
  private String exposureAboveFloor;
  private String widthBaseCarving;
  private String widthSculpturedArea;
  private String depthRelief;
  private String diameter;
  private String perimeter;
  // Not used in VL.
  // private String hasUnitWeight;
  private String weight;

  /**
   * @param height
   * @param width
   * @param thickness
   * @param unit
   * @param heightLowCarving
   * @param heightSculptuedArea
   * @param heightButt
   * @param exposureAboveFloor
   * @param widthBaseCarving
   * @param widthSculpturedArea
   * @param depthRelief
   * @param diameter
   * @param perimeter
   * @param weight
   */
  public ExtentDataObject(String height, String width, String thickness, String unit,
      String heightLowCarving, String heightSculptuedArea, String heightButt,
      String exposureAboveFloor, String widthBaseCarving, String widthSculpturedArea,
      String depthRelief, String diameter, String perimeter, String weight) {
    this.height = height;
    this.width = width;
    this.thickness = thickness;
    this.unit = unit;
    this.heightLowCarving = heightLowCarving;
    this.heightSculptuedArea = heightSculptuedArea;
    this.heightButt = heightButt;
    this.exposureAboveFloor = exposureAboveFloor;
    this.widthBaseCarving = widthBaseCarving;
    this.widthSculpturedArea = widthSculpturedArea;
    this.depthRelief = depthRelief;
    this.diameter = diameter;
    this.perimeter = perimeter;
    this.weight = weight;
  }

  /**
   * @return
   */
  public String getHeight() {
    return this.height;
  }

  /**
   * @param height
   */
  public void setHeight(String height) {
    this.height = height;
  }

  /**
   * @return
   */
  public String getWidth() {
    return this.width;
  }

  /**
   * @param width
   */
  public void setWidth(String width) {
    this.width = width;
  }

  /**
   * @return
   */
  public String getThickness() {
    return this.thickness;
  }

  /**
   * @param thickness
   */
  public void setThickness(String thickness) {
    this.thickness = thickness;
  }

  /**
   * @return
   */
  public String getUnit() {
    return this.unit;
  }

  /**
   * @param unit
   */
  public void setUnit(String unit) {
    this.unit = unit;
  }

  /**
   * @return
   */
  public String getHeightLowCarving() {
    return this.heightLowCarving;
  }

  /**
   * @param heightLowCarving
   */
  public void setHeightLowCarving(String heightLowCarving) {
    this.heightLowCarving = heightLowCarving;
  }

  /**
   * @return
   */
  public String getHeightSculptuedArea() {
    return this.heightSculptuedArea;
  }

  /**
   * @param heightSculptuedArea
   */
  public void setHeightSculptuedArea(String heightSculptuedArea) {
    this.heightSculptuedArea = heightSculptuedArea;
  }

  /**
   * @return
   */
  public String getHeightButt() {
    return this.heightButt;
  }

  /**
   * @param heightButt
   */
  public void setHeightButt(String heightButt) {
    this.heightButt = heightButt;
  }

  /**
   * @return
   */
  public String getExposureAboveFloor() {
    return this.exposureAboveFloor;
  }

  /**
   * @param exposureAboveFloor
   */
  public void setExposureAboveFloor(String exposureAboveFloor) {
    this.exposureAboveFloor = exposureAboveFloor;
  }

  /**
   * @return
   */
  public String getWidthBaseCarving() {
    return this.widthBaseCarving;
  }

  /**
   * @param widthBaseCarving
   */
  public void setWidthBaseCarving(String widthBaseCarving) {
    this.widthBaseCarving = widthBaseCarving;
  }

  /**
   * @return
   */
  public String getWidthSculpturedArea() {
    return this.widthSculpturedArea;
  }

  /**
   * @param widthSculpturedArea
   */
  public void setWidthSculpturedArea(String widthSculpturedArea) {
    this.widthSculpturedArea = widthSculpturedArea;
  }

  /**
   * @return
   */
  public String getDepthRelief() {
    return this.depthRelief;
  }

  /**
   * @param depthRelief
   */
  public void setDepthRelief(String depthRelief) {
    this.depthRelief = depthRelief;
  }

  /**
   * @return
   */
  public String getDiameter() {
    return this.diameter;
  }

  /**
   * @param diameter
   */
  public void setDiameter(String diameter) {
    this.diameter = diameter;
  }

  /**
   * @return
   */
  public String getPerimeter() {
    return this.perimeter;
  }

  /**
   * @param perimeter
   */
  public void setPerimeter(String perimeter) {
    this.perimeter = perimeter;
  }

  /**
   * @return
   */
  public String getWeight() {
    return this.weight;
  }

  /**
   * @param weight
   */
  public void setWeight(String weight) {
    this.weight = weight;
  }

}
