package org.classicmayan.tools;

public class Encoding {
    private String value;

    public Encoding(String value){
        setValue(value);
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

}
