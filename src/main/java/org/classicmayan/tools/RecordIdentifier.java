package org.classicmayan.tools;

/**
 * 
 * @author Maximilian Brodhun State- and University Library Goettingen
 * @version 1.0.0
 * @since 19.11.2018
 */

public class RecordIdentifier {	
	
	public String textgridURI;
	
	public RecordIdentifier(String textgridURI) {		
		this.textgridURI = textgridURI;		
	}
	
	public String getRecordIdentifierXML() {
		return "<recordIdentifier source=\"TWKM\">" + textgridURI + "</recordIdentifier>";
	}
	
	public String getXML() {
		return "<recordIdentifier source=\"TWKM\">" + textgridURI + "</recordIdentifier>";
	}
}
