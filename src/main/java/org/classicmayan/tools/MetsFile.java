package org.classicmayan.tools;

import java.util.ArrayList;
import java.util.List;

/**
 * 
 * @author Maximilian Brodhun State- and University Library Goettingen
 * @version 1.0.0
 * @since 23.11.2018
 */

public class MetsFile {

	private String id;
	private String mimeType;
	private String created;	
	
	private MetsFlocat metsFlocat;
	private List<MetsFile> files = new ArrayList<MetsFile>();
	
	
	public MetsFile(List<MetsFile> files){
		this.files = files;
	}
	
	public MetsFile(String id, String mimeType, String created) {
		this.id = id;
		this.mimeType = mimeType;
		this.created = created;
	}
	
	public MetsFlocat addMetsFlocat(MetsFlocat metsFlocat) {
		this.metsFlocat = metsFlocat;
		return metsFlocat;
	}
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getMimeType() {
		return mimeType;
	}
	public void setMimeType(String mimeType) {
		this.mimeType = mimeType;
	}
	public String getCreated() {
		return created;
	}
	public void setCreated(String created) {
		this.created = created;
	}
	
	public String getMetsFileXML() {
		return "<file "
					+ "ID=\"" + getId() 
					+ "\" MIMETYPE=\"" + getMimeType() 
					+ "\" CREATED=\"" + getCreated() + "\">" 
					+ metsFlocat.getMetsFlocatXML() 
				+ "</file>";
	}
	
	public String buildAllFiles() {

		String files="";
		
		for(MetsFile file : this.files){
			files = files.concat(file.getMetsFileXML());
		}
		return files;
	}

}
