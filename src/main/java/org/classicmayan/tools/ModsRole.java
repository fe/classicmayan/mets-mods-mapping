/*******************************************************************************
 * This software is copyright (c) 2021 by
 * 
 * State and University Library Goettingen
 *
 * This is free software. You can redistribute it and/or modify it under the terms described in the
 * GNU Lesser General Public License v3 of which you should have received a copy. Otherwise you can
 * download it from
 *
 * http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * @copyright State and University Library Goettingen
 * @license GNU Lesser General Public License v3 (http://www.gnu.org/licenses/lgpl-3.0.txt)
 * @author Maximilian Behnert-Brodhun (behnert-brodhun@sub.uni-goettingen.de)
 ******************************************************************************/



package org.classicmayan.tools;

public class ModsRole {
	public ModsRoleTerm modsRoleTerm;
	
	public ModsRole(){

	}

	public ModsRole(ModsRoleTerm roleTerm){
		this.modsRoleTerm = roleTerm;
	}

	public ModsRoleTerm addModsRoleTerm(ModsRoleTerm modsRoleTerm) {
		this.modsRoleTerm = modsRoleTerm;
		return modsRoleTerm;
	}
	
	public String getModsRoleXML() {		
		return "<role>" + this.modsRoleTerm.getModsRoleTermXML() + "</role>";
	}
}
