package org.classicmayan.tools;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.logging.Logger;
import org.apache.jena.query.QueryExecution;
import org.apache.jena.query.QueryExecutionFactory;
import org.apache.jena.query.QuerySolution;
import org.apache.jena.query.ResultSet;
import org.apache.jena.rdf.model.RDFNode;

/**
 * @author Maximilian Brodhun, SUB Göttingen
 * @author Stefan E. Funk, SUB Göttingen
 */
public class Queries {

  // **
  // FINALS
  // **

  // URLs for getting the metadata of the IDIOM object and the vocabulary.
  private static final String QUERY_URL = "https://www.classicmayan.org/trip/metadata/query";
  private static final String VOCABULARY_URL =
      "https://www.classicmayan.org/trip/vocabularies/query";
  private static final String ALL_SPARQL_PREFIXES =
      "prefix dct: <http://purl.org/dc/terms#>\n" +
          "prefix dc: <http://purl.org/dc/elements/1.1/>\n" +
          "prefix rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>\n" +
          "prefix rdfs: <http://www.w3.org/2000/01/rdf-schema#>\n" +
          "prefix owl: <http://www.w3.org/2002/07/owl#>\n" +
          "prefix xsd: <http://www.w3.org/2001/XMLSchema#>\n" +
          "prefix idi: <http://idiom.uni-bonn.de/terms#>\n" +
          "prefix idiom: <http://idiom-projekt.de/schema/>\n" +
          "prefix ecrm: <http://erlangen-crm.org/current#>\n" +
          "prefix edm: <http://www.europeana.eu/schemas/edm#>\n" +
          "prefix form: <http://rdd.sub.uni-goettingen.de/rdfform#>\n" +
          "prefix crm: <http://erlangen-crm.org/current/>\n" +
          "prefix tgforms: <http://www.tgforms.de/terms#>\n" +
          "prefix textgrid: <http://textgridrep.de/>\n" +
          "prefix schema: <http://schema.org/>\n" +
          "prefix skos: <http://www.w3.org/2004/02/skos/core#>\n" +
          "prefix wgs84pos: <http://www.w3.org/2003/01/geo/wgs84_pos#>\n" +
          "prefix rel: <http://purl.org/vocab/relationship/>\n" +
          "prefix edm: <http://www.europeana.eu/schemas/edm/>\n" +
          "prefix idiomcat: <http://idiom-projekt.de/catalogue/>\n";

  // **
  // STATICS
  // **

  private static Logger log = Logger.getLogger(Queries.class.getName());

  // **
  // CLASS
  // **

  ArrayList<String> artefactUriList = new ArrayList<String>();
  List<String> allObjectUriList = new ArrayList<String>();

  /**
   * @param offset
   * @return
   */
  public List<String> getArtefactList(int offset) {

    String query =
        ALL_SPARQL_PREFIXES +
            "SELECT * " +
            "WHERE {" +
            "GRAPH ?textgridUriForArtefact {" +
            "?tgObject rdf:type idiom:Artefact." +
            "}} LIMIT 100 OFFSET " + offset;

    QueryExecution q = QueryExecutionFactory.sparqlService(QUERY_URL, query);
    ResultSet results = q.execSelect();

    while (results.hasNext()) {
      QuerySolution soln = results.nextSolution();
      RDFNode artefactURI = soln.get("textgridUriForArtefact");
      this.artefactUriList.add(artefactURI.toString());
    }

    return this.artefactUriList;
  }

  /**
   * @return
   */
  public List<String> getArtefactList() {

    String query =
        ALL_SPARQL_PREFIXES +
            "SELECT * " +
            "WHERE {" +
            "GRAPH ?textgridUriForArtefact {" +
            "?tgObject rdf:type idiom:Artefact." +
            "}}";

    QueryExecution q = QueryExecutionFactory.sparqlService(QUERY_URL, query);
    ResultSet results = q.execSelect();

    while (results.hasNext()) {
      QuerySolution soln = results.nextSolution();
      RDFNode artefactURI = soln.get("textgridUriForArtefact");
      this.artefactUriList.add(artefactURI.toString());
    }

    return this.artefactUriList;
  }

  /**
   * @param tgArtefactURI
   * @return
   */
  public List<String> getImagesOfArtefact(String tgArtefactURI) {

    String query =
        ALL_SPARQL_PREFIXES +
            "SELECT distinct ?tgObject ?textgridUriForArtefact ?conedakorLink WHERE { " +
            "GRAPH <" + tgArtefactURI + "> {" +
            "?tgObject rdf:type idiom:Artefact." +
            "?tgObject idiom:shows ?conedakorLink." +
            "}" +
            "} ";

    QueryExecution q = QueryExecutionFactory.sparqlService(QUERY_URL, query);
    ResultSet results = q.execSelect();
    List<String> imageList = new ArrayList<String>();

    while (results.hasNext()) {
      QuerySolution soln = results.nextSolution();
      RDFNode conedaKorImageIdentifier = soln.get("conedakorLink");
      imageList.add(conedaKorImageIdentifier.toString());
    }

    return imageList;
  }

  /**
   * @return
   */
  public List<String> getArtefactListWithImages() {

    String query =
        ALL_SPARQL_PREFIXES +
            "SELECT distinct ?tgObject ?textgridUriForArtefact WHERE {\n"
            + "  GRAPH ?textgridUriForArtefact {\n"
            + "    ?tgObject rdf:type idiom:Artefact.\n"
            + "        ?tgObject idiom:shows ?conedakorLink.\n"
            + "  }\n"
            + "  }";

    QueryExecution q = QueryExecutionFactory.sparqlService(QUERY_URL, query);
    ResultSet results = q.execSelect();

    while (results.hasNext()) {
      QuerySolution soln = results.nextSolution();
      RDFNode artefactURI = soln.get("textgridUriForArtefact");
      this.artefactUriList.add(artefactURI.toString());
    }

    return this.artefactUriList;
  }

  /**
   * @return
   */
  public List<String> getAllObjectsWithGregorianDate() {

    String query = ALL_SPARQL_PREFIXES +
        "SELECT * WHERE { GRAPH ?tgURI { \n" +
        "    ?tgObject dc:date ?dateRef.\n" +
        "    ?dateRef idiom:gregorianDate ?gregDate.\n" +
        "  }\n" +
        "}";

    QueryExecution q = QueryExecutionFactory.sparqlService(QUERY_URL, query);
    ResultSet results = q.execSelect();

    while (results.hasNext()) {
      QuerySolution soln = results.nextSolution();
      RDFNode artefactURI = soln.get("tgObject");
      this.allObjectUriList.add(artefactURI.toString());
    }

    return this.allObjectUriList;
  }

  /**
   * @param URI
   * @return
   * @throws IOException
   */
  public static String getArtefactTitle(String uri) {

    // Default error title, title should always be existing!
    String result = "[[No title for " + uri + "]]";

    String query = ALL_SPARQL_PREFIXES +
        "\n SELECT * " +
        "WHERE {" +
        "GRAPH <" + uri + "> {" +
        "?tgObject idiom:preferredTitle ?objectTitle.}}";

    QueryExecution q = QueryExecutionFactory.sparqlService(QUERY_URL, query);
    ResultSet results = q.execSelect();

    try {
      QuerySolution soln = results.next();
      result = soln.get("objectTitle").toString();
    } catch (NoSuchElementException e) {
      log.fine("query (" + query + ") is empty for ID " + uri + "!");
    }

    return result;
  }

  /**
   * @param URI
   * @return
   */
  public static String getIdiomNumber(String uri) {

    // Default error number, IDIOM number should always be existing!
    String result = "[[No IDIOM number for " + uri + "]]";

    String query = ALL_SPARQL_PREFIXES +
        "SELECT *" +
        "WHERE {" +
        "GRAPH <" + uri + "> {" +
        "?tgObject idiom:identifier ?idiomNumber.}}";

    QueryExecution q = QueryExecutionFactory.sparqlService(QUERY_URL, query);
    ResultSet results = q.execSelect();
    try {
      QuerySolution soln = results.next();
      result = soln.get("idiomNumber").toString();
    } catch (NoSuchElementException e) {
      log.warning("query (" + query + ") is empty for ID " + uri + "!");
    }

    return result;
  }

  /**
   * @param uri
   * @return
   */
  public String getAdditionalIdentifier(String uri) {

    String result = "";

    String query = ALL_SPARQL_PREFIXES +
        "SELECT *" +
        "WHERE {" +
        "GRAPH <" + uri + "> {" +
        "?tgObject idiom:additionalIdentifier ?addIdentifierRef." +
        "?addIdentifierRef idiom:identifierType ?addIdentifierType." +
        "?addIdentifierRef idiom:noteId ?addIdentifier." +
        "}}";

    QueryExecution q = QueryExecutionFactory.sparqlService(QUERY_URL, query);
    ResultSet results = q.execSelect();
    String idiomAdditionalIdentifier;
    String idiomAdditionalIdentifierType;
    try {
      QuerySolution soln = results.next();
      idiomAdditionalIdentifier = soln.get("addIdentifier").toString();
      idiomAdditionalIdentifierType = soln.get("addIdentifierType").toString();
    } catch (NoSuchElementException e) {
      idiomAdditionalIdentifier = "[[?]]";
      idiomAdditionalIdentifierType = "[[?]]";
      log.warning("query (" + query + ") is empty for ID " + uri + "!");
    }
    result = idiomAdditionalIdentifier.concat(" " + "Type: " + idiomAdditionalIdentifierType);

    return result;
  }

  /**
   * @param textGridURI
   * @return
   */
  public static LocationDataObject getLocationData(String textGridURI) {

    LocationDataObject locationDataObject = new LocationDataObject();

    String query = ALL_SPARQL_PREFIXES +
        "SELECT * WHERE { GRAPH <" + textGridURI + "> {\n" +
        "	?tgObject rdf:type idiom:Artefact.\n" +
        "	OPTIONAL{?tgObject idiom:inSitu ?inSitu.}\n" +
        "OPTIONAL{?tgObject idiom:currentCustody ?custodyRef." +
        " BIND(IRI(CONCAT(' + \"<\" + ',?custodyRef,' + \">\" + ')) AS ?custodyRefUri)" +
        " GRAPH ?custodyRefUri {" +
        "?custodyRef idiom:custodian ?custodianRef. " +
        "BIND(IRI(CONCAT(' + \"< \" + ',?custodianRef,' + \">\" + ')) AS ?custodianRefUri) " +
        "GRAPH ?custodianRefUri{ " +
        "?custodianRef schema:name ?custodianName." +
        "}" +
        " OPTIONAL{" +
        "?custodyRef	crm:P7_took_place_at ?custodyPlaceRef." +
        "BIND(IRI(CONCAT(' + \"<\" + ',?custodyPlaceRef,' + \">\" + ')) AS ?custodyPlaceRefUri)" +
        "GRAPH ?custodyPlaceRefUri {" +
        "?custodyPlaceRef crm:P87_is_identified_by ?custodyPlaceNameRef. " +
        "BIND(IRI(CONCAT(' + \"<\" + ',?custodyPlaceNameRef,' + \">\" + ')) AS ?custodyPlaceNameRefUri) "
        +
        "GRAPH ?custodyPlaceNameRefUri{" +
        "      ?custodyPlaceNameRef idiom:placeNameType \"preferred\".\n" +
        "?custodyPlaceNameRef idiom:placeName ?custodyPlaceName." +
        "}" +
        "}" +
        "}" +
        "}}" +
        "	OPTIONAL{?tgObject idiom:wasFoundAt ?discoveryRef.\n" +
        "	BIND(IRI(CONCAT(' + \"<\" + ',?discoveryRef,' + \">\" + ')) AS ?discoveryRefUri)\n" +
        "    GRAPH ?discoveryRefUri{\n" +
        "	   ?discoveryRef crm:P7_took_place_at ?discoveryPlaceRef.\n" +
        "       BIND(IRI(CONCAT(' + \"<\" + ',?discoveryPlaceRef,' + \">\" + ')) AS ?discoveryPlaceRefUri)\n"
        +
        "       GRAPH ?discoveryPlaceRefUri{\n" +
        "		?discoveryPlaceRef crm:P87_is_identified_by ?placeNameRef.\n" +
        "        BIND(IRI(CONCAT(' + \"<\" + ',?placeNameRef,' + \">\" + ')) AS ?placeNameRefUri)\n"
        +
        "        GRAPH ?placeNameRefUri {\n" +
        "           ?placeNameRef idiom:placeNameType \"preferred\".\n" +
        "      		?placeNameRef idiom:placeName ?placeName.\n" +
        "            OPTIONAL {\n" +
        "      			?discoveryPlaceRef idiom:hasArchaeologicalCoordinates ?archaeologicalCoordinatesRef.  \n"
        +
        "      			BIND(IRI(CONCAT(' + \"<\" + ',?archaeologicalCoordinatesRef,' + \">\" + ')) AS ?archaeologicalCoordinatesRefURL)    \n"
        +
        "      			GRAPH ?archaeologicalCoordinatesRefURL {     \n" +
        "      				?archaeologicalCoordinatesRef idiom:archaeologicalCoordinates ?archaeologicalCoordinates.\n"
        +
        "      			} \n" +
        "    		}\n" +
        "    		OPTIONAL {\n" +
        "      			?placeNameRef crm:P89_falls_within ?nextPlaceRef. \n" +
        "    		}\n" +
        "        }\n" +
        "      }\n" +
        "    }\n" +
        "}}}\n" +
        "";

    QueryExecution q = QueryExecutionFactory.sparqlService(QUERY_URL, query);
    ResultSet results = q.execSelect();

    try {
      QuerySolution soln = results.next();

      if (soln.get("inSitu") != null && soln.get("inSitu").toString()
          .equals("true^^http://www.w3.org/2001/XMLSchema#boolean")) {
        locationDataObject.setInSitu(true);
      } else {
        locationDataObject.setInSitu(false);
      }

      if (soln.get("inSitu") == null && soln.get("custodianName") != null) {
        String locationValue = soln.get("custodianName").toString();
        if (soln.get("custodyPlaceName") != null) {
          locationValue = locationValue.concat(", " + soln.get("custodyPlaceName").toString());
        }
        locationDataObject.setCurrentCustody(locationValue);
      }

      if (soln.get("discoveryPlaceRef") != null) {
        locationDataObject
            .setDiscoveryPlace(hierarichalPlaceQuery(soln.get("discoveryPlaceRef").toString()));
      } else {
        locationDataObject.setDiscoveryPlace("unknown");
      }

      if (soln.get("archaeologicalCoordinates") != null) {
        locationDataObject.setFindingSpot(soln.get("archaeologicalCoordinates").toString());
      }

      return locationDataObject;

    } catch (NoSuchElementException e) {
      return null;
    }
  }

  /**
   * @param uri
   * @return
   */
  public static String hierarichalPlaceQuery(String uri) {

    String result = "";

    String query = ALL_SPARQL_PREFIXES
        + " SELECT  DISTINCT ?placeType ?placeName ?nextPlace ?placeTypeToPrint WHERE { GRAPH <"
        + uri.replace("http://textgridrep.de/", "textgrid:") + "> {        \n" +
        "    	?discoveryPlaceRef rdf:type crm:E53_Place.\n" +
        "        OPTIONAL {\n" +
        "      		?discoveryPlaceRef idiom:placeType ?placeType.\n" +
        "    	}\n" +
        "    	?discoveryPlaceRef crm:P89_falls_within ?nextPlace.\n" +
        "		 ?discoveryPlaceRef crm:P87_is_identified_by ?placeNameRef.  \n" +
        "         BIND(IRI(CONCAT(' + \"<\" + ',?placeNameRef,' + \">\" + ')) AS ?placeNameUrl)\n" +
        "         GRAPH ?placeNameUrl {\n" +
        "                  ?placeNameRef idiom:placeName ?placeName.\n" +
        "                  ?placeNameRef idiom:placeNameType \"preferred\".\n" +
        "         }\n" +
        "  }\n" +
        "}";

    QueryExecution q = QueryExecutionFactory.sparqlService(QUERY_URL, query);
    ResultSet results = q.execSelect();
    List<String> placeTypes = new ArrayList<String>();

    QuerySolution solnNext = null;
    while (results.hasNext()) {
      QuerySolution soln = results.nextSolution();
      if (soln.get("placeType") != null) {
        placeTypes.add(soln.get("placeType").toString());
      }
      solnNext = soln;
    }

    if (solnNext != null && solnNext.get("placeType") != null
        && (placeTypes.contains("http://idiom-projekt.de/voc/placetype/concept000010")
            || placeTypes.contains("http://idiom-projekt.de/voc/placetype/concept000011"))) {
      result = solnNext.get("placeName").toString();
    } else if (solnNext != null
        && !solnNext.get("nextPlace").toString().replace("http://textgridrep.de/", "textgrid:")
            .equals(uri.replace("http://textgridrep.de/", "textgrid:"))) {
      result = hierarichalPlaceQuery(solnNext.get("nextPlace").toString());
    } else {
      // Place name is empty, set "null" according to RDF2MODS mapping!
      // Fixes https://gitlab.gwdg.de/fe/classicmayan/mets-mods-mapping/-/issues/12.
      result = "null";
    }

    return result;
  }

  /**
   * @param textGridUri
   * @return
   */
  public static ArtefactCreationDateData getPhysicalCreationDateData(String textGridUri) {

    ArtefactCreationDateData artefactCreationDateData = new ArtefactCreationDateData();

    String query = ALL_SPARQL_PREFIXES + "SELECT * WHERE { GRAPH <" + textGridUri + "> {\n" +
        "	?tgObject idiom:wasDedicatedBy ?dedicationRef.\n" +
        "    BIND(IRI(CONCAT(' + \"<\" + ',?dedicationRef,' + \">\" + ')) AS ?dedicationRefUri)\n" +
        "    GRAPH ?dedicationRefUri{\n" +
        "      ?dedicationRef idiom:activityTitle ?dedicationTitle.\n" +
        "      ?dedicationRef dc:date ?dateRef.\n" +
        "      BIND(IRI(CONCAT(' + \"<\" + ',?dateRef,' + \">\" + ')) AS ?dateRefUri)\n" +
        "      GRAPH ?dateRefUri {\n" +
        "        ?dateRef idiom:isQualifiedBy ?qualifier.\n" +
        "        ?dateRef idiom:longcount ?longcount.\n" +
        "        ?dateRef idiom:tzolkinInput ?tzolkinInput.\n" +
        "        ?dateRef idiom:tzolkinUnit ?tzolkinUnit.\n" +
        "        ?dateRef idiom:haabInput ?haabInput.\n" +
        "        ?dateRef idiom:haabUnit ?haabUnit.\n" +
        "        ?dateRef idiom:gregorianDate ?gregDate.\n" +
        "		 ?dateRef idiom:calendarEra ?calendarEra." +
        "        BIND((CONCAT(STR(?tzolkinInput), \" \", STR(?tzolkinUnit), \" \", STR(?haabInput), \" \", STR(?haabUnit) )) AS ?calendarRound)\n"
        +
        "      }\n" +
        "    }\n" +
        "  }\n" +
        "}";

    log.fine("physical location query: " + query);

    QueryExecution q = QueryExecutionFactory.sparqlService(QUERY_URL, query);
    ResultSet results = q.execSelect();

    try {
      QuerySolution soln = results.next();

      artefactCreationDateData.setLongcount(soln.get("longcount").toString());
      artefactCreationDateData.setCalendarRound(soln.get("calendarRound").toString());
      artefactCreationDateData
          .setGregorianDate(soln.get("gregDate").toString() + " " + soln.get("calendarEra"));
      artefactCreationDateData.setQualifier(soln.get("qualifier").toString());
    } catch (NoSuchElementException e) {
      artefactCreationDateData.setLongcount("undated");
      artefactCreationDateData.setCalendarRound("undated");
      artefactCreationDateData.setGregorianDate("undated");
      artefactCreationDateData.setQualifier("undated");
    }

    return artefactCreationDateData;
  }

  /**
   * @param textGridUri
   * @return
   */
  public static GenreDataObject getGenreDataObject(String textGridUri) {

    GenreDataObject genreDataObject = new GenreDataObject();

    // Getting the necessary information from the SPARQL endpoint for the metadata objects.
    String query = ALL_SPARQL_PREFIXES + "SELECT * WHERE { GRAPH <" + textGridUri + "> {\n" +
        "	?tgObject crm:P41i_was_classified_by ?typeAssignmentRef.\n" +
        "    BIND(IRI(CONCAT(' + \"<\" + ',?typeAssignmentRef,' + \">\" + ')) AS ?typeAssignmentUri)\n"
        +
        "    GRAPH ?typeAssignmentUri{\n" +
        "    	?typeAssignmentRef crm:P42_assigned ?typeAssignment. \n" +
        "    }   \n" +
        "  }\n" +
        "}";
    QueryExecution q = QueryExecutionFactory.sparqlService(QUERY_URL, query);
    ResultSet results = q.execSelect();

    // TODO Remove try catch
    try {
      QuerySolution soln = results.next();
      String artefactTypeUriInVocabulary = soln.get("typeAssignment").toString();
      // The query for the genre information in the metadata SPARQL endpoint just serves an URI from
      // the vocabulary SPARQL endpoint. So an additional query inside the vocabulary endpoint is
      // necessary to get the specific value to give the information for the mods genre.

      // Putting the information of the genre in the data object.
      genreDataObject.setGenreValue((getVocabulary(artefactTypeUriInVocabulary)));
      genreDataObject.setLanguage("en");
      genreDataObject.setValueURI(soln.get("typeAssignment").toString());

      return genreDataObject;
    } catch (NoSuchElementException e) {
      return null;
    }
  }

  /**
   * @param textGridUri
   * @return
   */
  // TODO One Query!
  public static PhysicalDescriptionDataObject getPhysicalDescriptionDataObject(String textGridUri) {

    PhysicalDescriptionDataObject physicalDescriptionDataObject =
        new PhysicalDescriptionDataObject();

    String query = ALL_SPARQL_PREFIXES + "SELECT * WHERE { GRAPH <" + textGridUri + "> {\n" +
        "	?tgObject idiom:hasMaterialAssignment ?materialAssignmentRef.\n" +
        "    BIND(IRI(CONCAT(' + \"<\" + ',?materialAssignmentRef,' + \">\" + ')) AS ?materialAssignmentUri)\n"
        +
        "    GRAPH ?materialAssignmentUri{\n" +
        "    	?materialAssignmentRef idiom:assignedMaterial ?materialAssignment. \n" +
        "    }\n" +
        "  }\n" +
        "}";

    QueryExecution q = QueryExecutionFactory.sparqlService(QUERY_URL, query);
    ResultSet results = q.execSelect();

    while (results.hasNext()) {
      QuerySolution soln = results.nextSolution();
      RDFNode artefactURI = soln.get("materialAssignment");
      String materialAssignment = getVocabulary(soln.get("materialAssignment").toString());

      physicalDescriptionDataObject.formListMaterialAssignments
          .add(new FormDataObject("en", "material", artefactURI.toString(), materialAssignment));
    }

    String queryForTechnique = ALL_SPARQL_PREFIXES + "SELECT * WHERE { GRAPH <" + textGridUri
        + "> {\n" +
        "	?tgObject idiom:hasTechniqueAssignment ?techniqueAssignmentRef.\n" +
        "    BIND(IRI(CONCAT(' + \"<\" + ',?techniqueAssignmentRef,' + \">\" + ')) AS ?techniqueAssignmentUri)\n"
        +
        "    GRAPH ?techniqueAssignmentUri{\n" +
        "    	?techniqueAssignmentRef idiom:assignedTechnique ?techniqueAssignment. \n" +
        "    }\n" +
        "  }\n" +
        "}";

    QueryExecution qTechniqueAssignment =
        QueryExecutionFactory.sparqlService(QUERY_URL, queryForTechnique);
    ResultSet resultsTechniqueAssignment = qTechniqueAssignment.execSelect();

    while (resultsTechniqueAssignment.hasNext()) {
      QuerySolution solnTechniqueAssignment = resultsTechniqueAssignment.nextSolution();
      RDFNode artefactURI = solnTechniqueAssignment.get("techniqueAssignment");
      String techniqueAssignment =
          getVocabulary(solnTechniqueAssignment.get("techniqueAssignment").toString());

      physicalDescriptionDataObject.formListTechniqueAssignments
          .add(new FormDataObject("en", "technique", artefactURI.toString(), techniqueAssignment));
    }

    String queryForDimensions =
        ALL_SPARQL_PREFIXES + "SELECT * WHERE { GRAPH <" + textGridUri + "> {\n" +
            "	?tgObject crm:P43_has_dimension ?dimensionRef.\n" +
            "    BIND(IRI(CONCAT(' + \"<\" + ',?dimensionRef,' + \">\" + ')) AS ?dimensionUri)\n" +
            "    GRAPH ?dimensionUri{\n" +
            "      ?dimensionRef idiom:status ?status.\n" +
            "      OPTIONAL {?dimensionRef idiom:height ?height.}\n" +
            "      OPTIONAL {?dimensionRef idiom:width ?width.}\n" +
            "      OPTIONAL {?dimensionRef idiom:thickness ?thickness.}\n" +
            "      OPTIONAL {?dimensionRef idiom:hasUnit ?unit.}\n" +
            "      OPTIONAL {?dimensionRef idiom:heightLowCarving ?heightLowCarving.}\n" +
            "      OPTIONAL {?dimensionRef idiom:heightSculptuedArea ?heightSculptuedArea.}\n" +
            "      OPTIONAL {?dimensionRef idiom:heightButt ?heightButt.}\n" +
            "      OPTIONAL {?dimensionRef idiom:exposureAboveFloor ?exposureAboveFloor.}\n" +
            "      OPTIONAL {?dimensionRef idiom:widthBaseCarving ?widthBaseCarving.}\n" +
            "      OPTIONAL {?dimensionRef idiom:widthSculpturedArea ?widthSculpturedArea.}\n" +
            "      OPTIONAL {?dimensionRef idiom:depthRelief ?depthRelief.}\n" +
            "      OPTIONAL {?dimensionRef idiom:diameter ?diameter.}\n" +
            "      OPTIONAL {?dimensionRef idiom:perimeter ?perimeter.}\n" +
            "      OPTIONAL {?dimensionRef idiom:weight ?weight.}\n" +
            "    }\n" +
            "  }\n" +
            "}";

    log.fine("dimensions query: " + queryForDimensions);

    QueryExecution qDimensions = QueryExecutionFactory.sparqlService(QUERY_URL, queryForDimensions);
    ResultSet resultsDimensions = qDimensions.execSelect();

    while (resultsDimensions.hasNext()) {
      QuerySolution solnDimensions = resultsDimensions.nextSolution();

      String height = "";
      if (solnDimensions.get("height") != null) {
        height = solnDimensions.get("height").toString();
      }
      String width = "";
      if (solnDimensions.get("width") != null) {
        width = solnDimensions.get("width").toString();
      }
      String thickness = "";
      if (solnDimensions.get("thickness") != null) {
        thickness = solnDimensions.get("thickness").toString();
      }
      String unit = "";
      if (solnDimensions.get("unit") != null) {
        unit = solnDimensions.get("unit").toString();
      }
      String heightLowCarving = "";
      if (solnDimensions.get("heightLowCarving") != null) {
        heightLowCarving = solnDimensions.get("heightLowCarving").toString();
      }
      String heightSculptuedArea = "";
      if (solnDimensions.get("heightSculptuedArea") != null) {
        heightSculptuedArea = solnDimensions.get("heightSculptuedArea").toString();
      }
      String heightButt = "";
      if (solnDimensions.get("heightButt") != null) {
        heightButt = solnDimensions.get("heightButt").toString();
      }
      String exposureAboveFloor = "";
      if (solnDimensions.get("exposureAboveFloor") != null) {
        exposureAboveFloor = solnDimensions.get("exposureAboveFloor").toString();
      }
      String widthBaseCarving = "";
      if (solnDimensions.get("widthBaseCarving") != null) {
        widthBaseCarving = solnDimensions.get("widthBaseCarving").toString();
      }
      String widthSculpturedArea = "";
      if (solnDimensions.get("widthSculpturedArea") != null) {
        widthSculpturedArea = solnDimensions.get("widthSculpturedArea").toString();
      }
      String depthRelief = "";
      if (solnDimensions.get("depthRelief") != null) {
        depthRelief = solnDimensions.get("depthRelief").toString();
      }
      String diameter = "";
      if (solnDimensions.get("diameter") != null) {
        diameter = solnDimensions.get("diameter").toString();
      }
      String perimeter = "";
      if (solnDimensions.get("perimeter") != null) {
        perimeter = solnDimensions.get("perimeter").toString();
      }
      String weight = "";
      if (solnDimensions.get("weight") != null) {
        weight = solnDimensions.get("weight").toString();
      }

      physicalDescriptionDataObject.extentListDimensions.add(
          new ExtentDataObject(
              height,
              width,
              thickness,
              unit,
              heightLowCarving,
              heightSculptuedArea,
              heightButt,
              exposureAboveFloor,
              widthBaseCarving,
              widthSculpturedArea,
              depthRelief,
              diameter,
              perimeter,
              weight));
    }

    return physicalDescriptionDataObject;
  }

  /**
   * @param textGridUri
   * @return
   */
  public static List<String> getRelatedItems(String textGridUri) {

    List<String> relatedItems = new ArrayList<String>();
    String query = ALL_SPARQL_PREFIXES + "SELECT * WHERE { GRAPH <" + textGridUri + "> {\n" +
        "	?tgObject crm:P46_is_composed_of ?artefactPart.\n" +
        "  }\n" +
        "}";

    QueryExecution q = QueryExecutionFactory.sparqlService(QUERY_URL, query);
    ResultSet results = q.execSelect();

    while (results.hasNext()) {
      QuerySolution soln = results.nextSolution();
      RDFNode artefactPartURI = soln.get("artefactPart");
      relatedItems.add(artefactPartURI.toString());
    }

    return relatedItems;
  }

  /**
   * @param conceptURI
   * @return
   */
  public static String getVocabulary(String uri) {

    String result = "";

    String query = "PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>\n" +
        "PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>\n" +
        "PREFIX skos: <http://www.w3.org/2004/02/skos/core#>\n" +
        "PREFIX dct: <http://purl.org/dc/terms/>\n" +
        "\n" +
        "SELECT DISTINCT ?before ?conceptName ?conceptUri\n" +
        "  WHERE {    \n" +
        "   	<" + uri + "> skos:prefLabel ?conceptName.\n" +
        "  FILTER(LANG(?conceptName) = \"\" || LANGMATCHES(LANG(?conceptName), \"en\"))  \n" +
        "}";

    QueryExecution qVoc = QueryExecutionFactory.sparqlService(VOCABULARY_URL, query);
    ResultSet resultsVoc = qVoc.execSelect();

    try {
      QuerySolution solnVoc = resultsVoc.next();
      result = solnVoc.get("conceptName").toString().replace("@en", "");
    } catch (NoSuchElementException e) {
      log.warning("query (" + query + ") is empty for ID " + uri + "!");
    }

    return result;
  }

}
