package org.classicmayan.tools;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Maximilian Brodhun, SUB Göttingen
 * @author Stefan E. Funk, SUB Göttingen
 */
public class ModsPhysicalLocation {

  // **
  // CLASS
  // **

  private String physicalLocationType;
  private String physicalLocationDispayType;
  private String physicalLocationValue;
  private List<ModsPhysicalLocation> locations = new ArrayList<ModsPhysicalLocation>();

  /**
   * @param locations
   */
  public ModsPhysicalLocation(List<ModsPhysicalLocation> locations) {
    this.locations = locations;
  }

  /**
   * @param physicalLocationType
   * @param physicalLocationDisplayType
   * @param physicalLocationValue
   */
  public ModsPhysicalLocation(String physicalLocationType, String physicalLocationDisplayType,
      String physicalLocationValue) {
    this.physicalLocationType = physicalLocationType;
    this.physicalLocationDispayType = physicalLocationDisplayType;
    this.physicalLocationValue = physicalLocationValue;
  }

  /**
   * @param physicalLocationType
   * @param physicalLocationValue
   */
  public ModsPhysicalLocation(String physicalLocationType, String physicalLocationValue) {
    this.physicalLocationType = physicalLocationType;
    this.physicalLocationValue = physicalLocationValue;
  }

  /**
   * 
   */
  public ModsPhysicalLocation() {
    //
  }

  /**
   * @return
   */
  public String getPhysicalLocationType() {
    return this.physicalLocationType;
  }

  /**
   * @param physicalLocationType
   */
  public void setPhysicalLocationType(String physicalLocationType) {
    this.physicalLocationType = physicalLocationType;
  }

  /**
   * @return
   */
  /**
   * @return
   */
  public String getPhysicalLocationDisplayType() {
    return this.physicalLocationDispayType;
  }

  /**
   * @param physicalLocationDispayType
   */
  public void setPhysicalLocationDispayType(String physicalLocationDispayType) {
    this.physicalLocationDispayType = physicalLocationDispayType;
  }

  /**
   * @return
   */
  public String getPhysicalLocationValue() {
    return this.physicalLocationValue;
  }

  /**
   * @param physicalLocationValue
   */
  public void setPhysicalLocationValue(String physicalLocationValue) {
    this.physicalLocationValue = physicalLocationValue;
  }

  /**
   * @return
   */
  public String getPhysicalLocationXML() {

    try {
      if (getPhysicalLocationDisplayType() == null) {
        return "<physicalLocation "
            + "type=\"" + getPhysicalLocationType() + "\">"
            + getPhysicalLocationValue() + "</physicalLocation>";
      } else {
        return "<physicalLocation "
            + "type=\"" + getPhysicalLocationType() + "\" "
            + "displayLabel=\"" + getPhysicalLocationDisplayType() + "\">"
            + getPhysicalLocationValue() + "</physicalLocation>";
      }
    } catch (NullPointerException e) {
      e.printStackTrace();
    }

    return null;
  }

  /**
   * @return
   */
  public String buildAllLocations() {

    String physicalLocations = "";

    for (ModsPhysicalLocation location : this.locations) {
      physicalLocations = physicalLocations.concat(location.getPhysicalLocationXML());
    }

    return physicalLocations;
  }

}
