package org.classicmayan.tools;

import java.util.ArrayList;
import java.util.List;

/**
 * 
 * @author Maximilian Brodhun State- and University Library Goettingen
 * @version 1.0.0
 * @since 20.11.2018
 */

public class Extent {

	private String extent;
	private List<Extent> extents = new ArrayList<Extent>();
	
	public Extent(List<Extent> extents) {
		this.extents = extents;
	}
	
	public Extent(String extent) {
		this.extent = extent;
	}
	
	public Extent() {
		
	}

	public String getExtent() {
		return extent;
	}

	public void setExtent(String extent) {
		this.extent = extent;
	}
	
	public String getExtentXML() {
		try {
			return "<extent>" + getExtent() + "</extent>";
		}catch(NullPointerException n){
			return "";
		}
	}
	
	public String buildAllExtents() {
		String extents="";
		for(Extent extent : this.extents){
			extents = extents.concat(extent.getExtentXML());
		}
		return extents;
	}
}