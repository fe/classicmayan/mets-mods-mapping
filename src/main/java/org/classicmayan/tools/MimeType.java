package org.classicmayan.tools;

public class MimeType {

    private String mimeType;

    public MimeType(String mimeType){
        this.setMimeType(mimeType);
    }

    public String getMimeType() {
        return mimeType;
    }

    public void setMimeType(String mimeType) {
        this.mimeType = mimeType;
    }

}
