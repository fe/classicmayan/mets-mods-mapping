package org.classicmayan.tools;

/**
 * 
 * @author Maximilian Brodhun State- and University Library Goettingen
 * @version 1.0.0
 * @since 19.11.2018
 */

public class ModsTypeOfResource {
	
	private String typeOfResourceValue;
	
	
	public String getTypeOfReosourceXML() {
		try {
			return "<typeOfResource>" + getTypeOfResourceValue() + "</typeOfResource>";
		}catch(NullPointerException n) {
			return "";
		}
	}

	public String getTypeOfResourceValue() {
		return typeOfResourceValue;
	}

	public void setTypeOfResourceValue(String typeOfResourceValue) {
		this.typeOfResourceValue = typeOfResourceValue;
	}
	
}
