package org.classicmayan.tools;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Maximilian Brodhun, SUB Göttingen
 * @version 2022-08-18
 * @since 2018-11-20
 */

public class ModsNote {

  private String type;
  private String value;

  private List<ModsNote> notes = new ArrayList<ModsNote>();

  /**
   * @param notes
   */
  public ModsNote(List<ModsNote> notes) {
    this.notes = notes;
  }

  /**
   * @param type
   * @param value
   */
  public ModsNote(String type, String value) {
    this.type = type;
    this.value = value;
  }

  /**
   * 
   */
  public ModsNote() {
    //
  }

  /**
   * @return
   */
  public String getType() {
    return this.type;
  }

  /**
   * @param type
   */
  public void setType(String type) {
    this.type = type;
  }

  /**
   * @return
   */
  public String getValue() {
    return this.value;
  }

  /**
   * @param value
   */
  public void setValue(String value) {
    this.value = value;
  }

  /**
   * @return
   */
  public String getModsNoteXML() {
    return "<note type=\"" + getType() + "\">" + getValue() + "</note>";
  }

  /**
   * @return
   */
  public String buildAllModsNotes() {

    String notes = "";
    // System.out.println(this.notes.size());
    if (this.notes.size() > 0) {
      for (ModsNote note : this.notes) {
        notes = notes.concat(note.getModsNoteXML());
      }
    }

    return notes;
  }

}
