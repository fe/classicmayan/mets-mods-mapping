package org.classicmayan.tools;

import java.util.ArrayList;
import java.util.List;

/**
 * 
 * @author Maximilian Brodhun State- and University Library Goettingen
 * @version 1.0.0
 * @since 19.11.2018
 */

public class ModsTitleInfo {

	private String modsTitleInfoValue;
	private String modsTitleLanguageValue;

	List<Title> titles = new ArrayList<Title>();
	
	public void addTitle(Title title) {
		titles.add(title);
	}
	
	public String getModsTitleInfoXML() {
		return "<titleInfo><title xml:lang=\"" + this.getModsTitleInfoLanguageValue() + "\">"  + this.getModsTitleInfoValue() + "</title></titleInfo>";
	}
	
	public String getModsTitleInfoValue() {
		return modsTitleInfoValue;
	}
	public void setModsTitleInfoValue(String modsTitleInfoValue) {
		this.modsTitleInfoValue = modsTitleInfoValue;
	}

	public String getModsTitleInfoLanguageValue() {
		return modsTitleLanguageValue;
	}

	public void setModsTitleInfoLanguageValue(String modsTitleInfoLanguage) {
		this.modsTitleLanguageValue = modsTitleInfoLanguage;
	}
	
	public String getXML() {
		String xml="";
		
		for(Title title : titles) {
			xml += title.getXML();
		}
		
		return "<titleInfo>" + xml + "</titleInfo>";
	}
	
}
