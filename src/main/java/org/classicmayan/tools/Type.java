package org.classicmayan.tools;

public class Type {

    private String type;

    public Type(String type){
        this.setType(type);
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

}
