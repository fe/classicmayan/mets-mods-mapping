package org.classicmayan.tools;

import java.beans.XMLDecoder;

/**
 * 
 * @author Maximilian Brodhun State- and University Library Goettingen
 * @version 1.0.0
 * @since 20.11.2018
 */

public class ModsGenre {

	private String genreLanguage;
	private String genreValueURI;
	private String genreValue;
	private String xmlCode;
	
	private Language language;
	private ValueURI valueURI;
	private Authority authority;
	private String value;

	public ModsGenre(){

	}

	public ModsGenre(Language language, ValueURI valueURI, String value){
		setLanguage(language);
		setValueURI(valueURI);

		xmlCode = "<genre "  
			+ language.getAttributeName() + "\"" + language.getValue() + "\"" 
			+ valueURI.getAttributeName() + "\"" + valueURI.getValue() + "\">" + value + "</genre>"; 
	}

	public ModsGenre(Authority authority, String value){
		setAuthority(authority);
		setValue(value);
		xmlCode = "<genre " + authority.getAuthority() + ">" +  getValue() + "</genre>";
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}
	public Authority getAuthority() {
		return authority;
	}

	public void setAuthority(Authority authority) {
		this.authority = authority;
	}

	public String getXmlCode() {
		return xmlCode;
	}

	public void setXmlCode(String xmlCode) {
		this.xmlCode = xmlCode;
	}

	public ValueURI getValueURI() {
		return valueURI;
	}

	public void setValueURI(ValueURI valueURI) {
		this.valueURI = valueURI;
	}

	public Language getLanguage() {
		return language;
	}

	public void setLanguage(Language language) {
		this.language = language;
	}


	public String getGenreLanguage() {
		return genreLanguage;
	}
	public void setGenreLanguage(String genreLanguage) {
		this.genreLanguage = genreLanguage;
	}
	public String getGenreValueURI() {
		return genreValueURI;
	}
	public void setGenreValueURI(String genreValueURI) {
		this.genreValueURI = genreValueURI;
	}
	public String getGenreValue() {
		return genreValue;
	}
	public void setGenreValue(String genreValue) {
		this.genreValue = genreValue;
	}
	
	public String getModsGenreXML() {
		return "<genre xml:lang=\"" + getGenreLanguage() +  "\" valueURI=\"" + getGenreValueURI() + "\">" + getGenreValue() + "</genre>";
	}

	public String getXML(){
		return "<genre xml:lang=\"" + this.getGenreLanguage() +  "\" valueURI=\"" + this.getGenreValueURI() + "\">" + this.getGenreValue() + "</genre>";
	}

	public String getXMLFine(){
		return xmlCode;
	}
}
