package org.classicmayan.tools;


/**
 * 
 * @author Maximilian Brodhun State- and University Library Goettingen
 * @version 1.0.0
 * @since 23.11.2018
 */

public class MetsFptr {

	private String fileID;
		
	public MetsFptr(String id) {
		this.fileID = id;
	}
	
	public MetsFptr() {
		
	}
	
	public String getFileID() {
		return fileID;
	}

	public void setFileID(String fileID) {
		this.fileID = fileID;
	}
	
	public String getMetsFptrXML() {
		return "<fptr FILEID=\"" + getFileID() + "\"/>"; 
	}
	
	public String getXML() {
		String fptrs="";
		fptrs = fptrs.concat("<fptr FILEID=\"" + this.getFileID() + "\"/>");
		return fptrs;
	}
}
