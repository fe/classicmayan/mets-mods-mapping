package org.classicmayan.tools;

/**
 * 
 * @author Maximilian Brodhun State- and University Library Goettingen
 * @version 1.0.0
 * @since 20.11.2018
 */

public class ModsPhysicalDescription {

	Form form;
	Extent extent;
	
	public Form addForm(Form form) {
		this.form = form;
		return form;
	}
	
	public Extent addExtent(Extent extent) {
		this.extent = extent;
		return extent;
	}
	
	public String getModsPhysicalDescriptionXML() {
		
		return "<physicalDescription>" + 
					form.buildAllForms() + 
					extent.buildAllExtents() + 
				"</physicalDescription>";
	}
}
