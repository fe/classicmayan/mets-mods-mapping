package org.classicmayan.tools;

public class MdType {

    private String mdType;

    public MdType(String mdType){
        this.setMdType(mdType);
    }

    public String getMdType() {
        return mdType;
    }

    public void setMdType(String mdType) {
        this.mdType = mdType;
    }
}
