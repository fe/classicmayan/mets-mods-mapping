package org.classicmayan.tools;

/**
 * 
 * @author Maximilian Brodhun State- and University Library Goettingen
 * @version 1.0.0
 * @since 22.11.2018
 */

public class MetsOwnerSiteURL {
	private String metsOwnerSiteURLValue;

	public MetsOwnerSiteURL(){

	}

	public MetsOwnerSiteURL(String value){
		this.setMetsOwnerSiteURLValue(value);
	}

	public String getMetsOwnerSiteURLValue() {
		return metsOwnerSiteURLValue;
	}

	public void setMetsOwnerSiteURLValue(String metsOwnerSiteURLValue) {
		this.metsOwnerSiteURLValue = metsOwnerSiteURLValue;
	}
	
	public String getMetsOwnerSiteURLXML() {
		return "<ownerSiteURL>" + getMetsOwnerSiteURLValue() + "</ownerSiteURL>";
	}
}
