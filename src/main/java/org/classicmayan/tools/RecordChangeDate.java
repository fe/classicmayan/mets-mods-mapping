package org.classicmayan.tools;

/**
 * @author Maximilian Brodhun, SUB Göttingen
 * @author Stefan E. Funk, SUB Göttingen
 */

public class RecordChangeDate {

  // **
  // CLASS
  // **

  private String recordChangeDateValue;
  private String encoding;

  /**
   * @param encoding
   * @param recordChangeDate
   */
  public RecordChangeDate(String encoding, String recordChangeDate) {
    this.encoding = encoding;
    this.recordChangeDateValue = recordChangeDate;
  }

  /**
   * @return
   */
  public String getRecordChangeDateValue() {
    return this.recordChangeDateValue;
  }

  /**
   * @param changeDate
   */
  public void setRecordChangeDateValue(String changeDate) {
    this.recordChangeDateValue = changeDate;
  }

  /**
   * @return
   */
  public String getRecordChangeDateXML() {
    return "<recordChangeDate encoding=\"" + this.getEncoding() + "\">" + getRecordChangeDateValue()
        + "</recordChangeDate>";
  }

  /**
   * @return
   */
  public String getXML() {
    return "<recordChangeDate encoding=\"" + this.getEncoding() + "\">" + getRecordChangeDateValue()
        + "</recordChangeDate>";
  }

  /**
   * @return
   */
  public String getEncoding() {
    return this.encoding;
  }

  /**
   * @param encoding
   */
  public void setEncoding(String encoding) {
    this.encoding = encoding;
  }

}
