package org.classicmayan.tools;

import java.util.ArrayList;

public class PhysicalDescriptionDataObject {
	
	//private String formLanguage;
	private String formType;
	private String formValueURI;
	
	private String artefactHeight;
	private String artefactWidth;
	private String artefactthickness;
	private String dimensionsUnit;
	
	ArrayList<FormDataObject> formListMaterialAssignments = new ArrayList<FormDataObject>();
	ArrayList<FormDataObject> formListTechniqueAssignments = new ArrayList<FormDataObject>();
	ArrayList<ExtentDataObject> extentListDimensions = new ArrayList<ExtentDataObject>();
	
	public String getFormType() {
		return formType;
	}
	public void setFormType(String formType) {
		this.formType = formType;
	}
	public String getFormValueURI() {
		return formValueURI;
	}
	public void setFormValueURI(String formValueURI) {
		this.formValueURI = formValueURI;
	}
	public String getArtefactHeight() {
		return artefactHeight;
	}
	public void setArtefactHeight(String artefactHeight) {
		this.artefactHeight = artefactHeight;
	}
	public String getArtefactWidth() {
		return artefactWidth;
	}
	public void setArtefactWidth(String artefactWidth) {
		this.artefactWidth = artefactWidth;
	}
	public String getArtefactthickness() {
		return artefactthickness;
	}
	public void setArtefactthickness(String artefactthickness) {
		this.artefactthickness = artefactthickness;
	}
	public String getDimensionsUnit() {
		return dimensionsUnit;
	}
	public void setDimensionsUnit(String dimensionsUnit) {
		this.dimensionsUnit = dimensionsUnit;
	}
}
