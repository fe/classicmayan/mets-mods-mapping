package org.classicmayan.tools;

/**
 * 
 * @author Maximilian Brodhun State- and University Library Goettingen
 * @version 1.0.0
 * @since 21.11.2018
 */

public class MetsHdr {

	private String metsHdrCreationDate;

	public MetsHdr(){
		
	}

	public MetsHdr(String creationDate){
		this.metsHdrCreationDate = creationDate;
	}

	public String getMetsHdrCreationDate() {
		return metsHdrCreationDate;
	}

	public void setMetsHdrCreationDate(String metsHdrCreationDate) {
		this.metsHdrCreationDate = metsHdrCreationDate;
	}
	
	public String getMetsHdrXML() {
		return "<metsHdr CREATEDATE=\"" + getMetsHdrCreationDate() + "\"/>";
	}

	public String getXML() {
		return "<metsHdr CREATEDATE=\"" + getMetsHdrCreationDate() + "\"/>";
	}
}
