package org.classicmayan.tools;

import java.util.ArrayList;
import java.util.List;

/**
 * 
 * @author Maximilian Brodhun State- and University Library Goettingen
 * @version 1.0.0
 * @since 23.11.2018
 */

public class MetsFileSec {

	MetsFileGrp metsFileGrp;
	List<MetsFileGrp> metsFileGrps = new ArrayList<MetsFileGrp>();
	
	MetsFileGrp addMetsFileGrp(MetsFileGrp metsFileGrp) {
		metsFileGrps.add(metsFileGrp);
		this.metsFileGrp = metsFileGrp;
		return metsFileGrp;
	}
	
	public String getMetsFileSecXML() {		
		return "<fileSec>" + metsFileGrp.getXML() + "</fileSec>";
	}
	
	public String getXML() {		
		String xml = "";
		for(MetsFileGrp metsFileGrp : metsFileGrps) {
			xml+=metsFileGrp.getXML();
		}
		return "<fileSec>" + xml + "</fileSec>";
	}
}
