package org.classicmayan.tools;

public class OtherMdType {
    private String otherMdType;

    public OtherMdType(String otherMdType){
        this.setOtherMdType(otherMdType);
    }

    public String getOtherMdType() {
        return otherMdType;
    }

    public void setOtherMdType(String otherMdType) {
        this.otherMdType = otherMdType;
    }


}
