package org.classicmayan.tools;

import java.util.ArrayList;
import java.util.List;

/**
 * 
 * @author Maximilian Brodhun State- and University Library Goettingen
 * @version 1.0.0
 * @since 19.11.2018
 */

public class ModsOriginInfo {
	
	ModsDateCreated dateCreated;

	List<ModsDateCreated> dateCreatedList = new ArrayList<ModsDateCreated>();
	
	public ModsOriginInfo(){

	}

	public ModsDateCreated addDateCreated(ModsDateCreated dateCreated) {
		dateCreatedList.add(dateCreated);
		//this.dateCreated = dateCreated;
		return dateCreated;
	}
	
	public String getModsOriginInfoXML() {				
		String xml="";
		for(ModsDateCreated creationDate : dateCreatedList){
			xml += creationDate.getDateCreatedXML();
		}
		return "<originInfo>" + xml + "</originInfo>";	
		
	}

	public String getXML(){
		String xml="";
		for(ModsDateCreated creationDate : dateCreatedList){
			xml += creationDate.getXML();
		}

		return "<originInfo>" + xml + "</originInfo>";
	}

}
