/*******************************************************************************
 * This software is copyright (c) 2021 by
 * 
 * State and University Library Goettingen
 *
 * This is free software. You can redistribute it and/or modify it under the terms described in the
 * GNU Lesser General Public License v3 of which you should have received a copy. Otherwise you can
 * download it from
 *
 * http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * @copyright State and University Library Goettingen
 * @license GNU Lesser General Public License v3 (http://www.gnu.org/licenses/lgpl-3.0.txt)
 * @author Maximilian Behnert-Brodhun (behnert-brodhun@sub.uni-goettingen.de)
 ******************************************************************************/



package org.classicmayan.tools;

import java.util.ArrayList;
import java.util.List;

public class ModsName {

	List<ModsName> modsNames = new ArrayList<ModsName>();
	
	public ModsNamePart modsNamePart;
	public ModsRole modsRole;

	public ModsName() {
		
	}
	
	public ModsName(ModsNamePart namePart, ModsRole role){
		this.modsNamePart = namePart;
		this.modsRole = role;
	}

	public ModsName(List<ModsName> modsNames) {
		this.modsNames = modsNames;
	}
	
	public String getModsNameXML() {
		
		return "<name>" 
				+ this.modsNamePart.getModsNamePartXML()
				+ this.modsRole.getModsRoleXML()
				+ "</name>";
	}
	
	public ModsNamePart addModsNamePart(ModsNamePart modsNamePart) {
		this.modsNamePart = modsNamePart;
		return modsNamePart;
	}
	public ModsRole addModsRole(ModsRole modsRole) {
		this.modsRole = modsRole;
		return modsRole;
	}
	
	public String getModsNameXMLList() {
		String modsNamesXML = "";
		for(ModsName modsName : modsNames) {
			modsNamesXML+=modsName.getModsNameXML();
		}
		return modsNamesXML;
	}
	
	public String getXML() {
		return "<name>" 
		+ this.modsNamePart.getModsNamePartXML()
		+ this.modsRole.getModsRoleXML()
		+ "</name>";
	}
}
