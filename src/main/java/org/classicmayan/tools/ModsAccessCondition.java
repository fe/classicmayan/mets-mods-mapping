package org.classicmayan.tools;

import java.util.ArrayList;
import java.util.List;

/**
 * 
 * @author Maximilian Brodhun State- and University Library Goettingen
 * @version 1.0.0
 * @since 20.11.2018
 */

public class ModsAccessCondition {

	private String type;
	private String refToLicense;
	private String displayLabel;
	private String accessConditionValue;
	public ModsCopyRight modsCopyRight;
	private String xmlCode;
	
	private Type acType;
	private DisplayLabel acDisplayLabel;


	private List<ModsCopyRight> modsCopyRightsList = new ArrayList<ModsCopyRight>();
	
	public ModsCopyRight addModsCopyRight(ModsCopyRight modsCopyRight) {
		modsCopyRightsList.add(modsCopyRight);
		this.modsCopyRight = modsCopyRight;
		return modsCopyRight;
	}
	
	public DisplayLabel getAcDisplayLabel() {
		return acDisplayLabel;
	}

	public void setAcDisplayLabel(DisplayLabel acDisplayLabel) {
		this.acDisplayLabel = acDisplayLabel;
	}

	public Type getAcType() {
		return acType;
	}

	public void setAcType(Type acType) {
		this.acType = acType;
	}

	//Default Constructor
	public ModsAccessCondition() {
		setXmlCode("<accessCondition>");
	}
	
	public ModsAccessCondition(Type type, DisplayLabel displayLabel, String value){
		this.setAcType(acType);
		this.setAcDisplayLabel(displayLabel);
		this.setAccessConditionValue(value);

		setXmlCode("<accessCondition type=\"" + 
			type.getType() + "\" displayLabel=\"" + 
			displayLabel.getDisplayLabel() + "\">");		
	}

	public ModsAccessCondition(String type, String displayLabel) {
		this.type = type;
		this.displayLabel = displayLabel;
		
		setXmlCode("<accessCondition type=\"" + this.getType() + "\" displayLabel=\"" + this.getDisplayLabel() + "\">");		
	}
	
	public ModsAccessCondition(String type, String refToLicense, String displayLabel) {
		this.type = type;
		this.refToLicense = refToLicense;
		this.displayLabel = displayLabel;
		setXmlCode("<accessCondition type=\"" + this.getType() + "\" xlink:href=\"" + this.getRefToLicense() + "\" displayLabel=\"" + this.getDisplayLabel() + "\">");
	}
	
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getRefToLicense() {
		return refToLicense;
	}
	public void setRefToLicense(String refToLicense) {
		this.refToLicense = refToLicense;
	}
	
	/*public String getModsAccessConditionXML() {
		if(this.getDisplayLabel()==null) {			
			return "<accessCondition type=\"" + getType() + "\" xlink:href=\"" + getRefToLicense() + "\" displayLabel=\"CC-BY-Lizenz (4.0)\">cc-by_4</accessCondition>";
		}else {
			
			return "<accessCondition type=\"" + getType() +  "\" displayLabel=\"" + this.getDisplayLabel() + "\">"
					+ this.getAccessConditionValue()
					+ "</accessCondition>"
					
					+"<accessCondition>" +
						this.modsCopyRight.getModsCopyRightXML() 
					+ "</accessCondition>";
		}
		
	}*/
	
	public String getXML() {
	
		String copyRightXML = "";
		for(ModsCopyRight modsCopyRight : modsCopyRightsList) {
			copyRightXML += modsCopyRight.getXMLFine();
		}
		if(this.getAccessConditionValue() != null) {
			return this.getXmlCode() + this.getAccessConditionValue() + "</accessCondition>";
		}else {			
			return this.getXmlCode() + copyRightXML + "</accessCondition>";
		}
		
	}
	
		
	public String getDisplayLabel() {
		return displayLabel;
	}
	public void setDisplayLabel(String displayLabel) {
		this.displayLabel = displayLabel;
	}
	public String getAccessConditionValue() {
		return accessConditionValue;
	}
	public void setAccessConditionValue(String accessConditionValue) {
		this.accessConditionValue = accessConditionValue;
	}

	public String getXmlCode() {
		return xmlCode;
	}

	public void setXmlCode(String xmlCode) {
		this.xmlCode = xmlCode;
	}
}
