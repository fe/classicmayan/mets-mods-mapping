package org.classicmayan.tools;

import java.util.ArrayList;
import java.util.List;

/**
 * 
 * @author Maximilian Brodhun State- and University Library Goettingen
 * @version 1.0.0
 * @since 20.11.2018
 */

public class Form {

	private String lang;
	private String type;
	private String valueURI;
	private String formValue;	

	private List<Form> forms = new ArrayList<Form>();
	
	public Form(List<Form> forms) {
		this.forms = forms;
	}
	
	public Form(String lang, String type, String valueURI, String formValue) {
		this.lang = lang;
		this.type = type;
		this.valueURI = valueURI;
		this.formValue = formValue;
	}
	
	public Form() {
		
	}
	
	public String getLang() {
		return lang;
	}

	public void setLang(String lang) {
		this.lang = lang;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getValueURI() {
		return valueURI;
	}

	public void setValueURI(String valueURI) {
		this.valueURI = valueURI;
	}

	public String getFormValue() {
		return formValue;
	}

	public void setFormValue(String formValue) {
		this.formValue = formValue;
	}
	
	public String getFormXML() {
		return "<form xml:lang=\"" + getLang() + "\" type=\"" + getType() + "\" valueURI=\"" + getValueURI() + "\">" + getFormValue() + "</form>";
	}
	
	public String buildAllForms() {

		String forms="";		
		for(Form form : this.forms){
			forms = forms.concat(form.getFormXML());
		}
		return forms;
	}

}
