package org.classicmayan.tools;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Maximilian Brodhun, SUB Göttingen
 * @version 2022-08-18
 * @since 2018-11-19
 */

public class Mods {

  private static final String MODS_START_TAG =
      "<mods xmlns=\"http://www.loc.gov/mods/v3\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"\n xsi:schemaLocation=\"http://www.loc.gov/mods/v3 http://www.loc.gov/standards/mods/v3/mods-3-7.xsd\">";
  private static final String MODS_CLOSE_TAG = "</mods>";

  ModsRecordInfo modsRecordInfo;
  ModsTitleInfo modsTitleInfo;
  ModsLocation modsLocation;
  ModsTypeOfResource modsTypeOfResource;
  ModsOriginInfo modsOriginInfo;
  ModsGenre modsGenre;
  ModsPhysicalDescription modsPhysicalDescription;
  ModsRelatedItem modsRelatedItem;
  ModsNote modsNote;
  ModsAccessCondition modsAccessCondition;

  List<ModsRecordInfo> modsRecordInfoList = new ArrayList<ModsRecordInfo>();
  List<ModsTitleInfo> modsTitleInfoList = new ArrayList<ModsTitleInfo>();
  List<ModsGenre> modsGenres = new ArrayList<ModsGenre>();
  List<ModsAccessCondition> modsAccessConditions = new ArrayList<ModsAccessCondition>();
  List<ModsName> modsNames = new ArrayList<ModsName>();
  List<ModsOriginInfo> originInfos = new ArrayList<ModsOriginInfo>();

  private String textgridURI;

  /**
   * 
   */
  public Mods() {
    //
  }

  /**
   * @param name
   */
  public void addModsName(ModsName name) {
    this.modsNames.add(name);
  }

  /**
   * @param modsRecordInfo
   * @return
   */
  public ModsRecordInfo addRecordInfoToMods(ModsRecordInfo modsRecordInfo) {
    this.modsRecordInfoList.add(modsRecordInfo);
    this.modsRecordInfo = modsRecordInfo;
    return modsRecordInfo;
  }

  /**
   * @param modsTitleInfo
   * @return
   */
  public ModsTitleInfo addModsTitleInfo(ModsTitleInfo modsTitleInfo) {
    this.modsTitleInfoList.add(modsTitleInfo);
    this.modsTitleInfo = modsTitleInfo;
    return modsTitleInfo;
  }

  /**
   * @param modsTypeOfResource
   * @return
   */
  public ModsTypeOfResource addModsTypeOfResource(ModsTypeOfResource modsTypeOfResource) {
    this.modsTypeOfResource = modsTypeOfResource;
    return modsTypeOfResource;
  }

  /**
   * @param modsLocation
   * @return
   */
  public ModsLocation addModsLocation(ModsLocation modsLocation) {
    this.modsLocation = modsLocation;
    return modsLocation;
  }

  /**
   * @param modsOriginInfo
   * @return
   */
  public ModsOriginInfo addModsOriginInfo(ModsOriginInfo modsOriginInfo) {
    this.originInfos.add(modsOriginInfo);
    this.modsOriginInfo = modsOriginInfo;
    return modsOriginInfo;
  }

  /**
   * @param modsGenre
   * @return
   */
  public ModsGenre addModsGenre(ModsGenre modsGenre) {
    this.modsGenres.add(modsGenre);
    this.modsGenre = modsGenre;
    return modsGenre;
  }

  /**
   * @param modsPhysicalDescription
   * @return
   */
  public ModsPhysicalDescription addModsPhysicalDescription(
      ModsPhysicalDescription modsPhysicalDescription) {
    this.modsPhysicalDescription = modsPhysicalDescription;
    return modsPhysicalDescription;
  }

  /**
   * @param modsRelatedItem
   * @return
   */
  public ModsRelatedItem addModsRelatedItem(ModsRelatedItem modsRelatedItem) {
    this.modsRelatedItem = modsRelatedItem;
    return modsRelatedItem;
  }

  /**
   * @param modsNote
   * @return
   */
  public ModsNote addModsNote(ModsNote modsNote) {
    this.modsNote = modsNote;
    return modsNote;
  }

  /**
   * @param modsAccessCondition
   * @return
   */
  public ModsAccessCondition addModsAccessCondition(ModsAccessCondition modsAccessCondition) {
    this.modsAccessConditions.add(modsAccessCondition);
    this.modsAccessCondition = modsAccessCondition;
    return modsAccessCondition;
  }

  /**
   * @return
   * @throws ParseException
   */
  public String getModsXML() throws ParseException {

    String relatedItem;
    try {
      relatedItem = this.modsRelatedItem.buildAllModsRelatedItems();
    } catch (NullPointerException n) {
      relatedItem = "";
    }
    String modsLocation;
    try {
      modsLocation = this.modsLocation.getModsLocationXML();
    } catch (NullPointerException n) {
      modsLocation = "";
    }
    String genres = "";
    for (ModsGenre modsGenre : this.modsGenres) {
      genres += modsGenre.getXML();
    }
    try {
      MODS_START_TAG.concat(this.modsNote.buildAllModsNotes());
    } catch (NullPointerException e) {
      // FIXME CATCH NPE??? AARRRRRR!!!!!!
    }
    String physicalDescription = "";
    if (!this.modsPhysicalDescription.getModsPhysicalDescriptionXML()
        .equals("<physicalDescription></physicalDescription>")) {
      physicalDescription = this.modsPhysicalDescription.getModsPhysicalDescriptionXML();
      // System.out.println(physicalDescription);
    }
    return MODS_START_TAG +
        this.modsRecordInfo.getRecordInfoXML() +
        this.modsTitleInfo.getModsTitleInfoXML() +
        this.modsTypeOfResource.getTypeOfReosourceXML() +
        modsLocation +
        this.modsOriginInfo.getModsOriginInfoXML() +
        genres +
        // modsGenre.getModsGenreXML() +
        physicalDescription +
        relatedItem +
        // modsNote.buildAllModsNotes() +
        this.modsAccessCondition.getXML() +
        MODS_CLOSE_TAG;
  }

  /**
   * @return
   * @throws ParseException
   */
  public String getXML() throws ParseException {

    String recInfXml = "";

    for (ModsRecordInfo recInf : this.modsRecordInfoList) {
      recInfXml += recInf.getXML();
    }

    String titleInfoXml = "";
    for (ModsTitleInfo modsTitleInfo : this.modsTitleInfoList) {
      titleInfoXml += modsTitleInfo.getXML();
    }

    String accessConditionXml = "";
    for (ModsAccessCondition accessCondition : this.modsAccessConditions) {
      accessConditionXml += accessCondition.getXML();
    }
    String namesXml = "";
    for (ModsName name : this.modsNames) {
      namesXml += name.getXML();
    }
    String genres = "";
    for (ModsGenre modsGenre : this.modsGenres) {
      genres += modsGenre.getXMLFine();
    }
    String originInfoXML = "";
    for (ModsOriginInfo originInfo : this.originInfos) {
      originInfoXML += originInfo.getXML();
    }
    return MODS_START_TAG + recInfXml + titleInfoXml + accessConditionXml + namesXml + genres
        + originInfoXML + MODS_CLOSE_TAG;
  }

  /**
   * @return
   */
  public String getTextgridURI() {
    return this.textgridURI;
  }

  /**
   * @param textgridURI
   */
  public void setTextgridURI(String textgridURI) {
    this.textgridURI = textgridURI;
  }

}
