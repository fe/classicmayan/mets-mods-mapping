package org.classicmayan.tools;

public class TextGridUri {
    private String value;
    
    public TextGridUri(String value){
        this.value = value;
    }
    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    
}
