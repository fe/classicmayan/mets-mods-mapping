package org.classicmayan.tools;

import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.text.ParseException;
import java.time.Instant;
import java.util.logging.Logger;
import org.apache.commons.io.IOUtils;
import org.json.JSONException;
import org.json.JSONObject;
import info.textgrid.clients.tgcrud.TextGridObject;

/**
 *
 */
public class ImageMetsMods {

  // **
  // FINALS
  // **

  private static final String JSON_ERROR_STRING = "error getting json data string";
  private static final String RESERVED = "RESERVED";

  // **
  // STATICS
  // **

  protected static Logger log = Logger.getLogger(ImageMetsMods.class.getName());

  // **
  // CLASS
  // **

  protected JSONObject imageMetadata = new JSONObject();
  private String ID;
  private Mets mets;
  private String creationDate;
  private String changeDate;
  private String creator;
  private String rightsHolder;
  private String mimeType;
  private String thumbnailURL;
  private String defaultURL;
  private String title;
  private String license;
  private String licenseNote;
  private String creationDateOfImage;

  // **
  // CONSTRUCTORS
  // **

  /**
   * @param tgJSONImageObject
   * @param creationDate
   * @param changeDate
   * @throws IOException
   */
  public ImageMetsMods(TextGridObject tgJSONImageObject, String creationDate, String changeDate)
      throws IOException {

    String uri = tgJSONImageObject.getMetadatada().getObject().getGeneric().getGenerated()
        .getTextgridUri().getValue();

    log.fine("creating imagemetsmods from textgrid uri: " + uri);

    InputStream inputStream = tgJSONImageObject.getData();
    String mediumMetadataJSONEncoded = IOUtils.toString(inputStream, StandardCharsets.UTF_8).trim();

    log.fine("json from textrgid uri: " + mediumMetadataJSONEncoded);

    if (!mediumMetadataJSONEncoded.startsWith("{")) {
      throw new IOException("textgrid uri " + uri + " does not contain a json object!");
    }

    // Create JSON from TextGrid object.
    this.imageMetadata = new JSONObject(mediumMetadataJSONEncoded);
    this.setID(Integer.toString(this.imageMetadata.getInt("id")));
    // Get dates from params if formatting shall be UTC!
    this.setCreationDate(creationDate);
    this.setChangeDate(changeDate);

    // Get settings from JSON.
    settingsFromJSON(uri);

    // Build METS.
    buildMets();
  }

  /**
   * @param imageMetadata
   * @deprecated
   */
  @Deprecated
  public ImageMetsMods(JSONObject imageMetadata) {

    // FIXME Is it really deprecated? As I see it, image metadata comes from tgcrud now, and not
    // directly from JSON input??

    log.fine("creating imagemetsmods from json object: " + imageMetadata);

    // Set JSON from parameter.
    this.imageMetadata = imageMetadata;
    this.setID(Integer.toString(imageMetadata.getInt("id")));
    // Set dates from JSON directly.
    // TODO If used for MODS generation, please look at date formatting (ISO or UTC)!
    this.setCreationDate(imageMetadata.getString("created_at"));
    this.setChangeDate(imageMetadata.getString("updated_at"));

    // Get settings from JSON.
    settingsFromJSON(this.getID());

    // Build METS.
    buildMets();
  }

  /**
   * @param conedakorID
   * @throws JSONException
   * @throws IOException
   * @deprecated
   */
  @Deprecated
  public ImageMetsMods(ConedaKorID conedakorID) throws JSONException, IOException {

    // FIXME Is it really deprecated? As I see it, image metadata comes from tgcrud now, not from
    // ConedaKOR? anymore? Would that not be much faster??

    log.fine("creating imagemetsmods from conedakor id: " + conedakorID.getId());

    this.ID = conedakorID.getId();
    this.imageMetadata = ConedaKorQueries.getMediumMetadaSetForMetsModsByDirectID(this.ID);

    this.setID(Integer.toString(this.imageMetadata.getInt("id")));
    this.setCreationDate(this.imageMetadata.getString("created_at"));
    this.setChangeDate(this.imageMetadata.getString("updated_at"));
    this.setCreator(this.imageMetadata.getJSONObject("dataset").getString("creator"));
    this.setRightsHolder(this.imageMetadata.getJSONObject("dataset").getString("rights_holder"));
    this.setMimeType(this.imageMetadata.getJSONObject("medium").getString("content_type"));
    this.setThumbnailURL(
        this.imageMetadata.getJSONObject("medium").getJSONObject("url").getString("thumbnail"));
    this.setDefaultURL(
        this.imageMetadata.getJSONObject("medium").getJSONObject("url").getString("original"));
    this.setTitle(this.imageMetadata.getJSONObject("dataset").getString("image_description"));
    this.setLicense(this.imageMetadata.getJSONObject("dataset").getString("license"));
    if (getLicense().equals("RESERVED")) {
      this.setLicenseNote(this.imageMetadata.getJSONObject("dataset").getString("license_note"));
    }
    this.setCreationDateOfImage(
        this.imageMetadata.getJSONObject("dataset").getString("date_time_created"));

    buildMets();
  }

  // **
  // DOING
  // **

  /**
   * @return
   */
  public Mets buildMets() {

    this.mets = new Mets();

    // METS creation date is NOW!
    String metsCreationDate = ClassicMayanMetsMods.UTC_FORMATTER.format(Instant.now());

    log.fine("building mets with CREATEDATE " + metsCreationDate);

    this.mets.addMetsHeader(new MetsHdr(metsCreationDate));
    this.mets.addMetsDmdSec(buildDmdSec());
    this.mets.addMetsAmdSec(buildAmdSec());
    this.mets.addMetsFileSec(buildFileSec());
    this.mets.addMetsStructMap(buildStructMap("PHYSICAL"));
    this.mets.addMetsStructMap(buildStructMap("LOGICAL"));
    this.mets.addMetsStructLink(buildStructLink());

    return this.mets;
  }

  /**
   * @return
   */
  public MetsStructLink buildStructLink() {

    MetsStructLink structLink = new MetsStructLink();
    structLink.addMetsSmLink(new MetsSmLink("log_" + getID(), "phys_file_" + getID()));

    return structLink;
  }

  /**
   * @param type
   * @return
   */
  public MetsStructMap buildStructMap(String type) {

    MetsStructMap structMap = new MetsStructMap(type);

    // String artefactTitle = imageMetadata.get("<title>").toString();
    if (type.equals("PHYSICAL")) {

      MetsDiv div = new MetsDiv("phys_file_" + getID(), "page");
      structMap.addMetsDiv(div);
      div.addMetsFptr(new MetsFptr("conedaKor_" + getID() + "_t"));
      div.addMetsFptr(new MetsFptr("conedaKor_" + getID() + "_d"));
    }
    if (type.equals("LOGICAL")) {
      structMap
          .addMetsDiv(new MetsDiv(
              "log_" + getID(),
              "conedaKor_" + getID(),
              "conedaKor_" + getID() + "_amd",
              "document",
              getTitle().replace("\"", "").replace("\\", "")));
    }

    return structMap;
  }

  /**
   * @return
   */
  public MetsFileSec buildFileSec() {

    MetsFileSec fileSec = new MetsFileSec();
    fileSec.addMetsFileGrp(buildFileGrp("THUMBS"));
    fileSec.addMetsFileGrp(buildFileGrp("DEFAULT"));

    return fileSec;
  }

  /**
   * @param usage
   * @return
   */
  public MetsFileGrp buildFileGrp(String usage) {

    String imageURL = "";
    String identifierSuffix = "";
    if (usage.equals("THUMBS")) {
      imageURL = "https://classicmayan.kor.de.dariah.eu" + getThumbnailURL();
      identifierSuffix = "_t";
    }
    if (usage.equals("DEFAULT")) {
      imageURL = "https://classicmayan.kor.de.dariah.eu" + getDefaultURL();
      identifierSuffix = "_d";
    }

    MetsFileGrp fileGrp = new MetsFileGrp(new Use(usage));
    fileGrp.addMetsFile(
        new MetsFile(
            "conedaKor_" + getID() + identifierSuffix,
            getMimeType(),
            getCreationDate()))
        .addMetsFlocat(new MetsFlocat("URL", imageURL));

    return fileGrp;
  }

  /**
   * @return
   */
  public MetsDmdSec buildDmdSec() {

    MetsDmdSec metsDmdSec = new MetsDmdSec("conedaKor_" + getID());
    MetsMdWrap metsMdWrap = new MetsMdWrap(new MimeType(getMimeType()), new MdType("MODS"));
    MetsXmlData xmlData = new MetsXmlData();

    metsDmdSec.addMdWrap(metsMdWrap);
    metsMdWrap.addXmlData(xmlData);
    xmlData.addMods(buildMods());

    return metsDmdSec;
  }

  /**
   * @return
   */
  public MetsAmdSec buildAmdSec() {

    MetsAmdSec amdSec = new MetsAmdSec("conedaKor_" + getID() + "_amd");
    amdSec.addMetsRightsMD(buildRightsMDForAMD());
    amdSec.addMetsDigiProvMD(buildDigiProvMD());

    return amdSec;
  }

  /**
   * @return
   */
  public MetsRightsMD buildRightsMDForAMD() {

    MetsRightsMD rightsMD = new MetsRightsMD("conedaKor_" + getID() + "_r");
    MetsMdWrap metsMdWrapForMetsRightsMD = new MetsMdWrap(
        new MimeType(getMimeType()),
        new MdType("MODS"),
        new OtherMdType("DIVRIGHTS"));

    MetsXmlData xmlDataForMetsAmdSec = new MetsXmlData();
    xmlDataForMetsAmdSec.addMetsRights(new MetsRights(
        new MetsOwner("Text Database and Dictionary of Classic Mayan"),
        new MetsOwnerSiteURL("http://www.mayadictionary.de"),
        new MetsRightsSponsor(
            "Nordrhein-Westf&#228;lische Akademie der Wissenschaften und der K&#252;nste, Union der Wissenschaftsakademien")));

    metsMdWrapForMetsRightsMD.addXmlData(xmlDataForMetsAmdSec);
    rightsMD.addMetsMdWrap(metsMdWrapForMetsRightsMD);

    return rightsMD;
  }

  /**
   * @return
   */
  public MetsDigiProvMD buildDigiProvMD() {

    MetsDigiProvMD digiProvMD = new MetsDigiProvMD("conedaKor_" + getID() + "_digi");
    MetsMdWrap metsMdWrapForDigiProvMD = new MetsMdWrap(
        new MimeType(getMimeType()),
        new MdType("MODS"),
        new OtherMdType("DIVRIGHTS"));
    MetsXmlData xmlDataForDigiProvMD = new MetsXmlData();
    MetsLinks links = new MetsLinks();
    MetsReference reference = new MetsReference("Image in CoedaKor Archive",
        "https://classicmayan.kor.de.dariah.eu/#/entities/" + getID());
    links.addMetsReference(reference);
    xmlDataForDigiProvMD.addMetsLinks(links);
    metsMdWrapForDigiProvMD.addXmlData(xmlDataForDigiProvMD);
    digiProvMD.addMetsMdWrap(metsMdWrapForDigiProvMD);

    return digiProvMD;
  }

  /**
   * @return
   */
  public MetsRightsMD buildRightsMD() {

    MetsRightsMD rightsMD = new MetsRightsMD("conedaKor_" + getID() + "_r");

    return rightsMD;
  }

  /**
   * @return
   */
  public Mods buildMods() {

    Mods mods = new Mods();
    mods.addRecordInfoToMods(buildRecordInfo());
    mods.addModsTitleInfo(buildTitleInfo());
    mods.addModsAccessCondition(buildAccessCondition());
    if (getLicense().equals("RESERVED")) {
      mods.addModsAccessCondition(buildAccessConditionReservedCopyRights());
    }
    mods.addModsName(buildModsName("cph"));
    mods.addModsName(buildModsName("cre"));
    mods.addModsGenre(buildGenre());
    mods.addModsOriginInfo(buildOriginInfo());

    return mods;
  }

  /**
   * @return
   */
  public ModsOriginInfo buildOriginInfo() {

    ModsOriginInfo originInfo = new ModsOriginInfo();
    originInfo.addDateCreated(
        new ModsDateCreated(new Encoding(ClassicMayanMetsMods.ISO_8601), getCreationDateOfImage()));
    return originInfo;
  }

  public ModsGenre buildGenre() {
    ModsGenre genre = new ModsGenre(
        new Authority("marcgt"),
        "illustration");

    return genre;
  }

  /**
   * @param roleTerm
   * @return
   */
  public ModsName buildModsName(String roleTerm) {

    ModsName name = new ModsName(
        new ModsNamePart(getRightsHolder()),
        new ModsRole(
            new ModsRoleTerm("code", "marcelator", roleTerm)));
    return name;
  }

  /**
   * @return
   */
  public ModsRecordInfo buildRecordInfo() {

    // TODO: get the correct date fields
    // TODO: not parse any time
    ModsRecordInfo recordInfo = new ModsRecordInfo(
        new RecordIdentifier(this.getID()),
        new RecordCreationDate(ClassicMayanMetsMods.ISO_8601, getCreationDate()),
        new RecordChangeDate(ClassicMayanMetsMods.ISO_8601, getChangeDate()));

    return recordInfo;
  }

  /**
   * @return
   */
  public ModsTitleInfo buildTitleInfo() {

    ModsTitleInfo titleInfo = new ModsTitleInfo();
    titleInfo.addTitle(new Title(getTitle(), "en"));

    return titleInfo;
  }

  /**
   * @return
   */
  public ModsAccessCondition buildAccessCondition() {

    ModsAccessCondition accessCondition = new ModsAccessCondition(
        new Type("use and reproduction"),
        new DisplayLabel("kor_license"),
        getLicense());

    return accessCondition;
  }

  /**
   * @return
   */
  public ModsAccessCondition buildAccessConditionReservedCopyRights() {

    ModsAccessCondition reservedCopyrights = new ModsAccessCondition();
    ModsCopyRight copyRightsForReserverdAccessCondition = new ModsCopyRight(
        new CopyrightStats("copyrighted"),
        new PublicationStatus("unpublished"));
    copyRightsForReserverdAccessCondition.addModsGeneralNote(new ModsGeneralNote(getLicenseNote()));

    reservedCopyrights.addModsCopyRight(copyRightsForReserverdAccessCondition);

    return reservedCopyrights;
  }

  // **
  // PRIVATE
  // **

  /**
   * @param theObject
   * @param theKey
   * @param theURI
   * @return
   */
  private static String getJsonErrorString(String theObject, String theKey, String theURI) {
    return JSON_ERROR_STRING + " " + theObject + " '" + theKey + "' from " + theURI;
  }

  /**
   * @param theURI
   * @return
   */
  private String getCreatorFromJson(String theURI) {

    String result = "";

    String object = "dataset";
    String key = "creator";
    try {
      result = this.imageMetadata.getJSONObject(object).getString(key);
    } catch (JSONException je) {
      log.warning(getJsonErrorString(object, key, theURI) + "! " + je.getMessage());
    }

    return result;
  }

  /**
   * @param theURI
   * @return
   */
  private String getRightsHolderFromJson(String theURI) {

    String result = "";

    String object = "dataset";
    String key = "rights_holder";
    try {
      result = this.imageMetadata.getJSONObject(object).getString(key);
    } catch (JSONException je) {
      log.warning(getJsonErrorString(object, key, theURI) + "! " + je.getMessage());
    }

    return result;
  }

  /**
   * @param theURI
   * @return
   */
  private String getMimeTypeFromJson(String theURI) {

    String result = "";

    String object = "medium";
    String key = "content_type";
    try {
      result = this.imageMetadata.getJSONObject(object).getString(key);
    } catch (JSONException je) {
      log.warning(getJsonErrorString(object, key, theURI) + "! " + je.getMessage());
    }

    return result;
  }

  /**
   * @param theURI
   * @return
   */
  private String getThumbnailURLFromJson(String theURI) {

    String result = "";

    String object = "medium";
    String url = "url";
    String key = "thumbnail";
    try {
      result = this.imageMetadata.getJSONObject(object).getJSONObject(url).getString(key);
    } catch (JSONException je) {
      log.warning(getJsonErrorString(object, key, theURI) + "! " + je.getMessage());
    }

    return result;
  }

  /**
   * @param theURI
   * @return
   */
  private String getDefaultURLFromJson(String theURI) {

    String result = "";

    String key = "original";

    String object = "medium";
    String url = "url";
    try {
      result = this.imageMetadata.getJSONObject(object).getJSONObject(url).getString(key);
    } catch (JSONException je) {
      log.warning(getJsonErrorString(object, key, theURI) + "! " + je.getMessage());
    }

    return result;
  }

  /**
   * @param theURI
   * @return
   */
  private String getImageDescriptionFromJson(String theURI) {

    String result = "";

    String object = "dataset";
    String key = "image_description";
    try {
      result = this.imageMetadata.getJSONObject(object).getString(key);

    } catch (JSONException je) {
      log.warning(getJsonErrorString(object, key, theURI) + "! " + je.getMessage());
    }

    return result;
  }

  /**
   * @param theURI
   * @return
   */
  private String getLicenseFromJson(String theURI) {

    String result = "";

    String object = "dataset";
    String key = "license";
    try

    {
      result = this.imageMetadata.getJSONObject(object).getString(key);
    } catch (JSONException je) {
      log.warning(getJsonErrorString(object, key, theURI) + "! " + je.getMessage());
    }

    return result;
  }

  /**
   * @param theURI
   * @return
   */
  private String getLicenseNoteFromJson(String theURI) {

    String result = "";

    String object = "dataset";
    String key = "license_note";
    try {
      result = this.imageMetadata.getJSONObject(object).getString(key);
    } catch (JSONException je) {
      log.warning(getJsonErrorString(object, key, theURI) + "! " + je.getMessage());
    }

    return result;
  }

  /**
   * @param theURI
   */
  private void settingsFromJSON(String theURI) {

    this.setCreator(getCreatorFromJson(theURI));
    this.setRightsHolder(getRightsHolderFromJson(theURI));
    this.setMimeType(getMimeTypeFromJson(theURI));
    this.setThumbnailURL(getThumbnailURLFromJson(theURI));
    this.setDefaultURL(getDefaultURLFromJson(theURI));
    this.setTitle(getImageDescriptionFromJson(theURI));
    this.setLicense(getLicenseFromJson(theURI));
    if (getLicense().equals(RESERVED)) {
      this.setLicenseNote(getLicenseNoteFromJson(theURI));
    }
    this.setCreationDateOfImage(getCreationDateFromJson(theURI));
  }

  /**
   * @param theURI
   * @return
   */
  private String getCreationDateFromJson(String theURI) {

    String result = "";

    String object = "dataset";
    String key = "date_time_created";
    try {
      result = this.imageMetadata.getJSONObject(object).getString(key);
    } catch (JSONException je) {
      log.warning(getJsonErrorString(object, key, theURI) + "! " + je.getMessage());
    }

    return result;
  }

  // **
  // Getters & Setters
  // **

  /**
   * @return
   */
  public JSONObject getImageMetadata() {
    return this.imageMetadata;
  }

  /**
   * @return
   */
  public String getID() {
    return this.ID;
  }

  /**
   * @param iD
   */
  public void setID(String iD) {
    this.ID = iD;
  }

  /**
   * @return
   * @throws ParseException
   */
  public String getXML() throws ParseException {
    return this.mets.getXML();
  }

  /**
   * @return
   */
  public String getCreationDateOfImage() {
    return this.creationDateOfImage;
  }

  /**
   * @param creationDateOfImage
   */
  public void setCreationDateOfImage(String creationDateOfImage) {
    this.creationDateOfImage = creationDateOfImage;
  }

  /**
   * @return
   */
  public String getLicenseNote() {
    return this.licenseNote;
  }

  /**
   * @param licenseNote
   */
  public void setLicenseNote(String licenseNote) {
    this.licenseNote = licenseNote;
  }

  /**
   * @return
   */
  public String getLicense() {
    return this.license;
  }

  /**
   * @param license
   */
  public void setLicense(String license) {
    this.license = license;
  }

  /**
   * @return
   */
  public String getTitle() {
    return this.title;
  }

  /**
   * @param title
   */
  public void setTitle(String title) {
    this.title = title;
  }

  /**
   * @return
   */
  public String getDefaultURL() {
    return this.defaultURL;
  }

  /**
   * @param defaultURL
   */
  public void setDefaultURL(String defaultURL) {
    this.defaultURL = defaultURL;
  }

  /**
   * @return
   */
  public String getThumbnailURL() {
    return this.thumbnailURL;
  }

  /**
   * @param thumbnailURL
   */
  public void setThumbnailURL(String thumbnailURL) {
    this.thumbnailURL = thumbnailURL;
  }

  /**
   * @return
   */
  public String getMimeType() {
    return this.mimeType;
  }

  /**
   * @param mimeType
   */
  public void setMimeType(String mimeType) {
    this.mimeType = mimeType;
  }

  /**
   * @return
   */
  public String getRightsHolder() {
    return this.rightsHolder;
  }

  /**
   * @param rightsHolder
   */
  public void setRightsHolder(String rightsHolder) {
    this.rightsHolder = rightsHolder;
  }

  /**
   * @return
   */
  public String getCreator() {
    return this.creator;
  }

  /**
   * @param creator
   */
  public void setCreator(String creator) {
    this.creator = creator;
  }

  /**
   * @return
   */
  public String getChangeDate() {
    return this.changeDate;
  }

  /**
   * @param changeDate
   */
  public void setChangeDate(String changeDate) {
    this.changeDate = changeDate;
  }

  /**
   * @return
   */
  public String getCreationDate() {
    return this.creationDate;
  }

  /**
   * @param creationDate
   */
  public void setCreationDate(String creationDate) {
    this.creationDate = creationDate;
  }

}
