package org.classicmayan.tools;

public class LocationDataObject {

	private String discoveryPlace;
	private String findingSpot;
	private boolean inSitu;
	private String currentCustody;
	
	public String getDiscoveryPlace() {
		return discoveryPlace;
	}
	public void setDiscoveryPlace(String discoveryPlace) {
		this.discoveryPlace = discoveryPlace;
	}
	public String getFindingSpot() {
		return findingSpot;
	}
	public void setFindingSpot(String findingSpot) {
		this.findingSpot = findingSpot;
	}
	public boolean isInSitu() {
		return inSitu;
	}
	public void setInSitu(boolean inSitu) {
		this.inSitu = inSitu;
	}
	public String getCurrentCustody() {
		return currentCustody;
	}
	public void setCurrentCustody(String currentCustody) {
		this.currentCustody = currentCustody;
	}
}
