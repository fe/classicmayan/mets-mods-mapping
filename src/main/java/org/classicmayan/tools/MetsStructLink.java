package org.classicmayan.tools;

import java.util.ArrayList;
import java.util.List;

/**
 * 
 * @author Maximilian Brodhun State- and University Library Goettingen
 * @version 1.0.0
 * @since 23.11.2018
 */

public class MetsStructLink {
	
	MetsSmLink metsSmLink;
	private List<MetsStructLink> metsStructLinks = new ArrayList<MetsStructLink>();
	private List<MetsSmLink> metsSmLinks = new ArrayList<MetsSmLink>();
	
	public MetsStructLink(List<MetsStructLink> metsStructLinks) {
		this.metsStructLinks = metsStructLinks;
	}
	
	public MetsStructLink() {
		
	}
	
	MetsSmLink addMetsSmLink(MetsSmLink metsSmLink) {
		metsSmLinks.add(metsSmLink);
		this.metsSmLink = metsSmLink;
		return metsSmLink;
	}
	
	public String getMetsStructLinkXML() {
		return "<structLink>" + metsSmLink.buildAllSmLInks() + "</structLink>";
	}

	public String buildAllStructLinks() {

		String structLinks="";
		
		for(MetsStructLink structLink : this.metsStructLinks){
			structLinks = structLinks.concat(structLink.getMetsStructLinkXML());
		}
		return structLinks;
	}
	
	public String getXML() {
		String xml = "";
		
		for(MetsSmLink metsSmLink : metsSmLinks) {
			xml += metsSmLink.getXML();
		}
		
		return "<structLink>" + xml + "</structLink>";
	}
	
	
}
