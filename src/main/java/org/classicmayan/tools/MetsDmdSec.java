package org.classicmayan.tools;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

/**
 * 
 * @author Maximilian Brodhun State- and University Library Goettingen
 * @version 1.0.0
 * @since 22.11.2018
 */

public class MetsDmdSec {
	
	private String id;
    public MetsMdWrap metsMdWrap;
	
	
	public MetsDmdSec(){
		/**
		 * Default Counstructor
		 */
	}

	public MetsDmdSec(String id){
		this.id = id;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}
	
	public MetsMdWrap addMdWrap(MetsMdWrap metsMdWrap) {
		this.metsMdWrap = metsMdWrap;
		return metsMdWrap;
	}
	
	public String getMetsDmdDecXML() throws ParseException {
		
		return "<dmdSec ID=\"" + getId() + "\">" + metsMdWrap.getMetsMdWrapXML() + "</dmdSec>";
	}

	public String getXML() throws ParseException {
		
		return "<dmdSec ID=\"" + getId() + "\">" 
					+ metsMdWrap.getXML() + 
				"</dmdSec>";
	}

}
