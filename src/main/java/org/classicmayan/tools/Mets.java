package org.classicmayan.tools;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

/**
 * 
 * @author Maximilian Brodhun State- and University Library Goettingen
 * @version 1.0.0
 * @since 22.11.2018
 */

public class Mets {

	private static final String header = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n" + 
			"<?xml-model href=\"http://www.loc.gov/standards/mods/v3/mods-3-7.xsd\" type=\"application/xml\" schematypens=\"http://purl.oclc.org/dsdl/schematron\"?>";
	private static final String metsHeader = "<mets xmlns=\"http://www.loc.gov/METS/\"\n" + 
			"    xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"\n" + 
			"    xsi:schemaLocation=\"http://www.loc.gov/METS/ http://www.loc.gov/standards/mets/mets.xsd\" xmlns:dv=\"http://dfg-viewer.de/\" xmlns:xlink=\"http://www.w3.org/1999/xlink\">";
	
	MetsHdr metsHdr;
	MetsDmdSec metsDmdSec;
	MetsAmdSec metsAmdSec;
	MetsFileSec metsFileSec;
	MetsStructMap metsStructMap;
	MetsStructLink metsStructLink;	
	
	List<MetsStructMap> metsStructMaps = new ArrayList<MetsStructMap>();
	List<MetsDmdSec> dmdSecs = new ArrayList<MetsDmdSec>();
	List<MetsAmdSec> amdSecs = new ArrayList<MetsAmdSec>();
	List<MetsFileSec> fileSecs = new ArrayList<MetsFileSec>();
	List<MetsStructLink> structLinks = new ArrayList<MetsStructLink>();	

	public MetsHdr addMetsHeader(MetsHdr metsHdr) {
		this.metsHdr = metsHdr;
		return metsHdr;
	}
	
	public MetsDmdSec addMetsDmdSec(MetsDmdSec metsDmdSec) {
		this.dmdSecs.add(metsDmdSec);
		this.metsDmdSec = metsDmdSec;
		return metsDmdSec;
	}
	
	public MetsAmdSec addMetsAmdSec(MetsAmdSec metsAmdSec) {		
		this.amdSecs.add(metsAmdSec);
		this.metsAmdSec = metsAmdSec;
		
		return metsAmdSec;
	}
	
	public MetsFileSec addMetsFileSec(MetsFileSec metsFileSec) {
		this.fileSecs.add(metsFileSec);
		this.metsFileSec = metsFileSec;
		return metsFileSec;
	}
	
	public MetsStructMap addMetsStructMap(MetsStructMap metsStructMap) {
		metsStructMaps.add(metsStructMap);
		this.metsStructMap = metsStructMap;
		return metsStructMap;
	}
	
	public MetsStructLink addMetsStructLink(MetsStructLink metsStructLink) {
		this.structLinks.add(metsStructLink);
		this.metsStructLink = metsStructLink;
		return metsStructLink;
	}
	
	public String getMetsXML() throws ParseException {
		String metsStructMapsXML = "";
		for(MetsStructMap msp : metsStructMaps) {
			metsStructMapsXML += msp.getXML();
		}
		String fileSecXML = "";
		if(metsFileSec !=null){
			fileSecXML = metsFileSec.getXML();
		}

		String metsStructLinkXML = "";
		if(metsStructLink != null){
			metsStructLinkXML = metsStructLink.getXML();
		}
		return header + metsHeader + 
				metsHdr.getMetsHdrXML() +
				metsDmdSec.getMetsDmdDecXML() +
				metsAmdSec.getMetsAmdSecXML() +
				fileSecXML +
				metsStructMapsXML +
				metsStructLinkXML +
				"</mets>";
	}

	public String getXML() throws ParseException{

		String dmdSecXML = "";

		for(MetsDmdSec metsDmdSec : dmdSecs){
			dmdSecXML += metsDmdSec.getXML();
		}

		String amdSecXML = "";
		for(MetsAmdSec metsAmdSec : amdSecs){
			amdSecXML+=metsAmdSec.getXML();
		}

		String fileSecXML = "";
		for(MetsFileSec fileSec : fileSecs){
			fileSecXML += fileSec.getXML();
		}

		String metsStructMapsXML = "";
		for(MetsStructMap msp : metsStructMaps) {
			metsStructMapsXML += msp.getXML();
		}
		String metsStructLinkXML = "";
		for(MetsStructLink msl : structLinks) {
			metsStructMapsXML += msl.getXML();
		}
		
		return header + metsHeader +
		 metsHdr.getMetsHdrXML() + 
		 dmdSecXML +
		 amdSecXML +
		 fileSecXML + 
		 metsStructMapsXML +
		 metsStructLinkXML +
		 "</mets>";
	}

}
