/*******************************************************************************
 * This software is copyright (c) 2021 by
 * 
 * State and University Library Goettingen
 *
 * This is free software. You can redistribute it and/or modify it under the terms described in the
 * GNU Lesser General Public License v3 of which you should have received a copy. Otherwise you can
 * download it from
 *
 * http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * @copyright State and University Library Goettingen
 * @license GNU Lesser General Public License v3 (http://www.gnu.org/licenses/lgpl-3.0.txt)
 * @author Maximilian Behnert-Brodhun (behnert-brodhun@sub.uni-goettingen.de)
 ******************************************************************************/



package org.classicmayan.tools;

public class Title {
	private String language;
	private String title;
	
	public Title() {}
	
	public Title(String title, String language) {
		this.title=title;
		this.language=language;
	}
	
	public String getXML() {
		return "<title xml:lang=\"" + this.getLanguage() + "\">"  + this.getTitle() + "</title>";
	}
	
	public String getLanguage() {
		return language;
	}
	public void setLanguage(String language) {
		this.language = language;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
}
