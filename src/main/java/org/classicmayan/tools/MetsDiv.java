package org.classicmayan.tools;

import java.util.ArrayList;
import java.util.List;

/**
 * 
 * @author Maximilian Brodhun State- and University Library Goettingen
 * @version 1.0.0
 * @since 23.11.2018
 */

public class MetsDiv {
	private String type;
	private String id;
	private String order;
	private String dmdid;
	private String amdid;
	private String label;

	

	private String xml;

	MetsDiv metsDiv;
	MetsFptr metsFptr;
	
	private List<MetsDiv> divList = new ArrayList<MetsDiv>();
	private List<MetsFptr> metsFptrList = new ArrayList<MetsFptr>();
	
	public MetsDiv(String id,String type) {
		this.type = type;
		this.id = id;
	}
	
	public MetsDiv(String id, String type, String order) {
		this.id = id;
		this.type = type;
		this.order = order;		
	}
	
	public MetsDiv(String id, String dmdid, String amdid, String type, String label) {
		this.id = id;
		this.dmdid = dmdid;
		this.amdid = amdid;
		this.type = type;
		this.label = label;
	}
	
	public MetsDiv() {
		this.xml = "<div></div>";
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getOrder() {
		return order;
	}
	public void setOrder(String order) {
		this.order = order;
	}
	
	 MetsDiv addMetsDiv(MetsDiv metsDiv) {
		 divList.add(metsDiv);
		 this.metsDiv = metsDiv;
		 return metsDiv;
	 }
	 
	 MetsFptr addMetsFptr(MetsFptr metsFptr) {
		 metsFptrList.add(metsFptr);
		 this.metsFptr = metsFptr;
		 return metsFptr;
	 }
	
	public String getSingleXML() {

		if(getOrder() == null && getId() == null && getType() == null){
			return this.xml;
		}

		if(getOrder() == null) {
			if(amdid!=null || dmdid!=null) {
				return "<div" +
							" ID=\"" + getId() + "\"" +
							" DMDID=\"" + getDmdid() + "\"" +
							" ADMID=\"" + getAmdid() + "\"" +
							" TYPE=\"" + getType() + "\"" +
							" LABEL=\"" + getLabel() + "\"" +
						"/>";
			}
			if(metsFptr==null) {
				return "<div" +
						" ID=\"" + getId() + "\""  + 
						" TYPE=\"" + getType() + "\">" +
						this.getXML() + 
					"</div>";
			}else {
				String fptrXML ="";
				for(MetsFptr mts : metsFptrList) {
					fptrXML += mts.getXML();
				}
				return "<div" + 
						" ID=\"" + getId() + "\""  + 
						" TYPE=\"" + getType() + "\">" +
							this.getXML() + 
							fptrXML + 
						"</div>";
			}
		}else {
			String fptrXML ="";
			for(MetsFptr mts : metsFptrList) {
				fptrXML += mts.getXML();
			}
			if(metsDiv==null) {
				return "<div" + 
						" ID=\"" + getId() + "\""  + 
						" TYPE=\"" + getType() + "\"" +
						" ORDER=\"" + getOrder() + "\">" +
							fptrXML + 
					   "</div>";
			}else {
				String fptrXML2 ="";
				for(MetsFptr mts : metsFptrList) {
					fptrXML2 += mts.getXML();
				}
				return "<div" +
						" ID=\"" + getId() + "\""  + 
						" TYPE=\"" + getType() + "\">" +
						" ORDER=\"" + getOrder() + "\"" + 
							this.getXML() + 
							fptrXML2 + 
					   "</div>";
			}
		}
	}
	 
	public String getXML() {
		String xml = "";

		for(MetsDiv metsDiv : divList) {			
			xml += metsDiv.getSingleXML();
		}

		return xml; 
	}
	
	public String getDmdid() {
		return dmdid;
	}

	public void setDmdid(String dmdid) {
		this.dmdid = dmdid;
	}

	public String getAmdid() {
		return amdid;
	}

	public void setAmdid(String amdid) {
		this.amdid = amdid;
	}

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}
}
