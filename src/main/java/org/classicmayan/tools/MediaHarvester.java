package org.classicmayan.tools;

import java.io.IOException;

import org.json.JSONException;
import org.json.JSONObject;

public class MediaHarvester{

    private int totalMedia;
    private JSONObject mediaListFromConedaKor;

    public MediaHarvester(String kindId, String perPage, String page, String from, String to) throws JSONException, IOException{
        setMediaListFromConedaKor(ConedaKorQueries.getMediaList(kindId, perPage, page, from, to));
        setTotalMedia(mediaListFromConedaKor.getInt("total"));
    }

    public JSONObject getMediaListFromConedaKor() {
        return mediaListFromConedaKor;
    }

    public void setMediaListFromConedaKor(JSONObject mediaListFromConedaKor) {
        this.mediaListFromConedaKor = mediaListFromConedaKor;
    }

    public int getTotalMedia() {
        return totalMedia;
    }

    public void setTotalMedia(int totalMedia) {
        this.totalMedia = totalMedia;
    }
}
