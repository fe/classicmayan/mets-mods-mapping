package org.classicmayan.tools;

public class CopyrightStats {
    private String status;

    public CopyrightStats(String status){
        setStatus(status);
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

}