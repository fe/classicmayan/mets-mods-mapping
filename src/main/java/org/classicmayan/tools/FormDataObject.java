package org.classicmayan.tools;

public class FormDataObject{

	private String lang;
	private String type;
	private String valueURI;
	private String formValue;
	
	public FormDataObject(String lang, String type, String valueURI, String formValue) {
		this.lang = lang;
		this.type = type;
		this.valueURI = valueURI;
		this.formValue = formValue;
	}	
	
	public String getLang() {
		return lang;
	}
	public void setLang(String lang) {
		this.lang = lang;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getValueURI() {
		return valueURI;
	}
	public void setValueURI(String valueURI) {
		this.valueURI = valueURI;
	}
	public String getFormValue() {
		return formValue;
	}
	public void setFormValue(String formValue) {
		this.formValue = formValue;
	}
	
	
}
