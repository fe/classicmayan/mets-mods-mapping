package org.classicmayan.tools;

import java.util.ArrayList;
import java.util.List;

/**
 * 
 * @author Maximilian Brodhun State- and University Library Goettingen
 * @version 1.0.0
 * @since 23.11.2018
 */

public class MetsFileGrp {
	private String use;
	MetsFile metsFile;

	private Use usage;
	private List<MetsFile> metsFiles = new ArrayList<MetsFile>();
	private List<MetsFileGrp> fileGrps = new ArrayList<MetsFileGrp>();
	
	public MetsFileGrp(List<MetsFileGrp> fileGrps) {
		this.fileGrps = fileGrps;
	}
	
	public MetsFileGrp(String use) {
		this.use = use;
	}
	
	public MetsFileGrp(Use usage){
		this.setUsage(usage);
	}

	public MetsFileGrp() {
		
	}
	
	MetsFile addMetsFile(MetsFile metsFile) {
		metsFiles.add(metsFile);
		this.metsFile = metsFile;
		return metsFile;
	}
	
	public String getUse() {
		return use;
	}

	public void setUse(String use) {
		this.use = use;
	}
	
	public Use getUsage() {
		return usage;
	}

	public void setUsage(Use usage) {
		this.usage = usage;
	}


	public String getMetsFileGrpXML() {
		return "<fileGrp USE=\"" + getUse() +  "\">" + getXML() + "</fileGrp>";
	}
	
	public String buildAllFileGrps() {

		String fileGrps="";
		
		for(MetsFileGrp fileGrp : this.fileGrps){
			fileGrps = fileGrps.concat(fileGrp.getMetsFileGrpXML());
		}
		return fileGrps;
	}
	
	public String getXML() {
		String xml = "";
		for(MetsFile metsFile : metsFiles) {
			xml+=metsFile.getMetsFileXML();			
		}		
		return "<fileGrp USE=\"" + usage.getUsage() +  "\">" + xml + "</fileGrp>";
	}
	
}
