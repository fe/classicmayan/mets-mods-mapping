package org.classicmayan.tools;

public class Language {
    private String value;
    private final String attributeName = "xml:lang=";
    
    public Language(String value){
        this.setValue(value);
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
    
    public String getAttributeName() {
        return attributeName;
    }

}
