package org.classicmayan.tools;

import java.text.ParseException;

/**
 * @author Maximilian Brodhun, SUB Göttingen
 * @author Stefan E. Funk, SUB Göttingen
 */

public class RecordCreationDate {

  public String creationDate;
  private String encoding;

  /**
   * @param encoding
   * @param creationDate
   */
  public RecordCreationDate(String encoding, String creationDate) {
    this.setEncoding(encoding);
    this.creationDate = creationDate;
  }

  /**
   * @return
   */
  public String getEncoding() {
    return this.encoding;
  }

  /**
   * @param encoding
   */
  public void setEncoding(String encoding) {
    this.encoding = encoding;
  }

  /**
   * @param creationDate
   */
  public void setCreationDate(String creationDate) {
    this.creationDate = creationDate;
  }

  /**
   * @return
   */
  public String getCreationDate() {
    return this.creationDate;
  }

  /**
   * @return
   * @throws ParseException
   */
  public String getRecordCreationDateXML() throws ParseException {
    return "<recordCreationDate encoding=\"" + this.getEncoding() + "\">" + getCreationDate()
        + "</recordCreationDate>";
  }

  /**
   * @return
   * @throws ParseException
   */
  public String getXML() throws ParseException {
    return "<recordCreationDate encoding=\"" + this.getEncoding() + "\">" + getCreationDate()
        + "</recordCreationDate>";
  }

}
