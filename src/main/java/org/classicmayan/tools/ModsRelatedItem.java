package org.classicmayan.tools;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Maximilian Brodhun, SUB Göttingen
 */

public class ModsRelatedItem {

  private String refToComposedObject;
  private String type;
  private List<ModsRelatedItem> relatedItems = new ArrayList<ModsRelatedItem>();

  public ModsOriginInfo modsOriginInfo;
  public ModsTitleInfo modsTitleInfo;
  public ModsAccessCondition modsAccessCondition;
  public ModsName modsName;

  private List<ModsAccessCondition> modsAccessConditionList = new ArrayList<ModsAccessCondition>();

  /**
   * 
   */
  public ModsRelatedItem() {
    //
  }

  /**
   * @param modsName
   * @return
   */
  public ModsName addModsName(ModsName modsName) {
    this.modsName = modsName;
    return modsName;
  }

  /**
   * @param modsAccessCondition
   * @return
   */
  public ModsAccessCondition addModsAccessCondition(ModsAccessCondition modsAccessCondition) {
    this.modsAccessConditionList.add(modsAccessCondition);
    this.modsAccessCondition = modsAccessCondition;
    return modsAccessCondition;
  }

  /**
   * @param modsOriginInfo
   * @return
   */
  public ModsOriginInfo addOriginInfo(ModsOriginInfo modsOriginInfo) {
    this.modsOriginInfo = modsOriginInfo;
    return modsOriginInfo;
  }

  /**
   * @param modsTitleInfo
   * @return
   */
  public ModsTitleInfo addModsTitleInfo(ModsTitleInfo modsTitleInfo) {
    this.modsTitleInfo = modsTitleInfo;
    return modsTitleInfo;
  }

  /**
   * @param relatedItems
   */
  public ModsRelatedItem(List<ModsRelatedItem> relatedItems) {
    this.relatedItems = relatedItems;
  }

  /**
   * @param refToComposedObject
   * @param type
   */
  public ModsRelatedItem(String refToComposedObject, String type) {
    this.type = type;
    this.refToComposedObject = refToComposedObject;
  }

  /**
   * @return
   */
  public String getRefToComposedObject() {
    return this.refToComposedObject;
  }

  /**
   * @param refToComposedObject
   */
  public void setRefToComposedObject(String refToComposedObject) {
    this.refToComposedObject = refToComposedObject;
  }

  /**
   * @return
   */
  public String getType() {
    return this.type;
  }

  /**
   * @param type
   */
  public void setType(String type) {
    this.type = type;
  }

  /**
   * @return
   */
  public String getModsRelatedItemXML() {

    if (getRefToComposedObject().contains("textgrid")) {

      try {
        return "<relatedItem type=\"" + getType() + "\">"
            + "<recordInfo>"
            + "<recordIdentifier>"
            + getRefToComposedObject().replace("http://textgridrep.de/", "textgrid:")
            + "</recordIdentifier>"
            + "</recordInfo>"
            + "</relatedItem>";
      } catch (NullPointerException n) {

        return "ERROR";
      }
    } else {
      return getModsRelatedItemXMLForImage();
    }
  }

  /**
   * @return
   */
  public String getModsRelatedItemXMLForImage() {
    try {
      return "<relatedItem type=\"" + getType() + "\">"
          + "<recordInfo>"
          + "<recordIdentifier>" + getRefToComposedObject() + "</recordIdentifier>"
          + "</recordInfo>"
          // + this.modsOriginInfo.getModsOriginInfoXML()
          // + this.modsTitleInfo.getModsTitleInfoXML()
          // + this.modsAccessCondition.getXML()
          // + this.modsName.getModsNameXMLList()
          + "</relatedItem>";
    } catch (NullPointerException n) {

      n.printStackTrace();
    }
    return "";
  }

  /**
   * @return
   */
  public String getXML() {

    String xml = "";
    for (ModsAccessCondition modsAccessCondition : this.modsAccessConditionList) {
      xml += modsAccessCondition.getXML();
    }

    return "<relatedItem>" + xml + "</relatedItem>";
  }

  /**
   * @return
   */
  public String buildAllModsRelatedItems() {

    String relatedItems = "";
    for (ModsRelatedItem relatedItem : this.relatedItems) {
      relatedItems = relatedItems.concat(relatedItem.getModsRelatedItemXML());
    }

    return relatedItems;
  }

}
