package org.classicmayan.tools;

import java.text.ParseException;

/**
 * 
 * @author Maximilian Brodhun State- and University Library Goettingen
 * @version 1.0.0
 * @since 23.11.2018
 */

public class MetsRightsMD {

	private String ID;
	MetsMdWrap metsMdWrap;
	
	public MetsRightsMD(String ID) {
		this.ID = ID;
	}
	
	public String getID() {
		return ID;
	}

	public void setID(String iD) {
		ID = iD;
	}
	
	MetsMdWrap addMetsMdWrap(MetsMdWrap metsMdWrap) {
		this.metsMdWrap = metsMdWrap;
		return metsMdWrap;
	}
	
	public String getMetsRightMDXML() throws ParseException {	

		return "<rightsMD ID=\"" + getID() + "\">" + 
					metsMdWrap.getMetsMdWrapXML() + 
				"</rightsMD>";
	}

	public String getXML() throws ParseException {
		
		return "<rightsMD ID=\"" + getID() + "\">" + 
					metsMdWrap.getXML() + 
				"</rightsMD>";
	}
}
