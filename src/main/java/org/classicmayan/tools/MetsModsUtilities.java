package org.classicmayan.tools;

import java.text.ParseException;
import java.text.SimpleDateFormat;

/**
 *
 */
public class MetsModsUtilities {

  /**
   * @param dateToValidate
   * @return
   */
  public static boolean isThisDateValidToOtherTimeStamp(String dateToValidate) {

    SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");

    dateFormat.setLenient(false);
    try {
      dateFormat.parse(dateToValidate.trim());
    } catch (ParseException pe) {
      return false;
    }
    return true;
  }

}
