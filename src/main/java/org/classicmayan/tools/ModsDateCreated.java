package org.classicmayan.tools;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Maximilian Brodhun, SUB Göttingen
 * @version 2022-08-18
 * @since 2018-11-20
 */

public class ModsDateCreated {

  private String dateCreatedValue;
  private String dateCreatedCalendarType;
  private String dateQualifier;
  private String gregorianDateEncoding;
  private String keyDate;
  private Encoding encoding;
  private List<ModsDateCreated> creationDates = new ArrayList<ModsDateCreated>();

  /**
   * @param creationDates
   */
  public ModsDateCreated(List<ModsDateCreated> creationDates) {
    this.creationDates = creationDates;
  }

  /**
   * @param calendarType
   * @param dateValue
   */
  public ModsDateCreated(String calendarType, String dateValue) {
    this.dateCreatedValue = dateValue;
    this.dateCreatedCalendarType = calendarType;
  }

  /**
   * @param calendarType
   * @param dateValue
   * @param qualifier
   */
  public ModsDateCreated(String calendarType, String dateValue, String qualifier) {
    this.dateCreatedValue = dateValue;
    this.dateCreatedCalendarType = calendarType;
    this.dateQualifier = qualifier;
  }

  /**
   * @param encoding
   * @param value
   */
  public ModsDateCreated(Encoding encoding, String value) {
    setEncoding(encoding);
    setDateCreatedValue(value);
  }

  /**
   * @return
   */
  public Encoding getEncoding() {
    return this.encoding;
  }

  /**
   * @param encoding
   */
  public void setEncoding(Encoding encoding) {
    this.encoding = encoding;
  }

  /**
   * 
   */
  public ModsDateCreated() {
    // TODO ??
  }

  /**
   * @return
   */
  public String getDateCreatedValue() {
    return this.dateCreatedValue;
  }

  /**
   * @param dateCreatedValue
   */
  public void setDateCreatedValue(String dateCreatedValue) {
    this.dateCreatedValue = dateCreatedValue;
  }

  /**
   * @return
   */
  public String getDateCreatedCalendarType() {
    return this.dateCreatedCalendarType;
  }

  /**
   * @param dateCreatedCalendarType
   */
  public void setDateCreatedCalendarType(String dateCreatedCalendarType) {
    this.dateCreatedCalendarType = dateCreatedCalendarType;
  }

  /**
   * @return
   */
  public String getDateQualifier() {
    return this.dateQualifier;
  }

  /**
   * @param dateQualifier
   */
  public void setDateQualifier(String dateQualifier) {
    this.dateQualifier = dateQualifier;
  }

  /**
   * @return
   */
  public String getDateCreatedXML() {

    if (getKeyDate() != null) {
      // System.out.println("BLA");
      if (getDateCreatedValue().startsWith("undated")) {
        return "<dateCreated keyDate=\"" + getKeyDate() + "\" encoding=\"iso8601\">"
            + getDateCreatedValue().substring(0, getDateCreatedValue().indexOf("-")) +
            "</dateCreated>";
      } else {
        return "<dateCreated keyDate=\"" + getKeyDate() + "\" encoding=\"iso8601\">"
            + encodeDateInISO8601(getDateCreatedValue()) +
            "</dateCreated>";
      }
    }

    /**
     * Dates in gregorian format everytime gets the additional dataset in iso-8601 encoded date.
     * Longcount and calendarround not
     */

    /**
     * If the date qualifier is equals to "Uncertain", "Approximately", "Before" or "After" and the
     * calendarType is gregorian the creation date gets the qualifier information and the additional
     * encoding data set
     */

    if (getDateQualifier() != null && (getDateQualifier().equals("Uncertain") ||
        getDateQualifier().equals("Approximately") ||
        getDateQualifier().equals("Before") ||
        getDateQualifier().equals("After"))) {

      return "<dateCreated calendar=\"" + getDateCreatedCalendarType()
          + "\" qualifier=\"questionable\">"
          + getDateCreatedValue()
          + "</dateCreated>"
          + "<dateCreated encoding=\"iso8601\" qualifier=\""
          + "questionable" + "\">" + encodeDateInISO8601(getDateCreatedValue())
          + "</dateCreated>";
    }

    /**
     * If the date qualifier is equals to "Exactly" and the calendarType is gregorian the creation
     * date gets no point or qualifier information but the additional encoding data set
     */

    if (getDateQualifier() != null && getDateQualifier().equals("Exactly")) {
      return "<dateCreated calendar=\"" + getDateCreatedCalendarType() + "\">"
          + getDateCreatedValue()
          + "</dateCreated>"
          + "<dateCreated encoding=\"iso8601\">" + encodeDateInISO8601(getDateCreatedValue())
          + "</dateCreated>";

    }

    /**
     * If the date qualifier is equals to "Begin" or "End" and the calendarType is gregorian the
     * creation date gets the point information and the additional encoding data set
     */

    if (getDateQualifier() != null
        && (getDateQualifier().equals("begin") || getDateQualifier().equals("end"))) {
      // String isoEncodedDate = getDateCreatedValue().substring(0,
      // getDateCreatedValue().indexOf("-"));
      return "<dateCreated calendar=\"" + getDateCreatedCalendarType()
          + "point=\"" + this.dateQualifier + "\">"
          + getDateCreatedValue()
          + "</dateCreated>"
          + "<dateCreated encoding=\"iso8601\" "
          + "point=\"" + this.dateQualifier + "\">"
          + encodeDateInISO8601(getDateCreatedValue()) + "</dateCreated>";
    }

    /**
     * In the cases for the calendar type longcount and calendar round the date dataset just
     * consists of the information about calendar type and the date value
     */
    else {
      return "<dateCreated calendar=\"" + getDateCreatedCalendarType() + "\">"
          + getDateCreatedValue()
          + "</dateCreated>";
    }
  }

  /**
   * @return
   */
  public String getGregorianDateEncoding() {
    return this.gregorianDateEncoding;
  }

  /**
   * @param gregorianDateEncoding
   */
  public void setGregorianDateEncoding(String gregorianDateEncoding) {
    this.gregorianDateEncoding = gregorianDateEncoding;
  }

  /*
   * public String buildAllCreationDates() { String creationDates="";
   * 
   * for(ModsDateCreated creationDate : this.creationDates){ creationDates =
   * creationDates.concat(creationDate.getDateCreatedXML()); } return creationDates; }
   */

  /**
   * @param isoEncodedDate
   * @return
   */
  public String encodeDateInISO8601(String isoEncodedDate) {

    // TODO: Just if Date is given
    isoEncodedDate = getDateCreatedValue().substring(0, getDateCreatedValue().indexOf("-"));

    if (Integer.parseInt(isoEncodedDate) < 1000) {
      // isoEncodedDate = "0" + isoEncodedDate;
      if (Integer.parseInt(isoEncodedDate) < 0) {
        isoEncodedDate = "-" + isoEncodedDate;
      }
    }

    return isoEncodedDate;
  }

  /**
   * @return
   */
  public String getKeyDate() {
    return this.keyDate;
  }

  /**
   * @param keyDate
   */
  public void setKeyDate(String keyDate) {
    this.keyDate = keyDate;
  }

  /**
   * @return
   */
  public String getXML() {
    if (!getDateCreatedValue().getClass().getName().equals("java.lang.String")) {
      return "<dateCreated encoding=\"" + this.encoding.getValue() + "\">" + getDateCreatedValue()
          + "</dateCreated>";
    } else {
      return "<dateCreated>" + getDateCreatedValue() + "</dateCreated>";
    }
  }

}
