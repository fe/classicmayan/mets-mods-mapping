package org.classicmayan.tools;

/**
 * 
 * @author Maximilian Brodhun State- and University Library Goettingen
 * @version 1.0.0
 * @since 23.11.2018
 */

public class MetsRightsSponsor {

	private String sponsorValue;

	public MetsRightsSponsor(){

	}

	public MetsRightsSponsor(String value){
		this.setSponsorValue(value);
	}

	public String getSponsorValue() {
		return sponsorValue;
	}

	public void setSponsorValue(String sponsorValue) {
		this.sponsorValue = sponsorValue;
	}
	
	public String getMetsRightsSponsorXML() {
		return "<sponsor>" + getSponsorValue() + "</sponsor>";
	}
}
