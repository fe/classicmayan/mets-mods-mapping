package org.classicmayan.tools;

/**
 * 
 * @author Maximilian Brodhun State- and University Library Goettingen
 * @version 1.0.0
 * @since 22.11.2018
 */

public class MetsOwner {
	
	private String ownerValue;

	public MetsOwner(){

	}

	public MetsOwner(String value){
		this.setOwnerValue(value);
	}

	public String getOwnerValue() {
		return ownerValue;
	}

	public void setOwnerValue(String ownerValue) {
		this.ownerValue = ownerValue;
	}
	
	public String getMetsOwnerXML() {
		return "<owner>" + getOwnerValue() + "</owner>";
	}
	
}
