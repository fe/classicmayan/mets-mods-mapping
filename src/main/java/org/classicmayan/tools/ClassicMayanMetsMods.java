package org.classicmayan.tools;

import java.io.IOException;
import java.text.ParseException;
import java.time.Instant;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * @author Maximilian Brodhun, SUB Göttingen
 * @author Stefan E. Funk, SUB Göttingen
 */
public class ClassicMayanMetsMods {

  // **
  // STATICS
  // **

  protected static Logger log = Logger.getLogger(ClassicMayanMetsMods.class.getName());

  // **
  // FINALS
  // **

  private static final String HEIGHT_DESC = "height";
  private static final String WIDTH_DESC = "width";
  private static final String THICKNESS_DESC = "thickness";
  private static final String HEIGHTALC_DESC = "height above lowest carving";
  private static final String HEIGHTOSA_DESC = "height of sculptured area";
  private static final String HEIGHTOPB_DESC = "height of plain butt";
  private static final String EXPOSURE_DESC = "exposure above floor of plain butt";
  private static final String WIDTHABOC_DESC = "width at base of carving";
  private static final String WIDTHOSA_DESC = "width of sculptured area";
  private static final String RELIEF_DESC = "relief depth";
  private static final String DIAMETER_DESC = "diameter";
  private static final String PERIMETER_DESC = "perimeter";
  private static final String WEIGHT_DESC = "weight";
  private static final String UTC_FORMAT_STRING = "yyyy-MM-dd'T'HH:mm:ss'Z'";

  protected static final String ISO_8601 = "iso8601";
  protected static final DateTimeFormatter UTC_FORMATTER =
      DateTimeFormatter.ofPattern(UTC_FORMAT_STRING).withZone(ZoneId.of("UTC"));

  // **
  // CLASS
  // **

  String textgridURI;
  String creationDate;
  String changeDate;

  Mets mets = new Mets();
  Mods mods = new Mods();

  ModsTitleInfo modsTitleInfo = new ModsTitleInfo();
  ModsLocation modsLocation = new ModsLocation();
  ModsOriginInfo modsOriginInfo = new ModsOriginInfo();
  ModsGenre modsGenre = new ModsGenre();
  ModsPhysicalDescription modsPhysicalDescription = new ModsPhysicalDescription();
  ModsRelatedItem relatedItem = new ModsRelatedItem();
  ModsRecordInfo modsRecordInfo;
  ModsTypeOfResource modsTypeOfResource = new ModsTypeOfResource();
  List<String> linkedMedia = new ArrayList<String>();
  private String idiomNumber;
  private String artefactTitle;
  MetsAmdSec metsAmdSecAll = new MetsAmdSec();
  MetsDmdSec metsDmdSec = new MetsDmdSec();

  /**
   * @param textGridURI
   * @param creationDate
   * @param changeDate
   */
  public ClassicMayanMetsMods(String textGridURI, String creationDate, String changeDate) {

    this.textgridURI = textGridURI.replace("https://textgridrep.de/", "");
    this.creationDate = creationDate;
    this.changeDate = changeDate;

    // Get linked media from KonedaCOR.
    this.linkedMedia = TripleStoreQuery.getLinkedMediaInKorFromArtefact(textGridURI);

    log.fine("linked media list: " + this.linkedMedia.toString());

    // Get rest of data from classicmayan triple store.
    this.setIdiomNumber(Queries.getIdiomNumber(textGridURI).replace("idiom:", ""));

    String title = Queries.getArtefactTitle(textGridURI);
    this.setArtefactTitle(title);

    log.fine("artefact title: " + title);

    this.setTitleInfoData();
    this.setTypeOfResource();
    this.setRecordInfo();
    this.setLocationData();
    this.setArtefactCreationDate();
    this.setArtefactGenre();
    this.setPhysicalDescription();
    this.setRelatedItems();
    // this.setModsTranscriptionAndTranslation();
    this.setAccessCondition();
  }

  /**
   * @return
   */
  public MetsDmdSec buildDmdSec() {
    return new MetsDmdSec();
  }

  /**
   * @param type
   * @return
   */
  public MetsStructMap buildStructMap(String type) {

    MetsStructMap structMap = new MetsStructMap(type);

    // Build METS XML IDs from TextGrid URI instead of IDIOM numbers, we do not always have IDIOM
    // numbers!!
    structMap.addMetsDiv(new MetsDiv(
        "log_" + getTextGridURIPartForXMLID(this.textgridURI),
        "dmd_" + getTextGridURIPartForXMLID(this.textgridURI) + "_d1",
        "amd_" + getTextGridURIPartForXMLID(this.textgridURI),
        "document",
        getArtefactTitle()));

    return structMap;
  }

  /**
   * 
   */
  public void setRecordInfo() {

    RecordCreationDate recordCreationDate = new RecordCreationDate(ISO_8601, this.creationDate);
    RecordChangeDate recordChangeDate = new RecordChangeDate(ISO_8601, this.changeDate);
    RecordIdentifier recordIdentifier = new RecordIdentifier(this.textgridURI);

    this.modsRecordInfo =
        new ModsRecordInfo(recordIdentifier, recordCreationDate, recordChangeDate);
    this.mods.addRecordInfoToMods(this.modsRecordInfo);
  }

  /**
   * @return
   * @throws ParseException
   */
  public String getRecordInfo() throws ParseException {
    return this.modsRecordInfo.getRecordInfoXML();
  }

  /**
   * 
   */
  public void setTypeOfResource() {
    this.modsTypeOfResource.setTypeOfResourceValue("three dimensional object");
    this.mods.addModsTypeOfResource(this.modsTypeOfResource);
  }

  /**
   * @return
   */
  public String getTypeOfResource() {
    return this.modsTypeOfResource.getTypeOfReosourceXML();
  }

  /**
   * 
   */
  public void setTitleInfoData() {
    this.modsTitleInfo.setModsTitleInfoLanguageValue("en");
    this.modsTitleInfo.setModsTitleInfoValue(getArtefactTitle());
    this.mods.addModsTitleInfo(this.modsTitleInfo);
  }

  /**
   * @return
   */
  public String getTitleInfoData() {
    return this.modsTitleInfo.getModsTitleInfoXML();
  }

  /**
   * 
   */
  public void setLocationData() {

    LocationDataObject locationDataObject = Queries.getLocationData(this.textgridURI);
    List<ModsPhysicalLocation> physicalLocationList = new ArrayList<ModsPhysicalLocation>();
    ModsPhysicalLocation allLocations = new ModsPhysicalLocation(physicalLocationList);

    // Physical Location Objects for the possible location in IDIOM object.
    try {
      physicalLocationList.add(new ModsPhysicalLocation("provenance", "Provenance",
          locationDataObject.getDiscoveryPlace()));
      if (locationDataObject.getCurrentCustody() != null) {
        physicalLocationList
            .add(new ModsPhysicalLocation("current", locationDataObject.getCurrentCustody()));
      }
      if (locationDataObject.isInSitu()) {
        physicalLocationList.add(new ModsPhysicalLocation("current", "in situ"));
      }
    } catch (NullPointerException n) {
      // TODO Why are we catching an NPE here??
      log.warning(
          "NPE in ClassicMayanMetsMods.setLocationData()! System message: " + n.getMessage());
    }

    this.modsLocation.addModsPhysicalLocation(allLocations);
    this.mods.addModsLocation(this.modsLocation);
  }

  /**
   * @return
   */
  public String getLocationData() {
    return this.modsLocation.getModsLocationXML();
  }

  /**
   * 
   */
  public void setArtefactCreationDate() {

    ArtefactCreationDateData artefactCreationDateData =
        Queries.getPhysicalCreationDateData(this.textgridURI);

    this.modsOriginInfo
        .addDateCreated(new ModsDateCreated("Longcount", artefactCreationDateData.getLongcount()));
    this.modsOriginInfo.addDateCreated(
        new ModsDateCreated("Calendar Round", artefactCreationDateData.getCalendarRound()));
    this.modsOriginInfo.addDateCreated(new ModsDateCreated("Gregorian",
        artefactCreationDateData.getGregorianDate(), artefactCreationDateData.getQualifier()));
    this.mods.addModsOriginInfo(this.modsOriginInfo);
  }

  /**
   * 
   */
  public void setArtefactGenre() {

    GenreDataObject genreDataObject = Queries.getGenreDataObject(this.textgridURI);
    if (genreDataObject != null) {
      this.modsGenre.setGenreLanguage(genreDataObject.getLanguage());
      this.modsGenre.setGenreValueURI(genreDataObject.getValueURI());
      this.modsGenre.setGenreValue(genreDataObject.getGenreValue());

      this.mods.addModsGenre(this.modsGenre);
    }
  }

  /**
   * @return
   */
  public String getArtefactGenre() {
    return this.modsGenre.getModsGenreXML();
  }

  /**
   * 
   */
  public void setPhysicalDescription() {

    PhysicalDescriptionDataObject physicalDescriptionDataObject =
        Queries.getPhysicalDescriptionDataObject(this.textgridURI);
    List<Form> forms = new ArrayList<Form>();
    Form allForms = new Form(forms);
    List<Extent> extents = new ArrayList<Extent>();
    Extent allExtents = new Extent(extents);

    for (FormDataObject fdo : physicalDescriptionDataObject.formListMaterialAssignments) {
      forms.add(new Form(fdo.getLang(), fdo.getType(), fdo.getValueURI(), fdo.getFormValue()));
    }
    for (FormDataObject fdo : physicalDescriptionDataObject.formListTechniqueAssignments) {
      forms.add(new Form(fdo.getLang(), fdo.getType(), fdo.getValueURI(), fdo.getFormValue()));
    }
    for (ExtentDataObject edo : physicalDescriptionDataObject.extentListDimensions) {

      // Get unit first here, we need it with all the others.
      String unit = "";
      if (edo.getUnit() != null && edo.getUnit().length() > 0) {
        unit = edo.getUnit();
      }
      String beginDesc = " " + unit + " (";
      String endDesc = "), ";
      // Get all the others (using unit).
      String height = "";
      if (edo.getHeight().length() > 0) {
        height = edo.getHeight() + beginDesc + HEIGHT_DESC + endDesc;
      }
      String width = "";
      if (edo.getWidth().length() > 0) {
        width = edo.getWidth() + beginDesc + WIDTH_DESC + endDesc;
      }
      String thickness = "";
      if (edo.getThickness().length() > 0) {
        thickness = edo.getThickness() + beginDesc + THICKNESS_DESC + endDesc;
      }
      String heightLowCarving = "";
      if (edo.getHeightLowCarving().length() > 0) {
        heightLowCarving = edo.getHeightLowCarving() + beginDesc + HEIGHTALC_DESC + endDesc;
      }
      String heightSculptuedArea = "";
      if (edo.getHeightSculptuedArea().length() > 0) {
        heightSculptuedArea = edo.getHeightSculptuedArea() + beginDesc + HEIGHTOSA_DESC + endDesc;
      }
      String heightButt = "";
      if (edo.getHeightButt().length() > 0) {
        heightButt = edo.getHeightButt() + beginDesc + HEIGHTOPB_DESC + endDesc;
      }
      String exposureAboveFloor = "";
      if (edo.getExposureAboveFloor().length() > 0) {
        exposureAboveFloor = edo.getExposureAboveFloor() + beginDesc + EXPOSURE_DESC + endDesc;
      }
      String widthBaseCarving = "";
      if (edo.getWidthBaseCarving().length() > 0) {
        widthBaseCarving = edo.getWidthBaseCarving() + beginDesc + WIDTHABOC_DESC + endDesc;
      }
      String widthSculpturedArea = "";
      if (edo.getWidthSculpturedArea().length() > 0) {
        widthSculpturedArea = edo.getWidthSculpturedArea() + beginDesc + WIDTHOSA_DESC + endDesc;
      }
      String depthRelief = "";
      if (edo.getDepthRelief().length() > 0) {
        depthRelief = edo.getDepthRelief() + beginDesc + RELIEF_DESC + endDesc;
      }
      String diameter = "";
      if (edo.getDiameter().length() > 0) {
        diameter = edo.getDiameter() + beginDesc + DIAMETER_DESC + endDesc;
      }
      String perimeter = "";
      if (edo.getPerimeter().length() > 0) {
        perimeter = edo.getPerimeter() + beginDesc + PERIMETER_DESC + endDesc;
      }
      String weight = "";
      if (edo.getWeight().length() > 0) {
        weight = edo.getWeight() + beginDesc + WEIGHT_DESC + endDesc;
      }

      String extendsToAdd = cutExtend(height + width + thickness + heightLowCarving
          + heightSculptuedArea + heightButt + exposureAboveFloor + widthBaseCarving
          + widthSculpturedArea + depthRelief + diameter + perimeter + weight);

      log.fine("extent added: " + extendsToAdd);

      extents.add(new Extent(extendsToAdd));
    }

    this.modsPhysicalDescription.addForm(allForms);
    this.modsPhysicalDescription.addExtent(allExtents);

    this.mods.addModsPhysicalDescription(this.modsPhysicalDescription);
  }

  /**
   * @return
   */
  public String getPhysicalDescription() {
    return this.modsPhysicalDescription.getModsPhysicalDescriptionXML();
  }

  /**
   * 
   */
  public void setRelatedItems() {

    try {
      List<ModsRelatedItem> relatedItems = new ArrayList<ModsRelatedItem>();
      ModsRelatedItem allRealtedItems = new ModsRelatedItem(relatedItems);

      for (String artefactPartURI : Queries.getRelatedItems(this.textgridURI)) {
        relatedItems.add(new ModsRelatedItem(artefactPartURI, "constituent"));
      }
      for (ModsRelatedItem imageItem : getImagesAsRelatedItem()) {
        relatedItems.add(imageItem);
      }
      this.mods.addModsRelatedItem(allRealtedItems);
    } catch (IOException e) {
      log.warning(e.getClass().getName() + "!" + e.getMessage());
    }
  }

  /**
   * @return
   */
  // TODO: check if needed
  public String getRelatedItems() {
    return this.relatedItem.getModsRelatedItemXML();
  }

  /**
   * 
   */
  // TODO get Transcription and translation from ALMAH
  public void setModsTranscriptionAndTranslation() {

    List<ModsNote> notes = new ArrayList<ModsNote>();
    ModsNote allNotes = new ModsNote(notes);

    notes.add(new ModsNote("transcription",
        "waxak cauac lajcha' yax k' in chumwaan ta ajawlel ik' muy muwaan yeht k'aba'il u mam"));
    notes.add(new ModsNote("translation", "I am Spearthrower Owl"));

    this.mods.addModsNote(allNotes);
  }

  /**
   * 
   */
  public void setAccessCondition() {

    ModsAccessCondition modsAccessCondition = new ModsAccessCondition(
        "use and reproduction",
        "https://creativecommons.org/licenses/by/4.0",
        "CC BY 4.0");
    modsAccessCondition.setAccessConditionValue("cc-by-4.0");

    this.mods.addModsAccessCondition(modsAccessCondition);
  }

  /**
   * @return
   * @throws JSONException
   * @throws IOException
   */
  public List<ModsRelatedItem> getImagesAsRelatedItem() throws JSONException, IOException {

    log.info("  ##  start getImagesAsRelatedItem()");

    List<ModsRelatedItem> modsRelatedItemForImagesList = new ArrayList<ModsRelatedItem>();

    for (String linkedMedium : this.linkedMedia) {

      log.info("  ##  linkedMedium: " + linkedMedium);

      JSONObject imageDataFromConedaKor = new JSONObject();
      try {
        imageDataFromConedaKor = ConedaKorQueries.getMediumMetadaSetForMetsMods(linkedMedium);

        ModsRelatedItem imageRelatedItem = new ModsRelatedItem();
        // Set the metadata for RecordIdentifier
        imageRelatedItem.setType("constituent");
        imageRelatedItem.setRefToComposedObject(
            imageDataFromConedaKor.get("<recordIdentifier(returnedEntity)>").toString());

        log.info("  ##  set image ref to: " + imageRelatedItem.getRefToComposedObject());

        modsRelatedItemForImagesList.add(imageRelatedItem);

      } catch (JSONException mediaNotFound) {

        ModsRelatedItem imageRelatedItem = new ModsRelatedItem();
        // Set the metadata for RecordIdentifier
        imageRelatedItem.setType("constituent");
        imageRelatedItem.setRefToComposedObject(linkedMedium);

        log.info("  ##  !mediaNotFound! set image ref to: " + linkedMedium);

        modsRelatedItemForImagesList.add(imageRelatedItem);
      }
    }

    return modsRelatedItemForImagesList;
  }

  /**
   * @return
   * @throws ParseException
   */
  public String getMods() throws ParseException {
    return this.mods.getModsXML();
  }

  /**
   * @return
   * @throws ParseException
   * @throws JSONException
   * @throws IOException
   */
  public String getMets() throws ParseException, JSONException, IOException {

    MetsHdr metsHdr = new MetsHdr();
    // METS creation date is NOW!
    String metsCreationDate = UTC_FORMATTER.format(Instant.now());

    log.fine("building mets with CREATEDATE " + metsCreationDate);

    metsHdr.setMetsHdrCreationDate(metsCreationDate);
    this.mets.addMetsHeader(metsHdr);

    // TODO: File ID with Constructor
    MetsDmdSec metsDmdSec = new MetsDmdSec();
    metsDmdSec.setId("dmd_" + getTextGridURIPartForXMLID(this.textgridURI) + "_d1");
    this.mets.addMetsDmdSec(metsDmdSec);

    MetsMdWrap metsMdWrap = new MetsMdWrap();
    metsMdWrap.setMdtype("MODS");
    metsMdWrap.setMimeType("text/xml");
    metsDmdSec.addMdWrap(metsMdWrap);

    MetsXmlData metsXmlData = new MetsXmlData();
    metsMdWrap.addXmlData(metsXmlData);

    metsXmlData.addMods(this.mods);

    this.mets.addMetsStructMap(buildStructMap("LOGICAL"));

    // Set all necessary nodes for the MetsAmd Section.
    MetsAmdSec metsAmdSec = new MetsAmdSec("amd_" + getTextGridURIPartForXMLID(this.textgridURI));

    // Set all necessary nodes for the rights Section.
    MetsRightsMD metsRightsMD =
        new MetsRightsMD("rights_" + getTextGridURIPartForXMLID(this.textgridURI) + "_d1");
    metsAmdSec.addMetsRightsMD(metsRightsMD);

    MetsMdWrap metsMdWrapForMetsRightsMD = new MetsMdWrap(
        new MimeType("text/xml"),
        new MdType("MODS"),
        new OtherMdType("DIVRIGHTS"));
    MetsXmlData metsXmlDataForMetsRightsMD = new MetsXmlData();
    MetsRights metsRights = new MetsRights();
    MetsOwner metsOwner = new MetsOwner();
    MetsOwnerSiteURL metsOwnerSiteURL = new MetsOwnerSiteURL();
    MetsRightsSponsor metsRightsSponsor = new MetsRightsSponsor();

    metsRightsMD.addMetsMdWrap(metsMdWrapForMetsRightsMD);

    metsOwner.setOwnerValue("Text Database and Dictionary of Classic Mayan");
    metsRights.addMetsOwner(metsOwner);

    metsOwnerSiteURL.setMetsOwnerSiteURLValue("https://www.mayadictionary.de");
    metsRights.addMetsOwnerSiteURL(metsOwnerSiteURL);
    metsRightsSponsor.setSponsorValue(
        "Nordrhein-Westfälische Akademie der Wissenschaften und der Künste, Union der Wissenschaftsakademien");

    metsRights.addMetsRightsSponsor(metsRightsSponsor);
    metsXmlDataForMetsRightsMD.addMetsRights(metsRights);
    metsMdWrapForMetsRightsMD.addXmlData(metsXmlDataForMetsRightsMD);
    this.metsAmdSecAll.addMetsAmdSec(metsAmdSec);
    // Adding the amdSecs for the images by its self
    // setMetsAmdSecForImages();
    this.mets.addMetsAmdSec(metsAmdSec);

    /**
     * Set all necessary nodes for Digiprov Section
     */

    MetsDigiProvMD metsDigiProvMD =
        new MetsDigiProvMD("digiprov_" + getTextGridURIPartForXMLID(this.textgridURI));

    MetsMdWrap metsMdWrapForDigiProv = new MetsMdWrap(
        new MimeType("text/xml"),
        new MdType("MODS"),
        new OtherMdType("DIVLINKS"));

    metsDigiProvMD.addMetsMdWrap(metsMdWrapForDigiProv);

    MetsXmlData metsXmlDataForoDigiProv = new MetsXmlData();
    MetsLinks metsLinks = new MetsLinks();
    metsXmlDataForoDigiProv.addMetsLinks(metsLinks);
    metsMdWrapForDigiProv.addXmlData(metsXmlDataForoDigiProv);

    metsLinks.addMetsReference(new MetsReference("Artefact in TextGrid Repository",
        "https://textgridrep.org/" + this.textgridURI));
    metsLinks.addMetsReference(new MetsReference("Inscription in TextGrid Repository",
        "https://textgridrep.org/" + this.textgridURI));
    metsLinks.addMetsReference(new MetsReference("Linguistic Annotation in TextGrid Repository",
        "https://textgridrep.org/" + this.textgridURI));
    metsAmdSec.addMetsDigiProvMD(metsDigiProvMD);

    return this.mets.getMetsXML();
  }

  // **
  // PRIVATE
  // **

  /**
   * @param theExtend
   * @return
   */
  protected static String cutExtend(final String theExtend) {

    String result;

    if (theExtend.endsWith(", ")) {
      result = theExtend.substring(0, theExtend.length() - 2);
    } else {
      result = theExtend;
    }

    return result;
  }

  /**
   * @param textgridURI
   * @return
   */
  private static String getTextGridURIPartForXMLID(String textgridURI) {
    return textgridURI.replace("textgrid:", "");
  }

  // **
  // GETTERS & SETTERS
  // **

  /**
   * @return
   */
  public String getTextGridUri() {
    return this.textgridURI;
  }

  /**
   * @return
   */
  public String getIdiomNumber() {
    return this.idiomNumber;
  }

  /**
   * @param idiomNumber
   */
  public void setIdiomNumber(String idiomNumber) {
    this.idiomNumber = idiomNumber;
  }

  /**
   * @return
   */
  public String getArtefactTitle() {
    return this.artefactTitle;
  }

  /**
   * @param artefactTitle
   */
  public void setArtefactTitle(String artefactTitle) {
    this.artefactTitle = artefactTitle;
  }

}
