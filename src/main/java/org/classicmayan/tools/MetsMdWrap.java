package org.classicmayan.tools;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;



/**
 * 
 * @author Maximilian Brodhun State- and University Library Goettingen
 * @version 1.0.0
 * @since 23.11.2018
 */

public class MetsMdWrap {
	

	/**TODO: remove String and also getter and setter for this */
	private String mimeType;
	private String mdType;
	private String otherMdType;
	
	private MdType metadataType;
	private MimeType metsMimeType;
	private OtherMdType metsOtherMdType;

	public MetsXmlData metsXmlData;	
	List<MetsXmlData> metsXmlDataList = new ArrayList<MetsXmlData>();
	private String xmlAttributes;

	public MetsMdWrap(){

	}

	public OtherMdType getMetsOtherMdType() {
		return metsOtherMdType;
	}

	public void setMetsOtherMdType(OtherMdType metsOtherMdType) {
		this.metsOtherMdType = metsOtherMdType;
	}

	public MetsMdWrap(MimeType mimeType, MdType mdType){
		this.setMetsMimeType(mimeType);
		this.setMetadataType(mdType);		
		xmlAttributes = "MIMETYPE=\"" + mimeType.getMimeType() +  "\" MDTYPE=\"" + mdType.getMdType() + "\"";
	}

	public MetsMdWrap(MimeType mimeType, MdType mdType, OtherMdType otherMdType){
		this.setMetsMimeType(mimeType);
		this.setMetadataType(mdType);
		this.setMetsOtherMdType(otherMdType);
		xmlAttributes = "MIMETYPE=\"" + mimeType.getMimeType() +  "\" MDTYPE=\"" + mdType.getMdType() + "\"" + " OTHERMDTYPE=\"" + otherMdType.getOtherMdType() + "\"";
	}

	public MimeType getMetsmimeType() {
		return metsMimeType;
	}

	public void setMetsMimeType(MimeType metsmimeType) {
		this.metsMimeType = metsmimeType;
	}

	public MdType getMetadataType() {
		return metadataType;
	}

	public void setMetadataType(MdType metadataType) {
		this.metadataType = metadataType;
	}


	public MetsXmlData addXmlData(MetsXmlData metsXmlData) {
		this.metsXmlDataList.add(metsXmlData);
		this.metsXmlData = metsXmlData;
		return metsXmlData;
	}
	
	public String getMimeType() {
		return mimeType;
	}
	public void setMimeType(String mimeType) {
		this.mimeType = mimeType;
	}
	public String getMdType() {
		return mdType;
	}
	public void setMdtype(String mdType) {
		this.mdType = mdType;
	}

	public String getOtherMdType() {
		return otherMdType;
	}
	
	public void setOtherMdType(String otherMdType) {
		this.otherMdType = otherMdType;
	}
	
	public String getMetsMdWrapXML() throws ParseException {
		
		if(otherMdType == null) {
			return "<mdWrap MIMETYPE=\"" + getMimeType() +  "\" MDTYPE=\"" + getMdType() + "\">" + metsXmlData.getXmlDataXML() + "</mdWrap>";
		}else {
			return "<mdWrap MIMETYPE=\"" + getMimeType() +  "\" MDTYPE=\"" + getMdType() + "\" OTHERMDTYPE=\"" + getOtherMdType() + "\">" + metsXmlData.getXmlDataXML() + "</mdWrap>";
		}
	}
	
	public String getXML() throws ParseException{

		String xml = "<mdWrap " + xmlAttributes + ">";
		for(MetsXmlData xmlData : metsXmlDataList){
			xml += xmlData.getXML();
		}

		return xml + "</mdWrap>";
	}
}