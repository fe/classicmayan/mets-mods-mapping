package org.classicmayan.tools;

public class PublicationStatus {

    private String status;

    public PublicationStatus(String status){
        setStatus(status);
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }



}
