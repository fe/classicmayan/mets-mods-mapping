package org.classicmayan.tools;

public class Use {
    private String usage;

    public Use(String usage){
        this.setUsage(usage);
    }

    public String getUsage() {
        return usage;
    }

    public void setUsage(String usage) {
        this.usage = usage;
    }
}
