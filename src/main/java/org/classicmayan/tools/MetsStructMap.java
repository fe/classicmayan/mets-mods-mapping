package org.classicmayan.tools;

import java.util.ArrayList;
import java.util.List;

/**
 * 
 * @author Maximilian Brodhun State- and University Library Goettingen
 * @version 1.0.0
 * @since 23.11.2018
 */

public class MetsStructMap {
	
	private String xml;
	private String type;
	MetsDiv metsDiv;

	private List<MetsDiv> metsDivs = new ArrayList<MetsDiv>();
	
	public MetsStructMap(String type) {
		this.type = type;
		xml = "<structMap TYPE=\""+ getType() +  "\">";
		
	}
	
	public MetsStructMap(){
		metsDivs.add(new MetsDiv());
		xml = "<structMap>";		
	}
	
	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	MetsDiv addMetsDiv(MetsDiv metsDiv) {
		metsDivs.add(metsDiv);
		this.metsDiv = metsDiv;
		return metsDiv;
	}
	
	public String getXML() {
		String xml2="";
		for(MetsDiv metsDiv : metsDivs) {	
			xml2 += metsDiv.getSingleXML();			
		}
		return this.xml + xml2 + "</structMap>";
	}
}
