package org.classicmayan.tools;

public class ValueURI {
    private String value;
    private final String attributeName = "valueURI=";

    public ValueURI(String value){
        this.setValue(value);
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getAttributeName() {
        return attributeName;
    }
}
