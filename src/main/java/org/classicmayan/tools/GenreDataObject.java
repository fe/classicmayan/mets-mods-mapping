package org.classicmayan.tools;

public class GenreDataObject {
	
	private String language;
	private String valueURI;
	private String genreValue;
	
	public String getLanguage() {
		return language;
	}
	public void setLanguage(String language) {
		this.language = language;
	}
	public String getValueURI() {
		return valueURI;
	}
	public void setValueURI(String valueURI) {
		this.valueURI = valueURI;
	}
	public String getGenreValue() {
		return genreValue;
	}
	public void setGenreValue(String genreValue) {
		this.genreValue = genreValue;
	}

}
