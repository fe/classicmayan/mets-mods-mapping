package org.classicmayan.tools;

public class DisplayLabel {
    
    private String displayLabel;

    public DisplayLabel(String displayLabel){
        this.setDisplayLabel(displayLabel);
    }

    public String getDisplayLabel() {
        return displayLabel;
    }

    public void setDisplayLabel(String displayLabel) {
        this.displayLabel = displayLabel;
    }


}
