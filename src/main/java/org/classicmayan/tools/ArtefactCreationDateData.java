package org.classicmayan.tools;

public class ArtefactCreationDateData {

	private String longcount;
	private String calendarRound;
	private String gregorianDate;
	private String qualifier;
	private String point;
	private String keyDate;
	
	/**
	 * @return the longcount
	 */
	public String getLongcount() {
		return longcount;
	}
	/**
	 * @param longcount the longcount to set
	 */
	public void setLongcount(String longcount) {
		this.longcount = longcount;
	}
	/**
	 * @return the calendarRound
	 */
	public String getCalendarRound() {
		return calendarRound;
	}
	/**
	 * @param calendarRound the calendarRound to set
	 */
	public void setCalendarRound(String calendarRound) {
		this.calendarRound = calendarRound;
	}
	/**
	 * @return the gregorianDate
	 */
	public String getGregorianDate() {
		return gregorianDate;
	}
	/**
	 * @param gregorianDate the gregorianDate to set
	 */
	public void setGregorianDate(String gregorianDate) {
		this.gregorianDate = gregorianDate;
	}
	/**
	 * @return the qualifier
	 */
	public String getQualifier() {
		return qualifier;
	}
	/**
	 * @param qualifier the qualifier to set
	 */
	public void setQualifier(String qualifier) {
		this.qualifier = qualifier;
	}
	
	public String getPoint() {
		return point;
	}
	public void setPoint(String point) {
		this.point = point;
	}
	public String getKeyDate() {
		return keyDate;
	}
	public void setKeyDate(String keyDate) {
		this.keyDate = keyDate;
	}
}
