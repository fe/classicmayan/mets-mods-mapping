package org.classicmayan.tools;

/**
 * 
 * @author Maximilian Brodhun State- and University Library Goettingen
 * @version 1.0.0
 * @since 19.11.2018
 */

public class ModsLocation {
	
	ModsPhysicalLocation modsPhysicalLocation = new ModsPhysicalLocation();
	
	public ModsPhysicalLocation addModsPhysicalLocation(ModsPhysicalLocation mpl) {		
		
		modsPhysicalLocation = mpl;
		
		return mpl;
	}
	
	public String getModsLocationXML() {
		
		try {
			return "<location>" + modsPhysicalLocation.buildAllLocations() + "</location>" ;
		}catch(NullPointerException e) {
			return "";
		}
		
	}
	
}
