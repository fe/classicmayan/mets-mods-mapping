package org.classicmayan.tools;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

/**
 * 
 * @author Maximilian Brodhun State- and University Library Goettingen
 * @version 1.0.0
 * @since 21.11.2018
 */

public class MetsAmdSec {

	private String ID;
	private String xmlCode;
	MetsRightsMD metsRightsMD;
	MetsDigiProvMD metsDigiProvMD;
	private List<MetsAmdSec> amdSecList = new ArrayList<MetsAmdSec>();
	private List<MetsRightsMD> rightsMDList = new ArrayList<MetsRightsMD>();
	private List<MetsDigiProvMD> digiProvMDList = new ArrayList<MetsDigiProvMD>();

	public MetsAmdSec() {
		
	}
	
	public MetsAmdSec(String ID) {
		this.ID = ID;
		this.setXmlCode("<amdSec ID=\"" + this.getID() + "\">");
	}

	public String getXmlCode() {
		return xmlCode;
	}

	public void setXmlCode(String xmlCode) {
		this.xmlCode = xmlCode;
	}
	
	public MetsAmdSec(List<MetsAmdSec> amdSecList) {
	 this.amdSecList = amdSecList;	
	}
	
	public String getID() {
		return ID;
	}

	public void setID(String iD) {
		ID = iD;
	}
	
	public void addMetsAmdSec(MetsAmdSec metsAmdSec) {
		amdSecList.add(metsAmdSec);
	}	
		
	MetsRightsMD addMetsRightsMD(MetsRightsMD metsRightsMD) {
		this.rightsMDList.add(metsRightsMD);
		this.metsRightsMD = metsRightsMD;
		return metsRightsMD;
	}
	
	MetsDigiProvMD addMetsDigiProvMD(MetsDigiProvMD metsDigiProvMD) {
		this.digiProvMDList.add(metsDigiProvMD);
		this.metsDigiProvMD = metsDigiProvMD;
		return metsDigiProvMD;
	}
	
	public String getMetsAmdSecXML() throws ParseException {
		String rightsMdXML = "";
		for(MetsRightsMD rightsMD : rightsMDList) {
			rightsMdXML += rightsMD.getXML();
		}
		String digiProvMdXML = "";
		for(MetsDigiProvMD digiProvMD : digiProvMDList){
			digiProvMdXML += digiProvMD.getXML();
		}

		return "<amdSec ID=\"" + getID() + "\">" + 
					rightsMdXML + 
					digiProvMdXML +
				"</amdSec>";
	}	
	
	public String getXML() throws ParseException {
		String rightsMdXML = "";
		String digiProvMdXML = "";

		for(MetsRightsMD rightsMD : rightsMDList) {
			rightsMdXML += rightsMD.getXML();
		}

		for(MetsDigiProvMD digiProvMD : digiProvMDList){
			digiProvMdXML += digiProvMD.getXML();
		}
		return this.getXmlCode() + rightsMdXML + digiProvMdXML + "</amdSec>";
	}
}
