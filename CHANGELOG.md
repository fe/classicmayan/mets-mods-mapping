## [2.4.2](https://gitlab.gwdg.de/fe/classicmayan/mets-mods-mapping/compare/v2.4.1...v2.4.2) (2023-10-11)


### Bug Fixes

* increase queries version, add some unknown code... :-D ([e829c87](https://gitlab.gwdg.de/fe/classicmayan/mets-mods-mapping/commit/e829c8767dba3e3a3c7670b6bee97c0a762ff509))

## [2.4.1](https://gitlab.gwdg.de/fe/classicmayan/mets-mods-mapping/compare/v2.4.0...v2.4.1) (2023-03-13)


### Bug Fixes

* fix [#12](https://gitlab.gwdg.de/fe/classicmayan/mets-mods-mapping/issues/12) ([13bb1ec](https://gitlab.gwdg.de/fe/classicmayan/mets-mods-mapping/commit/13bb1ecd38a28181ee30719f32f56837c853ea16))

# [2.4.0](https://gitlab.gwdg.de/fe/classicmayan/mets-mods-mapping/compare/v2.3.2...v2.4.0) (2023-03-07)


### Bug Fixes

* dimensions fixed, fixes [#16](https://gitlab.gwdg.de/fe/classicmayan/mets-mods-mapping/issues/16) ([2683cca](https://gitlab.gwdg.de/fe/classicmayan/mets-mods-mapping/commit/2683cca992864df3ddedf71c0b360509be1753d4))
* reformat class, add logging ([48845d8](https://gitlab.gwdg.de/fe/classicmayan/mets-mods-mapping/commit/48845d8e961656584ba126254f4db4f2e60331d8))


### Features

* include new queries-collection version ([882d068](https://gitlab.gwdg.de/fe/classicmayan/mets-mods-mapping/commit/882d068bd7d49af46f32f5c39f38111fa9471098))

## [2.3.2](https://gitlab.gwdg.de/fe/classicmayan/mets-mods-mapping/compare/v2.3.1...v2.3.2) (2023-02-17)


### Bug Fixes

* creation and modification dates are now UTC (again), METS header CREATEDATE is current date now ([b40d09d](https://gitlab.gwdg.de/fe/classicmayan/mets-mods-mapping/commit/b40d09d7951c22d7417393c1df5baf9cb2e533a7))

## [2.3.1](https://gitlab.gwdg.de/fe/classicmayan/mets-mods-mapping/compare/v2.3.0...v2.3.1) (2023-02-10)


### Bug Fixes

* increase idiom-queries version ([12df5d3](https://gitlab.gwdg.de/fe/classicmayan/mets-mods-mapping/commit/12df5d3719492e92c076171d9addc70a62277540))

# [2.3.0](https://gitlab.gwdg.de/fe/classicmayan/mets-mods-mapping/compare/v2.2.0...v2.3.0) (2023-02-09)


### Features

* increase idiom queries version ([dde5c04](https://gitlab.gwdg.de/fe/classicmayan/mets-mods-mapping/commit/dde5c04294d62b3e37f602e61878d8e9e72379c5))

# [2.2.0](https://gitlab.gwdg.de/fe/classicmayan/mets-mods-mapping/compare/v2.1.4...v2.2.0) (2023-02-09)


### Bug Fixes

* title and idiom id are set with default values ([8a3681b](https://gitlab.gwdg.de/fe/classicmayan/mets-mods-mapping/commit/8a3681b9441a1385f5ae409ab18ae20f799507bf))


### Features

* change UTC date to ISO8601 in MODS recordInfo ([85e6043](https://gitlab.gwdg.de/fe/classicmayan/mets-mods-mapping/commit/85e604395e802d8b34f97d88515a296b3e37d926))

## [2.1.4](https://gitlab.gwdg.de/fe/classicmayan/mets-mods-mapping/compare/v2.1.3...v2.1.4) (2023-01-27)


### Bug Fixes

* add mooore loggggging ([a662e9e](https://gitlab.gwdg.de/fe/classicmayan/mets-mods-mapping/commit/a662e9ec30e8e8ea0ec612192735fcf35603486f))
* add query workaround named MAX :-) ([299796a](https://gitlab.gwdg.de/fe/classicmayan/mets-mods-mapping/commit/299796a43bdc8e44a8ec9233e862d6aa7fa02be9))

## [2.1.3](https://gitlab.gwdg.de/fe/classicmayan/mets-mods-mapping/compare/v2.1.2...v2.1.3) (2023-01-18)


### Bug Fixes

* add speakable extent descriptions ([efe8954](https://gitlab.gwdg.de/fe/classicmayan/mets-mods-mapping/commit/efe8954b573126e32721d88ab4cd40884dd33095))
* fix typo ([1e64433](https://gitlab.gwdg.de/fe/classicmayan/mets-mods-mapping/commit/1e64433fab12fd287ca2243be2bbce5f876fc177))

## [2.1.2](https://gitlab.gwdg.de/fe/classicmayan/mets-mods-mapping/compare/v2.1.1...v2.1.2) (2023-01-17)


### Bug Fixes

* add more extent infos in generated mets ([681b090](https://gitlab.gwdg.de/fe/classicmayan/mets-mods-mapping/commit/681b090621d23dd5890f7bdf84719c6d4203f19b))

## [2.1.1](https://gitlab.gwdg.de/fe/classicmayan/mets-mods-mapping/compare/v2.1.0...v2.1.1) (2022-12-20)


### Bug Fixes

* ignoring not existing data in ConedaKOR JSON now ([ed89ef9](https://gitlab.gwdg.de/fe/classicmayan/mets-mods-mapping/commit/ed89ef9afd0aa4ad2f6927dfebcb6c07c21f3f83))

# [2.1.0](https://gitlab.gwdg.de/fe/classicmayan/mets-mods-mapping/compare/v2.0.1...v2.1.0) (2022-12-07)


### Bug Fixes

* add JSONException on building mets ([7fd5f38](https://gitlab.gwdg.de/fe/classicmayan/mets-mods-mapping/commit/7fd5f38b1e88e379b3d0f385b71b945fdf02b6b0))
* add more error handling to image metadata processing ([4f21e34](https://gitlab.gwdg.de/fe/classicmayan/mets-mods-mapping/commit/4f21e347ea72c810c964e4a2026157b367924125))
* mark imageMetsMods from ConedaKOR data as deprecated ([4b93925](https://gitlab.gwdg.de/fe/classicmayan/mets-mods-mapping/commit/4b9392576004f8ed87c8c546b7c9903c6f1cc95e))


### Features

* increase idiomQueries version ([bc9d3b2](https://gitlab.gwdg.de/fe/classicmayan/mets-mods-mapping/commit/bc9d3b20f23183609a4cd95f6b4f8f5ab77fc3a3))

## [2.0.1](https://gitlab.gwdg.de/fe/classicmayan/mets-mods-mapping/compare/v2.0.0...v2.0.1) (2022-10-06)


### Bug Fixes

* correct dimension output (fixes [#5](https://gitlab.gwdg.de/fe/classicmayan/mets-mods-mapping/issues/5)) ([1b7de1a](https://gitlab.gwdg.de/fe/classicmayan/mets-mods-mapping/commit/1b7de1a0c481b60d77001f7dfd6e6dd8ec23c5a8))
* setting httpS and textgridrep.org as URLs ([d8c4bb4](https://gitlab.gwdg.de/fe/classicmayan/mets-mods-mapping/commit/d8c4bb4fac10269b6368dc80ebd6aa1ad44aca7c))

# [2.0.0](https://gitlab.gwdg.de/fe/classicmayan/mets-mods-mapping/compare/v1.1.42...v2.0.0) (2022-09-21)


### Bug Fixes

* add .gitignore ([0dfe544](https://gitlab.gwdg.de/fe/classicmayan/mets-mods-mapping/commit/0dfe54425e628a5e435a7a43d0da9c74b0cee825))
* add properties file ([4e2b849](https://gitlab.gwdg.de/fe/classicmayan/mets-mods-mapping/commit/4e2b849f335d9f65463d944cc635584e65fdc0b9))
* add test for NoSuchElementExceptions getting artefact titles ([580e87e](https://gitlab.gwdg.de/fe/classicmayan/mets-mods-mapping/commit/580e87e92889db2e2b58e1ab0b96d9c13f3b5530))
* add tgcrud endpoiont ([4191f3a](https://gitlab.gwdg.de/fe/classicmayan/mets-mods-mapping/commit/4191f3ab13deca46ba6a433afecb684fb989d2ae))
* and still more test ignorance ([8eae407](https://gitlab.gwdg.de/fe/classicmayan/mets-mods-mapping/commit/8eae407699ab0c3c13b9f86ad43cfc37fd3a7710))
* fix tests ([47ca582](https://gitlab.gwdg.de/fe/classicmayan/mets-mods-mapping/commit/47ca58207b93a9bbcb8445577c570aea560ec7dc))
* remove log settings, put into log config file ([1d8c7df](https://gitlab.gwdg.de/fe/classicmayan/mets-mods-mapping/commit/1d8c7dfdc6a764e5b49adcf816c7ff70748a15e3))
* somehow move files from somewhere to elsewhere due to Eclipse issues ([fe550cb](https://gitlab.gwdg.de/fe/classicmayan/mets-mods-mapping/commit/fe550cb3a129d20c33286b3982913bbe72d39b63))
* update ignore testing ([9954389](https://gitlab.gwdg.de/fe/classicmayan/mets-mods-mapping/commit/995438909bbb77997bbcdae7e6433ece72adffd7))


### Features

* remvoe SID from code ([e4d6468](https://gitlab.gwdg.de/fe/classicmayan/mets-mods-mapping/commit/e4d6468777f879a46dd8a8bc41ef7b6875625d91))
* take out tgcrud client dependencies, add them to OAI-PMH service ([97c3a54](https://gitlab.gwdg.de/fe/classicmayan/mets-mods-mapping/commit/97c3a5495f441a69bd3afa5eb843cc2134056d83))


### BREAKING CHANGES

* params did change
* Instantiating an ImageMetsMods object needs to set a SID to get proper non-public
responses from TG-crud

## [1.1.42](https://gitlab.gwdg.de/fe/classicmayan/mets-mods-mapping/compare/v1.1.41...v1.1.42) (2022-08-18)


### Bug Fixes

* increase SNAPSHOT version ([6b58d41](https://gitlab.gwdg.de/fe/classicmayan/mets-mods-mapping/commit/6b58d413489532cb2894f49f2e93c8d6d008e902))
* remove dry-run and release ([913f017](https://gitlab.gwdg.de/fe/classicmayan/mets-mods-mapping/commit/913f0172cc399c2f4bc1c093e9f94e0cbe0fc080))

# 1.1.41-SNAPSHOT

* refactor some classes
* remove sysouts
